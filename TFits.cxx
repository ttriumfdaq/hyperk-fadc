#include "TFits.hxx"



// Exponentially Modified Gaussian

double funcEMG(double* x, double* p){
  //Exponential gaussian used for fitting
  // p[0]: amplitude
  // p[1]: gaussian mu
  // p[2]: gaussian sig
  // p[3]: exponential decay constant  -  ???
  // p[4]: baseline

  double y = p[4] + (p[0]/0.3)*(p[3]/2.)*exp((p[1]+p[2]*p[2]/p[3]/2.-x[0])/(p[3]))*
    TMath::Erfc((p[1]+p[2]*p[2]/p[3] -x[0])/sqrt(2.)/p[2]);

  return y;

}


void fitEMG(TF1* fit,
	    double peakTime, 
	    double pulseHeight, 
	    double baseline, 
	    int pulseType,
	    int shaper) {

  double sigma;
  double lambda;

  if(pulseType == 1) {
    // For ref pulse
    sigma = 7.78;
    lambda = 0.982;

    fit->FixParameter(0,pulseHeight*6.16);
    fit->SetParameter(1,peakTime);
    fit->FixParameter(2,sigma);
    fit->FixParameter(3,lambda);
    fit->FixParameter(4,baseline);

  }

  if(pulseType == 0) {
    // Signal pulse
    if(shaper == 1){
      //30ns
      sigma = 30.;
      lambda = 1.0;
      fit->SetParameter(0,pulseHeight*25.0);

    }

    if(shaper == 0){
      //15nsn
      sigma = 30/2.5;
      lambda = 0.4;    
      fit->SetParameter(0,pulseHeight*60);
      
    }
    
    fit->SetParameter(1,peakTime);
    fit->FixParameter(2,sigma);
    fit->FixParameter(3,lambda);
    fit->SetParameter(4,baseline);
    
  }


}



// Fourth Order Exponential

double func4Exp(double* x, double* par) {
 
  //For reference
  //par[0] = pulseHeight
  //par[1] = peakTime (-28?)
  //par[2] = 9.68 or 8.30
  //par[3] = 260 or 130.6
  //par[4] = baseline
  double tDiff = (x[0]-par[1] + 80);
  tDiff/=par[2];
 
  double y;
  y = par[4]+par[0]*tDiff*tDiff*tDiff*tDiff*(1.-tDiff/par[3])*TMath::Exp(-tDiff/1.0);
      
  return tDiff <0. ? par[4] : y;

}


void fit4Exp(TF1* fit,
	     double peakTime, 
	     double pulseHeight, 
	     double baseline, 
	     int pulseType,
	     int shaper) {

  if(pulseType==0) {
   
    double param[2];
    
    param[0] = 13.5;
    param[1] = -800;

    fit->SetParameter(0,pulseHeight*0.5);
    fit->SetParameter(1,peakTime + 38);
    fit->SetParameter(2,param[0]);    
    fit->SetParameter(3,param[1]);
    fit->SetParameter(4,baseline);

  }

}



// Generalizede Normal Version 2 (Skewed Gaussian)

double funcGNV2(double* x, double* p) {
  //http://en.wikipedia.org/wiki/Generalized_normal_distribution   -- version 2
  //p[3] = baseline
  //p[4] = mag
  //p[5] = sigma
  //p[6] = start
  //p[7] = end
  

  double alpha, kappa, Xi, y, phi, range;  // scale, shape, location, --, standard normal gaussian, pulse region
  alpha = p[0];
  Xi = p[1];
  range = p[7]-p[6];
  kappa = p[2];

  // For Normal dist  (kappa = 0).
  if(kappa == 0.)
    y = (x[0] - Xi)/alpha;
  else
    y = - log(1 - kappa*(x[0] - Xi)/alpha) / kappa;  


  phi = exp(-y*y/p[5]) / sqrt(2*TMath::Pi());


  if(x[0] < (p[6]-range)  ||  x[0] > (p[7]+range)) {
    return p[3];  // return baseline; helps stabilize
  } else {
    return p[3] + p[4] * phi / (alpha - kappa*(x[0] - Xi))   *   exp(-0.5*pow((x[0]-Xi)/p[5],2)); 
    // Gaussian factor at end
  }


}

void fitGNV2(TF1* fit,
	     double peakTime, 
	     double pulseHeight, 
	     double baseline, 
	     int pulseType,
	     int shaper,
	     int isConvolt,
	     double start,
	     double end) {

  // Parameters
  double alpha = 1.0;
  double kappa = 1.0;
  double mag = 1.0; // help scaling
  double sigma = 1.0;


  if(pulseType == 0) {
    // Signal or convolution

    if(isConvolt == 0) {
      // Non-convolution shape -- signal

      if(shaper == 0) {

	// 15 ns shaper
	alpha = 1.065;
	kappa = -0.00588;
	mag = pulseHeight * 2.66;
	sigma = 295.6;


	// Attenuated with XL cable
	// alpha = 1.24;
	// kappa = -0.01424;
	// mag = pulseHeight * 3.06;
	// sigma = 466.42;



	// For custom short pulse
	// Apr 7
	// alpha = 1.03759;
	// kappa = -0.00716;
	// mag = pulseHeight * 2.68;
	// sigma = 307.9;


      }

      if(shaper == 1) {

	// 30 ns shaper
	alpha = 1.0615;
	kappa = -0.00504;
	mag = pulseHeight * 2.6146;
	sigma = 1386.65;

	// Attenuated with XL cable 
	// alpha = 1.40;
	// kappa = -0.00799;
	// mag = pulseHeight * 3.45;
	// sigma = 1170.41;


	// For customized short pulse
	// alpha = 1.003;
	// kappa = -0.005037;
	// mag = pulseHeight * 2.4745;
	// sigma = 1430.06;


      }

      fit->FixParameter(0,alpha);
      fit->SetParameter(1,peakTime);
      fit->FixParameter(2,kappa);
      fit->FixParameter(3,baseline);
      fit->FixParameter(4,mag);
      fit->FixParameter(5,sigma);
      fit->FixParameter(6,start);
      fit->FixParameter(7,end);


    }


    if(isConvolt == 1) {
      // Fit to convolution shape

      if(shaper == 0) {

	// 15 ns
	alpha = 1.40;
	kappa = -0.00233;
	mag = 3.5 * (pulseHeight - baseline);
	sigma = 350.0;


	// alpha = 19.0;
	// kappa = -0.000174;
	// //mag = 2 * pulseHeight / 38.;
	// mag = 1.2 * (pulseHeight - baseline);
	// sigma = 350.0;


      }

      if(shaper == 1) {

	// 30 ns
	// alpha = 1.35;
	// kappa = -0.0034;
	// mag = pulseHeight * 2.6 / 100.0 + 1.0; 
	// sigma = 859.4;

	// // DT5724 convolution
	alpha = 1.35;
	kappa = -0.000649;
	mag = (pulseHeight-baseline) * 3.5;
	sigma = 1575.0;


	// DT5724 correlation
	// alpha = 1.35;
	// kappa = 0.00169;
	// mag = (pulseHeight-baseline) * 3.5;
	// sigma = 1571.2;

      }

      fit->FixParameter(0,alpha);
      fit->SetParameter(1,peakTime);
      fit->FixParameter(2,kappa);
      fit->FixParameter(3,baseline);
      fit->FixParameter(4,mag);
      fit->FixParameter(5,sigma);
      fit->FixParameter(6,start);
      fit->FixParameter(7,end);


    }


  }

  if(pulseType == 1) {
  
    std::cout << "Ref pulse not configured for GNV2" << std::endl;
  
  }


}


