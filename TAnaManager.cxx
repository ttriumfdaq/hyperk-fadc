#include "TAnaManager.hxx"



TAnaManager::TAnaManager(TAnaManagerConfig *config){


  if(!config){
    Configuration = new TAnaManagerConfig();
  }else{
    Configuration = new TAnaManagerConfig(*config);
  }  

	fV792Histogram = 0;
#ifdef USE_V792
	fV792Histogram = new TV792Histograms();
	fV792Histogram->DisableAutoUpdate();  // disable auto-update.  Update histo in AnaManager.
#endif

	fV1190Histogram = 0;
#ifdef USE_V1190
	fV1190Histogram = new TV1190Histograms();
	fV1190Histogram->DisableAutoUpdate();  // disable auto-update.  Update histo in AnaManager.
#endif

	fL2249Histogram = 0;
#ifdef USE_L2249
	fL2249Histogram = new TL2249Histograms();
	fL2249Histogram->DisableAutoUpdate();  // disable auto-update.  Update histo in AnaManager.
#endif

	fAgilentHistogram = 0;
#ifdef USE_AGILENT
	fAgilentHistogram = new TAgilentHistograms();
	fAgilentHistogram->DisableAutoUpdate();  // disable auto-update.  Update histo in AnaManager.
#endif

	fV1720Waveform = 0;
#ifdef USE_V1720
	fV1720Waveform = new TV1720Waveform();
	fV1720Waveform->DisableAutoUpdate();  // disable auto-update.  Update histo in AnaManager.
#endif

	fV1730DppWaveform = 0;
#ifdef USE_V1730DPP
	fV1730DppWaveform = new TV1730DppWaveform();
	fV1730DppWaveform->DisableAutoUpdate();  // disable auto-update.  Update histo in AnaManager.
#endif

	fV1730RawWaveform = 0;
#ifdef USE_V1730RAW
	fV1730RawWaveform = new TV1730RawWaveform();
	fV1730RawWaveform->DisableAutoUpdate();  // disable auto-update.  Update histo in AnaManager.
#endif

	fDT724Waveform = 0;
#ifdef USE_DT724
	fDT724Waveform = new TDT724Waveform();
	fDT724Waveform->DisableAutoUpdate();  // disable auto-update.  Update histo in AnaManager.
#endif


	TimeDiffHisto = new TTimeDiffHisto();
	TimeDiffHisto->DisableAutoUpdate();


	// Make a tree of interesting variables.
	fOutputTree = new TTree("hkTimingTree","A simple summary tree");
	fOutputTree->Branch("PulseHeightSignal", &fPulseHeightSignal, "PulseHeightSignal/F");
	fOutputTree->Branch("PulseHeightReference", &fPulseHeightReference, "PulseHeightReference/F");
	fOutputTree->Branch("PulseChargeSignal", &fPulseChargeSignal, "PulseChargeSignal/F");
	fOutputTree->Branch("PulseChargeReference", &fPulseChargeReference, "PulseChargeReference/F");
	fOutputTree->Branch("PulseTimeSignal", &fPulseTimeSignal, "PulseTimeSignal/F");
	fOutputTree->Branch("PulseTimeReference", &fPulseTimeReference, "PulseTimeReference/F");
	fOutputTree->Branch("SigmaTimeDiff",&fSigmaTimeDiff,"SigmaTimeDiff/F");
	fOutputTree->Branch("SigmaTD_err",&fSigmaTD_err,"SigmaTD_err/F");
	fOutputTree->Branch("MeanSigHeight",&fMeanSigHeight,"MeanSigHeight/F");
	fOutputTree->Branch("MeanSH_err",&fMeanSH_err,"MeanSH_err/F");
	fOutputTree->Branch("EquipType",&EquipType,"EquipType/I");
	fOutputTree->Branch("Shaper",&Shaper,"Shaper/I");


	fPulseFinder = new TPulseFinder();
	fPulseFitter = new TPulseFitter();

	// Create histograms for storing information on signal and
	// and reference baseline measurements.
	signalBaseline = new TH1F("sigBase","Signal Baseline",100,-2,2);
	referenceBaseline = new TH1F("refBase","Reference Baseline",100,-2,2);

	signalPulseHeight = new TH1F("signalPulseHeight","Signal Pulse Height",5000,0,500);
	referencePulseHeight = new TH1F("referencePulseHeight","Reference Pulse Height",5000,0,500);

	signalPulseCharge = new TH1F("signalPulseCharge","Signal Pulse Charge",1000,0,1000);
	referencePulseCharge = new TH1F("referencePulseCharge","Reference Pulse Charge",1000,0,1000);


	sigConvolt = new TGraph();


	for(int i = 0; i< 40; i++){
	  tdiff_vs_pe[i] = new TH1F(); // Ranges are set in 'Process Midas Event()' to enable the range
	                               // to be based off the first tdiff measurement
	}


};



int TAnaManager::ProcessMidasEvent(TDataContainer& dataContainer){



	// Set EquipType
	if(Configuration->UseV1730){
	  EquipType = 0;
	}else if(Configuration->UseV1720){
	  EquipType = 1;
	}else if(Configuration->UseDT5724){
	  EquipType = 2;
	}else{
	  std::cout << "\n***Could not retrieve equipment type!***" << std::endl;
	  return 0;
	}

	// Set Shaper
	if(Configuration->Use15ns){
	  Shaper = 0;
	}else if(Configuration->Use30ns){
	  Shaper = 1;
	}else{
	  std::cout << "\n***Could not retrieve shaper type!***" << std::endl;
	  return 0;
	}


	// Check if using MF
	bool useMF = false;
	useMF =	Configuration->UseMF;

	
        if(fV792Histogram) fV792Histogram->UpdateHistograms(dataContainer);
        if(fV1190Histogram)  fV1190Histogram->UpdateHistograms(dataContainer);
        if(fL2249Histogram)  fL2249Histogram->UpdateHistograms(dataContainer);
        if(fAgilentHistogram)  fAgilentHistogram->UpdateHistograms(dataContainer);
        if(fV1720Waveform)  fV1720Waveform->UpdateHistograms(dataContainer);
        if(fV1730DppWaveform)  fV1730DppWaveform->UpdateHistograms(dataContainer);
        if(fV1730RawWaveform)  fV1730RawWaveform->UpdateHistograms(dataContainer);
        if(fDT724Waveform)  fDT724Waveform->UpdateHistograms(dataContainer); 

	// Choose the signal and reference waveform.  Need the right digitizer and 
	// channel number.
	TH1D *sig_waveform;
	TH1D *ref_waveform;
	if(Configuration->UseV1730){
	  sig_waveform = (TH1D*)(*fV1730RawWaveform)[1];
	  ref_waveform = (TH1D*)(*fV1730RawWaveform)[0];
	}else if(Configuration->UseV1720){
	  sig_waveform = (TH1D*)(*fV1720Waveform)[1];
	  ref_waveform = (TH1D*)(*fV1720Waveform)[0];	  
	}else if(Configuration->UseDT5724){
	  sig_waveform = (TH1D*)(*fDT724Waveform)[1];
	  ref_waveform = (TH1D*)(*fDT724Waveform)[0];	  	  
	}


	// To skip analysis
	//		return 0;



	// Find the signal pulse.
	PulseFinderResult result = fPulseFinder->FindPulses(sig_waveform,signalBaseline);

	fPulseHeightSignal = result.pulseHeight;
	fPulseChargeSignal = result.pulseCharge;

	if(result.negPulse == true) {

	  fPulseHeightSignal = -fPulseHeightSignal;
	  fPulseChargeSignal = -fPulseChargeSignal;

	}

	signalPulseHeight->Fill(fPulseHeightSignal);
	signalPulseCharge->Fill(fPulseChargeSignal);



	// Get signal noise
	double noise = 0.;
	signalBaseline->Fit("gaus","Q");
	noise = signalBaseline->GetFunction("gaus")->GetParameter(2);

	
	// Fit the signal pulse
	PulseFitterResult fitResult = 
	  fPulseFitter->FitPulse(sig_waveform, 1, result.negPulse, useMF, noise,
				 result.pulseHeight, result.pulseTime, result.baseline, 0, Shaper, sigConvolt);
	


	// Find the reference pulse.
	PulseFinderResult result2 = fPulseFinder->FindPulses(ref_waveform,referenceBaseline); 

	fPulseHeightReference = result2.pulseHeight;
	fPulseChargeReference = result2.pulseCharge;

	if(result2.negPulse == true) {

	  fPulseHeightReference = -fPulseHeightReference;
	  fPulseChargeReference = -fPulseChargeReference;

	}

	referencePulseHeight->Fill(fPulseHeightReference);
	referencePulseCharge->Fill(fPulseChargeReference);


	// Fit the reference pulse
	PulseFitterResult fitResult2 = 
	  fPulseFitter->FitPulse(ref_waveform, 0, result2.negPulse, useMF, noise,
				 result2.pulseHeight, result2.pulseTime, result2.baseline, 1, Shaper);
	

	if(0)	
	  std::cout << "sigpulsetime " << fitResult.fittedTime << std::endl
		    << "sigheight " << result.pulseHeight << std::endl
		    << "sigcharge " << result.pulseCharge << std::endl
		    << "sigbaseline " << result.baseline << std::endl
		    << "refpulsetime " << fitResult2.fittedTime << std::endl
		    << "refheight " << result2.pulseHeight << std::endl
		    << "refcharge " << result2.pulseCharge << std::endl
		    << "refbaseline " << result2.baseline << std::endl;


	
	// Skip on blank event -- caution, not infallible obviously
	if(result.baseline == 0 || result2.baseline == 0)
	  return 0;


	// Choose MF or non-MF
	if(useMF){
	  fPulseTimeSignal = (float) ConvoltPeakTime(sigConvolt,result.baseline,Shaper);
	}else{
	  fPulseTimeSignal = fitResult.fittedTime;
	}


	fPulseTimeReference = fitResult2.fittedTime;


	// Skip on fit failure
	if(fPulseTimeSignal == 0 || fitResult2.fittedTime == 0) {
	  return 0;
	}
	
	// Time difference
	float tdiff = fPulseTimeSignal - fPulseTimeReference;
	//float tdiff = fPulseTimeSignal;

	double pe = result.pulseHeightPE;
	if(result.negPulse == true)
	  pe = -pe;

	int pe_bin = (pe-0.25)*2;

	// Make tdiff histos, based on first measurement
	if(tdiff_vs_pe[pe_bin]->GetEntries() == 0) {  //first entry

	  for(int i = 0; i< 40; i++){
	    char name[100];
	    double low = i*0.5 + 0.25;
	    double high = low + 0.5;
	    sprintf(name,"tdiff_%f_%f",low,high);
	    tdiff_vs_pe[i]->SetNameTitle(name,name);
	    tdiff_vs_pe[i]->SetBins(1600,tdiff-40,tdiff+40);
	    
	    TimeDiffHisto->tmp->SetBins(3200,tdiff-40,tdiff+40);

	  }
	}

	if(pe >= 0. && pe < 40.) 	
	  tdiff_vs_pe[pe_bin]->Fill(tdiff);


	// TimeDiffHisto
	TimeDiffHisto->UpdateHistograms(pe,tdiff);


	//Gaussian fit to TimeDiffHisto
	TimeDiffHisto->tmp->Fit("gaus","Q");
	fSigmaTimeDiff = TimeDiffHisto->tmp->GetFunction("gaus")->GetParameter(2);
	fSigmaTD_err = TimeDiffHisto->tmp->GetFunction("gaus")->GetParError(2);


	// Gaussian fit to signal height
	signalPulseHeight->Fit("gaus","Q");
	fMeanSigHeight = signalPulseHeight->GetFunction("gaus")->GetParameter(1);;
	fMeanSH_err = signalPulseHeight->GetFunction("gaus")->GetParError(1);


	// Output every 500 events
	static int event;
	event += 1;

	if(event % 500 == 0) {
	std::cout << "sig height " << fMeanSigHeight << "\t"
		  << "sigma time " << fSigmaTimeDiff << std::endl;
	}


	fOutputTree->Fill();

	return 1;
}



bool TAnaManager::HaveV792Histograms(){
	if(fV792Histogram) return true; 
	return false;
}
bool TAnaManager::HaveV1190Histograms(){
	if(fV1190Histogram) return true; 
	return false;
};
bool TAnaManager::HaveL2249Histograms(){
	if(fL2249Histogram) return true; 
	return false;
};
bool TAnaManager::HaveAgilentistograms(){
	if(fAgilentHistogram) return true; 
	return false;
};
bool TAnaManager::HaveV1720Histograms(){
	if(fV1720Waveform) return true; 
	return false;
};
bool TAnaManager::HaveV1730DPPistograms(){
	if(fV1730DppWaveform) return true; 
	return false;
};
bool TAnaManager::HaveV1730Rawistograms(){
	if(fV1730RawWaveform) return true; 
	return false;
};
bool TAnaManager::HaveDT724Histograms(){
	if(fDT724Waveform) return true; 
	return false;
};

TV792Histograms* TAnaManager::GetV792Histograms() {return fV792Histogram;}
TV1190Histograms* TAnaManager::GetV1190Histograms(){return fV1190Histogram;}
TL2249Histograms* TAnaManager::GetL2249Histograms(){return fL2249Histogram;}
TAgilentHistograms* TAnaManager::GetAgilentistograms(){return fAgilentHistogram;}
TV1720Waveform* TAnaManager::GetV1720Histograms(){return fV1720Waveform;}
TV1730DppWaveform* TAnaManager::GetV1730DPPistograms(){return fV1730DppWaveform;}
TV1730RawWaveform* TAnaManager::GetV1730Rawistograms(){return fV1730RawWaveform;}
TDT724Waveform* TAnaManager::GetDT724Histograms(){return fDT724Waveform;}

