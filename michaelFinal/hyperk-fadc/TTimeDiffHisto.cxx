#include "TTimeDiffHisto.h"

#include "TV1720RawData.h"
#include "TDirectory.h"


/// Reset the histograms for this canvas
TTimeDiffHisto::TTimeDiffHisto(){

  CreateHistograms();

}


void TTimeDiffHisto::CreateHistograms(){


  // check if we already have histogramss.
  char tname[100];
  sprintf(tname,"tdiffhistos_%f_%f",0.25,0.75);

  
  tmp = (TH1F*)gDirectory->Get(tname);
  if (tmp) return;

  // Otherwise make histograms
  clear();

  tmp = new TH1F();
  tmp->SetNameTitle(tname,tname);

  push_back(tmp);
  


  
}


void TTimeDiffHisto::UpdateHistograms(double pe, double timeDiff){


  int pe_bin = (pe-0.25)*2;
  
  if(pe_bin >= 0 && pe_bin < 40)
    GetHistogram(0)->Fill(timeDiff);


  tmp->Fill(timeDiff);
  

}



void TTimeDiffHisto::Reset(){

  
  for(int i = 0; i < 8; i++){ // loop over channels
    int index =  i;
    
    GetHistogram(index)->Reset();
    
  }
}
