// Default program for dealing with various standard TRIUMF VME setups:
// V792, V1190 (VME), L2249 (CAMAC), Agilent current meter
//
//

#include <stdio.h>
#include <iostream>
#include <time.h>

#include "TRootanaEventLoop.hxx"
#include "TAnaManager.hxx"


class Analyzer: public TRootanaEventLoop {


public:

	// An analysis manager.  Define and fill histograms in 
	// analysis manager.
	TAnaManager *anaManager;

  Analyzer() {
    //DisableAutoMainWindow();

    anaManager = new TAnaManager();

  };

  virtual ~Analyzer() {};

  void Initialize(){


  }

	void Init(){

	  TAnaManagerConfig *old_config = 0;;
	  if(anaManager){
	    old_config = anaManager->Configuration;
	    delete anaManager;
	  }
	  
	  anaManager = new TAnaManager(old_config);
	}


  void BeginRun(int transition,int run,int time){

		Init();

  }

  bool CheckOption(std::string option)
  {
    return anaManager->CheckOption(option);
  }


  void Usage()
  {
    anaManager->Usage();
  }

  void EndRun(int transition,int run,int time){

    anaManager->EndOfRun();

  }


  bool ProcessMidasEvent(TDataContainer& dataContainer){

		if(!anaManager) Init();

		anaManager->ProcessMidasEvent(dataContainer);

    return true;
  }


}; 


int main(int argc, char *argv[])
{

  Analyzer::CreateSingleton<Analyzer>();
  return Analyzer::Get().ExecuteLoop(argc, argv);

}

