#include "TPulseFitter.hxx"

#include <cmath>
#include <vector>
#include "TROOT.h"
#include "TMath.h"
#include "TFitResult.h"
#include "TGraph.h"
#include <iomanip>


TF1 *fStep, *Template, *Prod;


PulseFitterResult TPulseFitter::FitPulse(TH1D *waveform, int ch, bool negPulse, bool useMF,
					 double pulseHeight, double peakTime, double baseline, 
					 int shaper, TGraph *graph){
  

  PulseFitterResult result;

  double range_low = 0;
  double range_high = 0;

  // Set fitting ranges (ns)
  // 1 is signal, 0 is reference
  if(ch == 1){
    
    if(shaper == 0){
      // 15 ns
      range_low = 50; 
      range_high = 30;
    }else if(shaper == 1){
      // 30 ns
      range_low = 120;
      range_high = 50;
    }else{
      //no shaper
      range_low = 22;
      range_high = 30;
    }

  }

  if(ch == 0){
    range_low = 60;
    //range_low = 30;
    range_high = 30;
    //range_high = 10;

  }
  
  double start = peakTime - range_low;
  double end = peakTime + range_high;

  // Delete old fits
  char refname[100],signame[100],tempname[100];
  sprintf(refname,"pulse_timing_0");
  sprintf(signame,"pulse_timing_1");
  sprintf(tempname,"template_name");

  TF1 *oldref = (TF1*)gROOT->Get(refname);
  TF1 *oldsig = (TF1*)gROOT->Get(signame);
  TF1 *oldtemp = (TF1*)gROOT->Get(tempname);

  if(oldref){
    delete oldref;
  }
  if(oldsig){
    delete oldsig;
  }
  if(oldtemp){
    delete oldtemp;
  }


  // Reference pulse  --  EMG

  if(ch == 0) {
  
    // Currently only has range set up for EMG, but EMG works well
    TF1 *refFit_tf1 = new TF1(refname,funcEMG,start,end,5);

    // Fitting function that sets parameters
    fitEMG(refFit_tf1,peakTime,pulseHeight,baseline,ch,shaper);
    refFit_tf1->SetLineColor(2);
    
    TFitResultPtr refFit_resultPtr =  waveform->Fit(refname,"QRS","",start, end);

    // Filter non-convergent fits
    // Using Minuit convention : =0 not calculated, =1 approximated (failed), =2 made pos def, =3 accurate 
    if(refFit_resultPtr->CovMatrixStatus() == 3) {

      if(negPulse == false){
    	result.fittedTime = refFit_tf1->GetMaximumX();
	result.fittedHeight = refFit_tf1->GetMaximum() - baseline;
      }else{
    	result.fittedTime = refFit_tf1->GetMinimumX();
	result.fittedHeight = baseline - refFit_tf1->GetMinimum();
      }

    } else {

      result.fittedTime = 0;
      result.fittedHeight = 0;
      std::cout << "Bad reference fit\n";

    }


    // Loop parameter values to determine an average
    if(0) {
      static double avg  = 0;
      static double avgn = 0;
      int par = 2;
        
      avg += refFit_tf1->GetParameter(par);
      avgn += 1.0;
       
      std::cout << "single value " << refFit_tf1->GetParameter(par)
    		<< " average value " <<  avg/avgn 
    		<< std::endl;

    }


  }


  // Signal Pulse

  // For a fitting function "FF"
  //     funcFF() is the function equation
  //     fitFF() sets the parameters
  

  if(ch == 1) {

    if(useMF) {

      // Matced Filtering

      // double window = (end-start)*3*2;
      // double win_low = start - window/3;
      // double win_high = end + window/3;

      double window = 600;
      double win_low = 800;
      double win_high = 1400;


      double binwidth = waveform->GetBinWidth(1);


      // Make template
      Template = new TF1(tempname,funcGNV2,win_low-window/3,win_high+window/3,8);
      //   fitGNV2(Template,peakTime,pulseHeight,baseline,ch,2,0,start,end); 

      // double alpha = 0.5479;
      // double kappa = -0.00391206;
      // //    double mag = pulseHeight * 1.363;
      // //double mag = 173.798 * 1.363;
      // double mag = 25.0;
      // double sigma = 418.061;


      // DT5724 template 30ns
      double alpha = 2.08;
      double kappa = -0.00664;
      double mag = -400.0;
      double sigma = 400.0;

  

  
      Template->FixParameter(0,alpha);
      Template->FixParameter(1,win_low + window/2);
      Template->FixParameter(2,kappa);
      Template->FixParameter(3,baseline);
      Template->FixParameter(4,mag);
      Template->FixParameter(5,sigma);
      Template->FixParameter(6,start);
      Template->FixParameter(7,end);

      Template->SetLineColor(49);
    
      TFitResultPtr Tempfit =  waveform->Fit(tempname,"QRS","",win_low-window/3,win_high+window/3);


      if(negPulse == false)
        result.fittedTime = Template->GetMaximumX();
      else
        result.fittedTime = Template->GetMinimumX();


      int nfactor = 1; // number of points per bin

      if(shaper == 0) {
        nfactor = 2; 
      }


      int n = nfactor * window / binwidth; // number of graph points in convolt region
      std::vector<double> x(n),ytemp(n),ydat(n),prod(n),yconv(n);
      int index;
      double step = window/n;


      // Clear graph
      if(graph){
      
        graph->Clear();

      }


      // Scaling factor -- smaller factor means large height
      double factor = nfactor * fabs(pulseHeight - baseline) / 2.0;

      if(fabs(pulseHeight - baseline) < 15.0)
        factor = factor / 150.0;


      // Gather template and waveform data points, then convolude
      for(int j=0;j<n;j++) {

        for(int i=0;i<n;i++) {

      	index = (win_low + i*step)/binwidth + 1;
    	
      	x[i] = win_low + (i)*step;
      	ydat[i] = waveform->GetBinContent(index) - baseline;

      	// For Convolution
      	ytemp[i] = Template->Eval(result.fittedTime + range_high - i*step + step*j ) - baseline;

      	// For Correlation
      	//ytemp[i] = Template->Eval(result.fittedTime - range_low + i*step - step*j ) - baseline;

      	prod[i] = ydat[i] * ytemp[i] / factor;

      	yconv[j] += prod[i] + baseline/n;

        }

        graph->SetPoint(j,x[j],yconv[j]);

      }



 
    } else {

      // Direct, non-MF


      // Don't forget to adjust the fitting function and number of parameters accordingly in TF1
      int npar = 5;
      TF1 *sigFit_tf1 = new TF1(signame,funcEMG,start,end,npar);
      //fitGNV2(sigFit_tf1,peakTime,pulseHeight,baseline,ch,shaper,0,start,end); //8par
      //fit4Exp(sigFit_tf1,peakTime,pulseHeight,baseline,ch,shaper); //5par
      fitEMG(sigFit_tf1,peakTime,pulseHeight,baseline,ch,shaper); //5par
      sigFit_tf1->SetLineColor(9);
    
      TFitResultPtr sigFit_resultPtr =  waveform->Fit(signame,"QRS","",start,end);

      // risetime
      double rt_start = 0;
      double rt_end = 0;


      if(sigFit_resultPtr->CovMatrixStatus() == 3) {

	if(negPulse == false){
	  result.fittedTime = sigFit_tf1->GetMaximumX();
	  result.fittedHeight = sigFit_tf1->GetMaximum() - baseline;

	  rt_start = sigFit_tf1->GetX(0.1*result.fittedHeight + baseline,start,result.fittedTime);
	  rt_end = sigFit_tf1->GetX(0.9*result.fittedHeight + baseline,start,result.fittedTime);

	  result.fittedRisetime = rt_end - rt_start;

	}else{
	  result.fittedTime = sigFit_tf1->GetMinimumX();
	  result.fittedHeight = baseline - sigFit_tf1->GetMinimum();
	  
	  rt_start = sigFit_tf1->GetX(baseline - 0.1*result.fittedHeight,start,result.fittedTime);
	  rt_end = sigFit_tf1->GetX(baseline - 0.9*result.fittedHeight,start,result.fittedTime);

	  result.fittedRisetime = rt_end - rt_start;

	}

      } else {

	result.fittedTime = 0;
	result.fittedHeight = 0;
	std::cout << "Bad signal fit\n";

      }


    
      // Loop parameter values to determine an average
      if(0){
	static double avg  = 0;
	static double avgn = 0;
	static double sumrt = 0;
	static double rtn = 0;

	int par = 3;

	avgn += 1.0;
	rtn += 1.0;

	// The V1720 blanks on first event, so skip the first and double the second event values to help the averaging
	if(avgn == 1) {
	
	}else if(avgn == 2) {
	  avg = 2*sigFit_tf1->GetParameter(par);
	  sumrt = 2*result.fittedRisetime;
	}else{
	  avg += sigFit_tf1->GetParameter(par);
	  sumrt += result.fittedRisetime;

	   std::cout << "single value " << sigFit_tf1->GetParameter(par)
	   	  << " average value " <<  avg/avgn 
	   	  << std::endl;
	  // std::cout << std::setprecision(10) << "single value rt " << result.fittedRisetime
	  // 	    << " average risetime " << sumrt/rtn 
	  // 	    << std::endl;
	}

      }


    }

 
  }

  
  return result;


}
