#ifndef TPulseFitter_h
#define TPulseFitter_h

#include "TTree.h"
#include "TH1D.h"
#include "TF1.h"
#include "TGraph.h"

#include "TFits.hxx"


class PulseFitterResult{

public:

  double fittedTime;
  double fittedHeight;
  double fittedRisetime;

};



class TPulseFitter  {

public:

  TPulseFitter(){
    
  }


  PulseFitterResult FitPulse(TH1D *waveform, 
			     int ch,
			     bool negPulse,
			     bool useMF,
			     double pulseHeight, 
			     double peakTime, 
			     double baseline, 
			     int shaper,
			     TGraph *graph = 0);


  void EndRun(){

  }
  
private:

};

#endif

