#include "TCanvas.h"
#include "TROOT.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TAxis.h"
#include "TLegend.h"
#include "TGaxis.h"
#include "TFile.h"
#include "TTree.h"
#include "TMultiGraph.h"
#include "TPaveText.h"

#include <iostream>
#include <fstream>
#include <string>

// .L TimeResMacro.C+g

void myStyle(TGraphErrors *g, int color, int style, double size = 1.4) {

  g->SetMarkerColor(color);
  g->SetMarkerStyle(style);
  g->SetMarkerSize(size);
  
}

void TimeResMacro() {
  
  TCanvas *c1 = new TCanvas("c1","Timing Resolution vs Signal Height",700,700);
  TCanvas *c2 = new TCanvas("c2","Timing Resolution vs SNR",700,700);

  c1->Clear();
  c2->Clear();

  // Open file for run list
  std::string run;
  std::ifstream runlist ("runlist.txt");

  int filerange = 0;

  if(runlist.is_open()) {
    for(; std::getline(runlist,run); ++filerange) {
      //std::cout << run << std::endl;
    }
  }  
  else 
    cout << "Unable to open file" << std::endl;


  // Arrays for plotting
  float V30_15x[filerange], V30_15y[filerange], V30_15x_err[filerange], V30_15y_err[filerange];
  float V20_15x[filerange], V20_15y[filerange], V20_15x_err[filerange], V20_15y_err[filerange];
  float D24_15x[filerange], D24_15y[filerange], D24_15x_err[filerange], D24_15y_err[filerange];
  float V30_30x[filerange], V30_30y[filerange], V30_30x_err[filerange], V30_30y_err[filerange];
  float V20_30x[filerange], V20_30y[filerange], V20_30x_err[filerange], V20_30y_err[filerange];
  float D24_30x[filerange], D24_30y[filerange], D24_30x_err[filerange], D24_30y_err[filerange];

  int V30_15count = 0;
  int V20_15count = 0;
  int D24_15count = 0;
  int V30_30count = 0;
  int V20_30count = 0;
  int D24_30count = 0;

  float snrV30_15x[filerange], smpV30_15y[filerange], snrV30_15x_err[filerange];
  float snrV20_15x[filerange], smpV20_15y[filerange], snrV20_15x_err[filerange];
  float snrD24_15x[filerange], smpD24_15y[filerange], snrD24_15x_err[filerange];
  float snrV30_30x[filerange], smpV30_30y[filerange], snrV30_30x_err[filerange];
  float snrV20_30x[filerange], smpV20_30y[filerange], snrV20_30x_err[filerange];
  float snrD24_30x[filerange], smpD24_30y[filerange], snrD24_30x_err[filerange];

  float baselineRMS_rUncty = 0.001;



  float MeanSigHeight, MeanSH_err;
  float SigmaTimeDiff, SigmaTD_err;
  int equipType; //0 - V1730 ; 1 - V1720 ; 2 - DT5724
  int shaper;
  float tdiff;
  float baseRMS;

  //Ranges
  double_t xmin, xmax, ymin, ymax;
  xmin = 0.;
  ymin = 0.;
  xmax = 35.;
  ymax = 2;


  // fill arrays
  runlist.clear();
  runlist.seekg(0, ios::beg);

  int runcount = 1;

  while(std::getline(runlist,run)) {
    
    // Convert string to char
    char *runnum = new char[run.length() + 1];
    strcpy(runnum, run.c_str());

    // Get Tree
    TFile *data_file = new TFile (runnum);

    TTree *hkTimingTree = (TTree*)data_file->Get("hkTimingTree");

    hkTimingTree->SetBranchAddress("MeanSigHeight",&MeanSigHeight);
    hkTimingTree->SetBranchAddress("MeanSH_err",&MeanSH_err);
    hkTimingTree->SetBranchAddress("SigmaTimeDiff",&SigmaTimeDiff);
    hkTimingTree->SetBranchAddress("SigmaTD_err",&SigmaTD_err);
    hkTimingTree->SetBranchAddress("EquipType",&equipType);
    hkTimingTree->SetBranchAddress("Shaper",&shaper);
    hkTimingTree->SetBranchAddress("baseRMS",&baseRMS);

    int entries = hkTimingTree->GetEntries();
    hkTimingTree->GetEntry(entries-1);

    std::cout << "\n--------" << runnum << "--------" << std::endl;

    if(equipType == 0)
      std::cout << "V1730, 500 MSPS" << std::endl;
    else if(equipType == 1)
      std::cout << "V1720, 250 MSPS" << std::endl;
    else
      std::cout << "DT5724, 100 MSPS" << std::endl;

    if(shaper == 0)
      std::cout << "15 ns shaper" << std::endl;
    else
      std::cout << "30 ns shaper" << std::endl;

    std::cout << "Number of entries " << entries << std::endl
    	      << "Mean Sig height " << MeanSigHeight << " +/- " << MeanSH_err << std::endl
    	      << "RMS tdiff " << SigmaTimeDiff << " +/- " << SigmaTD_err << std::endl;


    // V1730
    if(equipType == 0) {
      if(shaper == 0) {

	V30_15x[V30_15count] = MeanSigHeight;
	V30_15y[V30_15count] = SigmaTimeDiff;
	V30_15x_err[V30_15count] = MeanSH_err;
	V30_15y_err[V30_15count] = SigmaTD_err;
	
	snrV30_15x[V30_15count] = 20 * log10(MeanSigHeight / baseRMS);
	smpV30_15y[V30_15count] = SigmaTimeDiff / 2.;
	snrV30_15x_err[V30_15count] = 20*sqrt((MeanSH_err/MeanSigHeight)*(MeanSH_err/MeanSigHeight)
					      + baselineRMS_rUncty*baselineRMS_rUncty);

	V30_15count++;

      } else if(shaper == 1) {

	V30_30x[V30_30count] = MeanSigHeight;
	V30_30y[V30_30count] = SigmaTimeDiff;
	V30_30x_err[V30_30count] = MeanSH_err;
	V30_30y_err[V30_30count] = SigmaTD_err;

	snrV30_30x[V30_30count] = 20 * log10(MeanSigHeight / baseRMS);
	smpV30_30y[V30_30count] = SigmaTimeDiff / 2.;
	snrV30_30x_err[V30_30count] = 20*sqrt((MeanSH_err/MeanSigHeight)*(MeanSH_err/MeanSigHeight)
					      + baselineRMS_rUncty*baselineRMS_rUncty);
	

	V30_30count++;

      }
    }

    // DT5724
    // 30ns shaper
    if(equipType == 2) {
      if(0 < runcount && runcount < 15) {

	V20_15x[V20_15count] = MeanSigHeight;
	V20_15y[V20_15count] = SigmaTimeDiff;
	V20_15x_err[V20_15count] = MeanSH_err;
	V20_15y_err[V20_15count] = SigmaTD_err;

	snrV20_15x[V20_15count] = 20 * log10(MeanSigHeight / baseRMS);
	smpV20_15y[V20_15count] = SigmaTimeDiff / 10.;
	snrV20_15x_err[V20_15count] = 20*sqrt((MeanSH_err/MeanSigHeight)*(MeanSH_err/MeanSigHeight)
					      + baselineRMS_rUncty*baselineRMS_rUncty);


	V20_15count++;

      } else if(55 < runcount && runcount < 100) {
	// low power

	V20_30x[V20_30count] = MeanSigHeight;
	V20_30y[V20_30count] = SigmaTimeDiff;
	V20_30x_err[V20_30count] = MeanSH_err;
	V20_30y_err[V20_30count] = SigmaTD_err;

	snrV20_30x[V20_30count] = 20 * log10(MeanSigHeight / baseRMS);
	smpV20_30y[V20_30count] = SigmaTimeDiff / 10.;
	snrV20_30x_err[V20_30count] = 20*sqrt((MeanSH_err/MeanSigHeight)*(MeanSH_err/MeanSigHeight)
					      + baselineRMS_rUncty*baselineRMS_rUncty);

	V20_30count++;
 
      }
    }

    // DT5724
    // 15ns SHAPER
    if(equipType == 2) {
      if(14 < runcount && runcount < 24) {

	D24_15x[D24_15count] = MeanSigHeight;
	D24_15y[D24_15count] = SigmaTimeDiff;
	D24_15x_err[D24_15count] = MeanSH_err;
	D24_15y_err[D24_15count] = SigmaTD_err;

	snrD24_15x[D24_15count] = 20 * log10(MeanSigHeight / baseRMS);
	smpD24_15y[D24_15count] = SigmaTimeDiff / 10.;
	snrD24_15x_err[D24_15count] = 20*sqrt((MeanSH_err/MeanSigHeight)*(MeanSH_err/MeanSigHeight)
					      + baselineRMS_rUncty*baselineRMS_rUncty);

	D24_15count++;

      } else if(23 < runcount && runcount < 56) {
	//low power

	D24_30x[D24_30count] = MeanSigHeight;
	D24_30y[D24_30count] = SigmaTimeDiff;
	D24_30x_err[D24_30count] = MeanSH_err;
	D24_30y_err[D24_30count] = SigmaTD_err;

	snrD24_30x[D24_30count] = 20 * log10(MeanSigHeight / baseRMS);
	smpD24_30y[D24_30count] = SigmaTimeDiff / 10.;
	snrD24_30x_err[D24_30count] = 20*sqrt((MeanSH_err/MeanSigHeight)*(MeanSH_err/MeanSigHeight)
					      + baselineRMS_rUncty*baselineRMS_rUncty);

	D24_30count++;

      }


    }


    delete data_file;
    delete [] runnum;

    runcount++;

  }


  runlist.close();

  // 15ns shaper, circles
  TGraphErrors *V30_15plot = new TGraphErrors(V30_15count, V30_15x, V30_15y, V30_15x_err, V30_15y_err);
  myStyle(V30_15plot,kCyan+1,20);
  TGraphErrors *V20_15plot = new TGraphErrors(V20_15count, V20_15x, V20_15y, V20_15x_err, V20_15y_err);
  myStyle(V20_15plot,kMagenta-3,22);
  TGraphErrors *D24_15plot = new TGraphErrors(D24_15count, D24_15x, D24_15y, D24_15x_err, D24_15y_err);
  myStyle(D24_15plot,kMagenta-3,20);

  // 30ns shaper, triangles
  TGraphErrors *V30_30plot = new TGraphErrors(V30_30count, V30_30x, V30_30y, V30_30x_err, V30_30y_err);
  myStyle(V30_30plot,kCyan+1,22);
  TGraphErrors *V20_30plot = new TGraphErrors(V20_30count, V20_30x, V20_30y, V20_30x_err, V20_30y_err);
  myStyle(V20_30plot,kMagenta-3,26);
  TGraphErrors *D24_30plot = new TGraphErrors(D24_30count, D24_30x, D24_30y, D24_30x_err, D24_30y_err);
  myStyle(D24_30plot,kMagenta-3,24);


  // Samples vs. SNR plots
  TGraphErrors *sV30_15plot = new TGraphErrors(V30_15count, snrV30_15x, smpV30_15y, V30_15x_err, V30_15y_err);
  myStyle(sV30_15plot,kCyan+1,20);
  TGraphErrors *sV20_15plot = new TGraphErrors(V20_15count, snrV20_15x, smpV20_15y, V20_15x_err, V20_15y_err);
  myStyle(sV20_15plot,kMagenta-3,22);
  TGraphErrors *sD24_15plot = new TGraphErrors(D24_15count, snrD24_15x, smpD24_15y, D24_15x_err, D24_15y_err);
  myStyle(sD24_15plot,kMagenta-3,20);

  TGraphErrors *sV30_30plot = new TGraphErrors(V30_30count, snrV30_30x, smpV30_30y, V30_30x_err, V30_30y_err);
  myStyle(sV30_30plot,kCyan+1,22);
  TGraphErrors *sV20_30plot = new TGraphErrors(V20_30count, snrV20_30x, smpV20_30y, V20_30x_err, V20_30y_err);
  myStyle(sV20_30plot,kMagenta-3,26);
  TGraphErrors *sD24_30plot = new TGraphErrors(D24_30count, snrD24_30x, smpD24_30y, D24_30x_err, D24_30y_err);
  myStyle(sD24_30plot,kMagenta-3,24);


  //Fit 1/x
  if(0) {
    TF1 *f1 = new TF1("f1","[0]/x",xmin,xmax);
    f1->SetLineColorAlpha(kCyan+1,0.15);
    V30_15plot->Fit("f1","+");
    f1->SetLineColorAlpha(kGreen+2,0.15);
    V20_15plot->Fit("f1","+");  
    f1->SetLineColorAlpha(kMagenta-3,0.15);
    D24_15plot->Fit("f1","+");  
    f1->SetLineColorAlpha(kCyan+1,0.15);
    V30_30plot->Fit("f1","+");  
    f1->SetLineColorAlpha(kGreen+2,0.15);
    V20_30plot->Fit("f1","+");  
    f1->SetLineColorAlpha(kMagenta-3,0.15);
    D24_30plot->Fit("f1","+");    
  }


  c1->cd();

  //Make multiplot
  TMultiGraph *Multiplot = new TMultiGraph();
  TMultiGraph *sMultiplot = new TMultiGraph();

  Multiplot->Add(V30_15plot);
  Multiplot->Add(V20_15plot);
  Multiplot->Add(D24_15plot);
  Multiplot->Add(V30_30plot);
  Multiplot->Add(V20_30plot);
  Multiplot->Add(D24_30plot);

  sMultiplot->Add(sV30_15plot);
  sMultiplot->Add(sV20_15plot);
  sMultiplot->Add(sD24_15plot);
  sMultiplot->Add(sV30_30plot);
  sMultiplot->Add(sV20_30plot);
  sMultiplot->Add(sD24_30plot);

  Multiplot->Draw("AP");

  //Multiplot->SetTitle("Timing Resolution vs Signal Pulse Height");
  Multiplot->GetXaxis()->SetTitle("Signal Pulse Height (mV)");
  Multiplot->GetXaxis()->CenterTitle();
  Multiplot->GetXaxis()->SetTitleSize(0.04);
  Multiplot->GetYaxis()->SetTitle("Timing Resolution (ns)");
  Multiplot->GetYaxis()->CenterTitle();
  Multiplot->GetYaxis()->SetTitleSize(0.04);
  Multiplot->GetYaxis()->SetTitleOffset(1.1);

  ymax = 1;
  xmax = Multiplot->GetXaxis()->GetXmax();
  Multiplot->GetXaxis()->SetLimits(xmin,xmax); //Sets axis min to xmin, but keeps the axis max automatic
  Multiplot->SetMinimum(ymin);
  Multiplot->SetMaximum(ymax);


  //Legend
  int nentries = 4;
  TLegend *leg = new TLegend(0.65,0.9-0.05*nentries,0.9,0.9,"100MSPS Digitizer");
  //leg->AddEntry(V30_15plot,"500MSPS - 15ns sh.","p");
  //leg->AddEntry(V30_30plot,"500MSPS - 30ns sh.","p");
  leg->AddEntry(V20_15plot,"30ns sh. - High power","p");
  leg->AddEntry(V20_30plot,"30ns sh. - Low power","p");
  leg->AddEntry(D24_15plot,"15ns sh. - High power","p");
  leg->AddEntry(D24_30plot,"15ns sh. - Low power","p");

  leg->Draw();

  
  //PE axis
  if(0) {
    TGaxis *PE = new TGaxis(0,ymax,xmax,ymax,0,(xmax/5),510,"-");
    PE->Draw();
    PE->SetTitle("PE");
    PE->SetTitleOffset(1.0);
  }  

  //Custom PaveText title
  if(0) {
    TPaveText *Title = new TPaveText(.05,ymax,18.,ymax+0.3);
    Title->AddText("blank");
    Title->Draw();
  }

  //For logY
  // Multiplot->SetMinimum(0.005);
  // Multiplot->SetMaximum(10);
  // gPad->SetLogy();


  c1->Update();


  c2->cd();
  sMultiplot->Draw("AP");
  gPad->SetLogy();
  gPad->SetGrid();

  //sMultiplot->SetTitle("Timing Resolution vs. SNR");
  sMultiplot->GetXaxis()->SetTitle("SNR (dB)");
  sMultiplot->GetXaxis()->CenterTitle();
  sMultiplot->GetXaxis()->SetTitleSize(0.04);
  sMultiplot->GetYaxis()->SetTitle("Time Resolution (samples)");
  sMultiplot->GetYaxis()->CenterTitle();
  sMultiplot->GetYaxis()->SetTitleSize(0.04);
  sMultiplot->GetYaxis()->SetTitleOffset(1.1);

  xmin = 10;
  xmax = sMultiplot->GetXaxis()->GetXmax();
  ymin = 0.005;
  //ymax = 10;
  sMultiplot->GetXaxis()->SetLimits(xmin,xmax); //Sets axis min to xmin, but keeps the axis max automatic
  sMultiplot->SetMinimum(ymin);
  sMultiplot->SetMaximum(ymax);

  leg->Draw();
  c2->Update();


}

#ifndef __CINT__
int main() {
  TimeResMacro();
  return 0;
}
#endif

