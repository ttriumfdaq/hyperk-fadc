#include "TPulseFinder.hxx"
#include "TROOT.h"

#include <iostream>

 
PulseFinderResult TPulseFinder::FindPulses(TH1D *waveform, TH1F *baseline_histo, double init_baseline){

  PulseFinderResult result;


  float pulseCharge = 0;
  result.pulseTime = -9999;
  double bin_sum = 0;
  double wavemax = waveform->GetMaximum();
  double wavemin = waveform->GetMinimum();
  double maxdist = 0;
  double mindist = 0;
  double baseline = 0;

  // Loop over the bins in waveform.
  for(int i = 1; i < waveform->GetNbinsX(); i++){

    double x = waveform->GetBinCenter(i);
    double bin_value = waveform->GetBinContent(i);


    bin_sum += bin_value;

    // Fill the baseline histogram
    if(baseline_histo && x < pulseWindow[0]){
      baseline_histo->Fill(bin_value-init_baseline);
    }

    // Calculate baseline
    if(pulseCharge == 0) {
      baseline = bin_sum/(double)i;
      maxdist = fabs(wavemax - baseline);
      mindist = fabs(wavemin - baseline);
    }

          
    // In pulse region 
    if(x > pulseWindow[0] && x < pulseWindow[1]) {
	
      // Pulse time

      if(maxdist > mindist && (bin_value - wavemax) == 0) {
	result.pulseTime = x;
	result.pulseHeight = bin_value - baseline;
	result.negPulse = false;
      }

      if(maxdist < mindist && fabs(bin_value - wavemin) == 0) {
	result.pulseTime = x;
	result.pulseHeight = bin_value - baseline;
	result.negPulse = true;
      }

      // Pulse charge
      pulseCharge += bin_value - baseline;

    }

  }


  result.pulseHeightPE = result.pulseHeight/5.;
  result.pulseCharge = pulseCharge;
  result.baseline = baseline;

  return result;

}
