#ifndef TPulseFinder_h
#define TPulseFinder_h
 
#include "TTree.h"
#include "TH1D.h"
#include <cmath>

class PulseFinderResult{

public:
  bool foundPulse;
  bool negPulse;
  double pulseHeight; // in mV
  double pulseHeightPE; // in PE
  double pulseTime; // in ns
  double pulseCharge; // integral
  double baseline;
  double wavemax;
  double wavemin;

};


// When giving a waveform, class will perform the
// following analysis:
//  i) calculate the integral in the pulse region (pulse charge)
//  ii) measure average baseline before pulse region
//  iii) fill baseline histogram before pulse region
//  iv) locate pulse time for either positive or negative pulse
class TPulseFinder  {

public:

  TPulseFinder(){
    // Perhaps make dynamic
    pulseWindow[0] = 600;
    pulseWindow[1] = 1600;
  }
  
  PulseFinderResult FindPulses(TH1D *waveform, TH1F *baseline_histo, double init_baseline);

  void EndRun(){}

private:

  // Define the window inside which we will look for pulses.
  double pulseWindow[2];


};

#endif

