#ifndef TFits_h
#define TFits_h

#include "TROOT.h"
#include "TMath.h"
#include <cmath>
#include "TFitResult.h"
#include "TF1.h"


double funcGNV2(double *x, double *p);
double funcEMG(double *x, double *p);
double func4Exp(double *x, double *p); 

void fitGNV2(TF1*,double peakTime,double pulseHeight,double baseline,int ch,int shaper,
	     int isConvolt,double start,double end);
void fitEMG(TF1*,double peakTime,double pulseHeight,double baseline,int ch,int shaper);
void fit4Exp(TF1*,double peakTime,double pulseHeight,double baseline,int ch,int shaper);


#endif
