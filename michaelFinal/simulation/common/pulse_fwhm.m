function fwhm = pulse_fwhm(t, y)
%PULSE_FWHM Calculate FWHM of the pulse
%
%   PULSE_FWHM( t, y ) 
%   Calculates FWHM (Full Width Half Maximum) of the provided pulse. The method finds the
%   maximum, then proceeds backwards and forward in order to find left and right points
%   of crossing the thresholds of half the amplitude. The exact times  are
%   interpolated using linear interpolation. The method DOES NOT subtract
%   pedestal.
%
%   Arguments:
%       t - time vector
%       y - pulse vector (must be same length as time)

% Wyznacz progi (niski i wysoki)
[a_max,i_max] = max(y);
thr = 0.5 * a_max;

%
% Wyznacz czas narastania
%

% Wyznacz lewy punkt 50%
j = find(y(1:i_max) < thr, 1, 'last');
y_vector = [y(j) y(j+1)];
t_vector = [t(j) t(j+1)];
y_interp = [y(j) thr y(j+1)];
t_interp = interp1(y_vector, t_vector, y_interp, 'linear');
t1 = t_interp(2);

% Wyznacz prawy punkt 50%
j = i_max-1 + find(y(i_max:length(y)) > thr, 1, 'last');
y_vector = [y(j) y(j+1)];
t_vector = [t(j) t(j+1)];
y_interp = [y(j) thr y(j+1)];
t_interp = interp1(y_vector, t_vector, y_interp, 'linear');
t2 = t_interp(2);

% Oblicz czas narastania
fwhm = t2-t1;
    
end

