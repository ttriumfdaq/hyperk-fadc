function m = calculate_sallen_key_m( n, Q, K )
% Wylicza wsp�czynnik proporcjonalno�ci rezystor�w w filtrze o topografii
% Sallen-Key, na postawie:
%   n - wybrany wsp�czynnik proporcjonalno�ci kondensator�w
%   Q - tzw. damping factor. Filtr krytyczny to Q=0.5
%   K - wzmocnienie filtru w pa�mie przepustowym

A = (1 + n*(1-K))^2 * Q^2;
B = 2 * (1 + n*(1-K)) * Q^2 - n;
C = Q^2;

delta = B^2 - 4*A*C;

m1 = (-B - sqrt(delta)) / (2*A)
m2 = (-B + sqrt(delta)) / (2*A)

Q_m1 = sqrt(m1*n) / (m1 + 1 + m1*n*(1-K))

if (Q_m1 > 0)
    m = m1;
else
    m = m2;
end

end

