function FormatAxes()
%Funkcja formatuje bie��cy wykres do wydruku. Domy�lny format powinna mie� w zmiennej globalnyj DEFAULT_PRINT_FMT

global DEFAULT_PRINT_FMT;

%F=gcf;
%set(F,'Position',[0 0 1400 1200]);

A=gca;

set(A,'FontName',DEFAULT_PRINT_FMT.AxesFontName);
set(A,'FontSize',DEFAULT_PRINT_FMT.AxesFontSize);
set(A,'LineWidth',DEFAULT_PRINT_FMT.AxesLineWidth);

    %Font dla wszystkich obiekt�w testowych - dla tych poni�ej mo�e by�
    %nadpisany
Texts = findall(A,'Type','text');
for i=1:length(Texts)
    set(Texts(i),'FontName',DEFAULT_PRINT_FMT.TextsFontName);
    set(Texts(i),'FontSize',DEFAULT_PRINT_FMT.TextsFontSize);
end;


XL = get(A,'Xlabel');
set (XL,'FontName',DEFAULT_PRINT_FMT.LabelFontName);
set(XL,'FontSize',DEFAULT_PRINT_FMT.LabelFontSize);

YL = get(A,'Ylabel');
set (YL,'FontName',DEFAULT_PRINT_FMT.LabelFontName);
set(YL,'FontSize',DEFAULT_PRINT_FMT.LabelFontSize);

T = get(A,'Title');
set (T,'FontName',DEFAULT_PRINT_FMT.TitleFontName);
set(T,'FontSize',DEFAULT_PRINT_FMT.TitleFontSize);

L=legend(A);
set (L,'FontName',DEFAULT_PRINT_FMT.LegendFontName);
set(L,'FontSize',DEFAULT_PRINT_FMT.LegendFontSize);
    %poszerzamy troszk� legend�
LP = get(L,'Position');
if length(LP)
    set(L,'Position',[LP(1)+DEFAULT_PRINT_FMT.LegendScale(1), LP(2)+DEFAULT_PRINT_FMT.LegendScale(2), LP(3)*DEFAULT_PRINT_FMT.LegendScale(3), LP(4)*DEFAULT_PRINT_FMT.LegendScale(4)]);
end;%set(L,'xlim',[0 1.5]);

    %Grubo�� wszystkich linii
Lines = findall(A,'Type','line');
for i=1:length(Lines)
    set(Lines(i),'LineWidth',DEFAULT_PRINT_FMT.PlotLineWidth);
end;