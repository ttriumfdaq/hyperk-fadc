function [spectrum, freq] = noise_spectrum(data, fs, N)
%Fnkcja wyznacza widmo g�sto�ci mocy metod� u�rednionych periodogram�w
%Wyj�cie:
%   spectrum    - widmo wyj�ciowe, wyskalowane w V/rtHz
%   freq        - warto�ci dla osi X wykresu
%Wej�cie:
%   data        - dane przebiegu
%   fs          - cz�stotliwo�� pr�bkowania
%   N           - �yczona liczba pr�bek na osi cz�stotliwo�ci

N2 = 2*(N-1);

Nbunches = floor(length(data)/N2);

for i = 1:Nbunches
    ffts(i,:) = abs(fft(data((i-1)*N2+1:i*N2)));
end;
    
spectrum = mean (ffts.^2); %mamy u�rednione, niewyskalowane widmo MOCY

spectrum = [spectrum(1) 2*spectrum(2:N)];
%spectrum = 2*spectrum(1:N);

bin_width = fs/N2;
freq = (0:N-1)*bin_width;

spectrum = spectrum / N2;  %w V^2 RMS        
spectrum = spectrum / fs;  %w V^2/Hz

spectrum = sqrt(spectrum);

plot(freq,spectrum);
xlabel('Frequency [Hz]');
ylabel('Spectral density [V/\surd{Hz}]');

%a tak w og�le to jest funkcja pwelch, kt�ra robi to samo, tylko �e
%lepiej...




