function spice_parameters(filename,parameters)
%Buduje plik z parametrami dla spajsa
%filename - nazwa pliku bez rozszerzenia (zostanie dodane .inc)
%Ten sam plik musi by� zalibowany w .asc
%Parametry maj� posta� rekordu P:
%   P.label - nazwa parametru
%   P.value - warto�� parametru (liczba rzeczywista)

file = fopen([filename '.inc'],'wt');    %otwieramy plik do zapisu
for i=1:length(parameters)
    line = ['.param ' parameters(i).label '=' sprintf('%e',parameters(i).value)];
    fprintf(file,'%s\n',line);    
end;

fclose(file);