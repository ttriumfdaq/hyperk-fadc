function [ cf_time integral cf_slope ] = pulse_cf_time_integral_fft( samples, cf_delay, window, fft_N, zero_N, show_plots )
%PULSE_CF_TIME_INTEGRAL_FFT Extract time of pulse arrival (by a digital constant fraction algorithm) and pulse integral
%
%   [ cf_time integral cf_slope ] = PULSE_CF_TIME_INTEGRAL_FFT( samples, delay, pedestal, window, fft_N, zero_method_N, show_plots )
%   Extract time of pulse arrival and its integral. Time is calculated using a digital constant fraction algorithm, which  
%   returnes a sample index (may be a non-integer value) corresponding to zero-crossing of the constant fraction waveform. 
%   Source waveform may be interpolated using fft interpolation in order to increase sample density. Integral is calculated 
%   using the fft-interpolated waveform, within a window given in the parameters. 
%
%   WARNING: It is assumed that the source waveform has zero pedestal (i.e. pedestal estimation and subtraction must be done
%            somewhere else).
%
%   The algorithm is as follows:
%
%   1. The original waveform is interpolated using the FFT algorithm. 
%   2. The interpolated waveform is delayed by the amount of samples corresponding to 'delay' parameter 
%     (units: samples of original waveform, may be non-integer), inverted and subsequently multiplied by 2.
%   3. The interpolated and the delayed/inverted waveform are added together, thus forming the 'cf' waveform.
%   4. The section between the maximum and the minimum value of the 'cf' waveform is examined and an intersection 
%      of the 'cf' waveform and the abisca is found. The exact value is interpolated using a linear interpolation.
%   5. The integral is calculated using the interpolated waveform, within the window specified in the parameters.
%
%   Arguments:
%       samples - waveform to analyze; pedestal subtraction must be done elsewhere.
%       cf_delay - delay of inverted waveform (in samples)
%       window - a two-element vector specifying the begin and the end of the integration window (sample indexes). 
%                Set to [0,0] to disable integral calculation.
%       fft_N - factor of fft interpolation (i.e. by how much we increase number of samples)
%       zero_N - number of samples (in CF-waveform) used for interpolating zero-crossing point 
%       show_plots - turns on/off display of CF waveforms
%
%   Returnes values:
%       cf_time - calculated time of pulse arrival (in samples)
%       integral - calculated pulse integral
%       cf_slope - slope at the intersection point (in arbitrary unints per sample).

% Okienko debug-owania
my_figure = 104;

% Oblicz op�nienie dla przebiegu interpolowanego
cf_delay_interp = round(cf_delay * fft_N);

% Wykonaj interpolacj� FFT w celu zwi�kszenia g�sto�ci pr�bek (je�li trzeba)
cnt = length(samples);
if (fft_N == 1)
    main_interp = samples';
else
    main_interp = interpft(samples, fft_N*cnt)';
end;

% Utw�rz przebieg odwr�cony i dodaj wymagane pr�bki do przebiegu interpolowanego
delayed_interp(1:cf_delay_interp) = -2 .* main_interp(1);
delayed_interp(cf_delay_interp+1 : cf_delay_interp + length(main_interp)) = -2 .* main_interp;
main_interp(length(main_interp):length(main_interp)+cf_delay_interp) = main_interp(length(main_interp));

% Zbuduj przebieg dyskryminatora CF i znajd� minimum - nasz punkt wyj�ciowy do szukania punktu przeci�cia z osi� x
cf_interp = main_interp + delayed_interp;
[~, idx] = min(cf_interp);

% Sprawd�, czy rzeczywi�cie mamy przej�cie przez poziom zera
if (cf_interp(idx) * cf_interp(idx+1) > 0)
	% Sprawd�, czy mamy przesun�� si� w lewo
	while (cf_interp(idx) < 0) && (idx > 1)
		idx = idx-1;
	end;
	
	% Sprawd�, czy mamy przesun�� si� w prawo
	while (cf_interp(idx+1) > 0) && (idx+1 < length(cf_interp))
		idx = idx+1;
	end;	
end

%
% Wylicz czas na podstawie punktu przeci�cia kawa�ka przebiegu o najwi�kszym nachyleniu i osi x.
% W zale�no�ci od liczby uwzgl�dnianych pr�bek skorzystaj z interpolacji liniowej b�d� z regresji liniowej
%
if zero_N <= 2
    %
    % Mamy dwie pr�bki - skorzystaj z interpolacji liniowej
    %
    x1 = idx;
    x2 = idx+1;
    y1 = cf_interp(x1);
    y2 = cf_interp(x2);
    cf_time = (((y1-y2)*x2 - y2*(x1-x2)) / (y1-y2)) / fft_N;
    cf_slope = (y2-y1) / (x2-x1);
else
    %
    % Mamy powy�ej dw�ch pr�bek - wykorzystaj regresj� liniow�
    %
    
    % Okre�l punkty s�siaduj�ce z punktem przeci�cia
    zero_neighbours = ceil(zero_N/2);
    x = idx-zero_neighbours+1 : idx+zero_neighbours;
    y = cf_interp(x);
    
    % Wykonaj fit liniowy i wyznacz punkt przeci�cia oraz nachylenie krzywej w punkcie przeci�cia
    zero_fit = fit(y', x', 'poly1');
    cf_time = zero_fit(0);
    cf_slope = 1/zero_fit.p1;
end

% Wylicz ca�k� (je�li podano poprawne okno)
if (window(1) > 0)
    integral_start = (window(1)-1) * fft_N + 1;
    integral_stop = window(2) * fft_N;
    integral = sum(main_interp(integral_start:integral_stop)) / fft_N;
else
    integral = 0;
end

if show_plots > 0
    figure(my_figure);
	    
    % Narysuj przebiegi
	cnt_interp = length(cf_interp);
	plot_x_wfm = 0 : fft_N : fft_N*cnt-1;
	plot_x_interp = 0:cnt_interp-1;
	plot([1 cnt_interp], [0 0], 'k:');
    hold on;
    plot(plot_x_interp, main_interp, 'r--.', 'MarkerSize', 5);
    plot(plot_x_interp, delayed_interp, 'b--.', 'MarkerSize', 5);
    plot(plot_x_interp, cf_interp, 'g--.', 'MarkerSize', 5);    
    plot(plot_x_wfm, samples, 'k.', 'MarkerSize', 15);
	plot(cf_time*fft_N-1, 0, 'kx', 'MarkerSize', 15, 'LineWidth', 2);
    hold off;
	
	title('Digital CF waveforms');
	xlabel('Sample no.');
	ylabel('Sample value');
end

end

