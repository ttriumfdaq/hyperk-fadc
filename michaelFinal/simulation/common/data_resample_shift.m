function D=data_resample_shift(x,y,Ts,Tshift)
%Resampluje dane do postaci liniowej
%Ts to nowy okres próbkowania
%Tshift to przesunięcie początku próbkowania
%x i y to dane dla obydwu osi

tmax = x(length(x));
t = 0;  %aktualny czas
i = 1;  %numer punktu
pointer = 1;    %wskaźnik do punktu w starych tablicach
T = Tshift;
while (t < tmax)
        while t > x(pointer)
            pointer = pointer+1;
        end;
        if pointer == 1
            %pierwszy punkt
            D(i) = y(pointer);
        else
            %interpolacja liniowa
            bigdiff = x(pointer) - x(pointer-1);
            smalldiff = t - x(pointer-1);
            D(i) = (smalldiff/bigdiff) * y(pointer) + (1-smalldiff/bigdiff) * y(pointer-1);
        end;   
        %inkrementujemy czas
    t=(i*Ts)+Tshift;
    i=i+1;  %właśnie w tej kolejności, bo t idzie od 0, a i od 1
end;