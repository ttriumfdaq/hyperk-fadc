function Format = DefaultAxesFormat()
%Zwraca strukturę ze standardowym formatowaniem wykresu

Format.AxesFontName = 'Helvetica-Narrow';
Format.AxesFontSize = 13;
Format.AxesLineWidth = 1;

Format.TextsFontName = 'Helvetica';
Format.TextsFontSize = 13;

Format.LabelFontName = 'Helvetica';
Format.LabelFontSize = 14;

Format.TitleFontName = 'Helvetica';
Format.TitleFontSize = 16;

Format.LegendFontName = 'Helvetica';
Format.LegendFontSize = 12;
%Format.LegendScale = [-0.05 +0.045 1.4 1];   %Dwa pierwsze parametry to przesunięcie legendy, dwa drugie to skalowanie
Format.LegendScale = [0 0 1 1];

Format.PlotLineWidth = 1;
