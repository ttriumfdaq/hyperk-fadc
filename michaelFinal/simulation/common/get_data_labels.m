function L=get_data_labels(data)
%Wypluwa etykiety wszystkich przebiegów, w postaci tablicy celli

for i=1:length(data)
    L{i} = data(i).label;
end;