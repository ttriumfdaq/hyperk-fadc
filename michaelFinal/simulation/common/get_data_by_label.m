function D=get_data_by_label(data,label)
%Ze struktury wypluwanej przez symulator spajsowy wypluwa tablic� dla
%przebiegu o zadanej nazwie

found = 0;
for i=1:length(data)
    if strcmp(data(i).label,label)
        found = 1;
        break;
    end;
end;

if found == 0
    D = [];
    return; %Nie znaleziono
end;

D = data(i).data;