function command = spice_tran_command(stoptime, time_to_start, max_timestamp, steady, startup, step, nodiscard, uic)
%Generuje komend� .tran (Transient)
%Parametry:
%   stoptime:       Stop Time
%   time_to_start:  Time to start saving data
%   max_timestamp:  Maximum timestamp
%Flagi (0/1)
%   steady:         Stop simulating if steady state is detected
%   startup:        Start external DC voltages at 0V
%   step:           Step the load current source
%   nodiscard:      Don't reset T=0 when steady state is detected
%   uic:            Skip initial operating point solution

command = sprintf('.tran 0 %e %e %e', stoptime, time_to_start, max_timestamp);
if steady
    command = [command ' steady'];
end;
if startup
    command = [command ' startup'];
end;
if step
    command = [command ' step'];
end;
if nodiscard
    command = [command ' nodiscard'];
end;
if uic
    command = [command ' uic'];
end;
