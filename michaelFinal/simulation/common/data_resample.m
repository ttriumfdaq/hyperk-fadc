function D=data_resample(x,y,Ts)
%Resampluje dane do postaci liniowej
%Ts to nowy okres pr�bkowania
%x i y to dane dla obydwu osi

tmax = x(length(x));
t = 0;  %aktualny czas
i = 1;  %numer punktu
pointer = 1;    %wska�nik do punktu w starych tablicach
while (t < tmax)
        while t > x(pointer)
            pointer = pointer+1;
        end;
        if pointer == 1
            %pierwszy punkt
            D(i) = y(pointer);
        else
            %interpolacja liniowa
            bigdiff = x(pointer) - x(pointer-1);
            smalldiff = t - x(pointer-1);
            D(i) = (smalldiff/bigdiff) * y(pointer) + (1-smalldiff/bigdiff) * y(pointer-1);
        end;   
        %inkrementujemy czas
    t=i*Ts;
    i=i+1;  %w�a�nie w tej kolejno�ci, bo t idzie od 0, a i od 1
end;