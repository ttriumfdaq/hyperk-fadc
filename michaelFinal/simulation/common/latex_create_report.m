function latex_create_report( filename_tex, figures_to_include )
%LATEX_CREATE_REPORT Create TeX file from the given list of figures

fid = fopen(filename_tex, 'w+');

for i=1:length(figures_to_include)
    fprintf(fid, '\\begin{figure}[H]\n');
    fprintf(fid, '  \\begin{center}\n');
    
    for j=1:length(figures_to_include{i}.filenames)
        fprintf(fid, '    \\begin{minipage}[t]{.49\\textwidth}\n');
        fprintf(fid, '      \\begin{center}\n');
        fprintf(fid, '        \\includegraphics[width=\\textwidth] {%s}\n', figures_to_include{i}.filenames{j});
        fprintf(fid, '      \\end{center}\n');
        fprintf(fid, '    \\end{minipage}\n');
    end
    
    fprintf(fid, '  \\end{center}\n');
    fprintf(fid, '  \\caption{%s}\n', figures_to_include{i}.caption);
    fprintf(fid, '  \\label{fig:%s}\n', figures_to_include{i}.label);
    fprintf(fid, '\\end{figure}\n\n');
end

fclose(fid);

end

