function spice_addlines(filename,newfilename,lines)
%Dodaje dodatkowe linie do pliku netlisty
%filename tradycyjnie bez rozszerzenia (.net)
%lines to tablica celli z liniami

filename_in = [filename '.net'];
if ~exist(filename_in,'file')
    return;
end;
filename_out = [newfilename '.net'];

file_in = fopen(filename_in,'rt');
file_out = fopen(filename_out,'wt');

finished = 0;
while ~finished
    if feof(file_in)
        break;
    end;
    line = fgets(file_in);
    if length(strfind(line,'.end')) %znale�li�my koniec pliku
        for i=1:length(lines)
            fprintf(file_out,'%s\n',lines{i});
        end;  
        finished = 1;
    end;
    fprintf(file_out,'%s',line);    
end;




fclose(file_out);
fclose(file_in);