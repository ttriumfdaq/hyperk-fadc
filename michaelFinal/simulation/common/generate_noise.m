function noise = generate_noise( filename, density, ts, stoptime )
% Generuje pr�bki szumu bia�ego i zapisuje dane do pliku.
% Parametry:
% filename - nazwa pliku, do kt�rego maj� zosta� zapisane dane
% density - g�sto� widmowa szumu, w V/rtHz
% ts - okres pr�bkowania (tj. krok czasowy)
% stoptime - czas trwania przebiegu

N = stoptime ./ ts;
fs = 1 / ts;

% Alokuj pami��
noise = zeros(N,2);

% Wygeneruj warto�ci dla osi czasu
noise(:,1) = [0:N-1] .* ts;

if (density > 0)
	disp('MATLAB: Generating noise samples');

	% Wylicz amplitud� szumu
	Vn = density * sqrt(fs/2);

	% Wygeneruj warto�ci dla szumu
	noise(:,2) = randn(N,1) .* Vn;

	% Wyzeruj pierwsz� pr�bk� - to musimy zrobi�, bo inaczej SPICE zak�ada, i� �r�d�o ma niezerow� sk�adow� sta��
	% i �le wylicza punkty pracy - trzeba czeka�, a� "dojad�" do w�a�wiwych warto�ci
	noise(1,2) = 0;	

	disp(['MATLAB: Writing noise samples to a file: ' filename]);
else
	disp(['MATLAB: Writing zero waveform to a file: ' filename]);
end

% Zapisz przebieg do pliku
dlmwrite(filename, noise, 'delimiter', ' ');

end

