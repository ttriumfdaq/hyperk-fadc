function [tau_R2C1 m] = calculate_sallen_key( tau, n, Q, K )
% Wylicza wsp�czynnik proporcjonalno�ci rezystor�w w filtrze o topografii
% Sallen-Key oraz sta�� czasu R2C1 wymagan� do osi�gni�cia zak�adanej
% sta�ej czasu filtru aktywnego (fg = 1/(2*pi*tau)).
% Parametry wej�ciowe:
%   tau - ��dana sta�a czasowa filtra aktywnego
%   n - wybrany wsp�czynnik proporcjonalno�ci kondensator�w
%   Q - tzw. damping factor. Filtr krytyczny to Q=0.5
%   K - wzmocnienie filtru w pa�mie przepustowym
% Warto�ci wyj�ciowe:
%   tau_R1C1 - sta�a czasowa dla element�w R2 oraz C1 (patrz topologia filtra)
%   m - wsp�czynnim proporcjonalno�ci dla resystor�w (R1 = m*R2)

% Oblicz m na podstawie zadanych
A = (1 + n*(1-K))^2 * Q^2;
B = 2 * (1 + n*(1-K)) * Q^2 - n;
C = Q^2;

delta = B^2 - 4*A*C

m1 = (-B - sqrt(delta)) / (2*A)
m2 = (-B + sqrt(delta)) / (2*A)

Q_m1 = sqrt(m1*n) / (m1 + 1 + m1*n*(1-K));

if (Q_m1 > 0)
    m = m1;
else
    m = m2;
end

tau_R2C1 = tau / sqrt(m*n);

end

