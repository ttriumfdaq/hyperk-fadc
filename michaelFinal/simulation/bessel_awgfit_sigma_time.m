% Script for simulation time resolution of the setup AWG+shaper, at various AWG pulse rise times, shaper rise times and
% signal to noise ratios. Script also allows for checking different orders of Bessel-type low-pass shaping filters.
%
% NOTE: Shapers realized in hardware use Bessel-type filters of order 5.

global SHOW_PLOTS;
global RESULTS_FOLDER;
close;
% Simulation settings
min_order = 5;      % Minimum order of Bessel-type low pass filter used in shaper
max_order = 5;      % Maximum order of Bessel-type low pass filter used in shaper
t_max = 22;         % Maximum time for simulated impulse response of the filter (note: this is normalized time used by MATLAB functions)
thr_low = 0.1;      % Lower threshold for calculating rise/fall times of the pulse (relative to pulse amplitude)
thr_high = 0.9;     % Higher threshold for calculating rise/fall times of the pulse (relative to pulse amplitude)

sh_risetime = [1.405 3.24 3.51 8.1 7.03 16.2];    % Number of samples along rising edge for impulse response of the shaper, seen by the different ADCs

% Number of samples along rising edge for AWG pulse seen by the different ADCs
awg_risetime = [1.2 1.2 3.1 3.1 6.2 6.2];  %High power 

risetime = [sh_risetime' awg_risetime'];

SNR_dB = [10:5:50];               % Signal to noise ratio, in decibels
awgval = [0 0 1 16 167 1103 4761 13521 25570 32767 29238 18958 9514 4010 1542 572 210 77 28 10 3 1 0 0];    % AWG waveform

sim_cnt = 1000;      % Number of events to be simulated
sim_resolution = 0.001;     % Timestep for simulating analog waveforms (to be given as a fraction of a sample of the ADC)
% Sometimes better resolution (0.0001, 0.00005) is needed for high SNR (90-100dB)

% Create normalized prototypes of shaper transfer functions
fprintf(1,'Creating transfer functions of Bessel-type low pass filters...\n');

H = {};
for N = min_order:max_order
    [num,den] = besself(N,1);
    H{N-min_order+1} = tf(num,den);
end;

% Prepare for parallel calculations
if matlabpool('size') == 0
    matlabpool open;
end;

% Create a structure describing the results
info_cf_sigma_time.description = 'Results of timing resolution using fitting method and simulated AWG pulses';
info_cf_sigma_time.parameters = 'filter order; SNR; rise time;';
info_cf_sigma_time.filter_order = min_order:max_order;
info_cf_sigma_time.SNR_dB = SNR_dB;
info_cf_sigma_time.risetime = risetime;

% Pre-allocate structure for results
awgfit_sigma_time = zeros(length(H), length(SNR_dB), length(risetime));

% Chi2 histogram matrix
nbins = 20;
centers = zeros(1,length(nbins));
counts = zeros(1,length(nbins));
chi2hist = zeros(length(H), length(SNR_dB), length(risetime),2,nbins);

%Fit to exponentially modified Gaussian
[funcEqn,funcname,nparam] = funcEMG();


tdiff = zeros(length(SNR_dB),sim_cnt);

% Loop over filter orders
for i_order = 1:length(H)
    
    % Loop over risetimes
    for i_risetime = 1:length(risetime(:,1))
        
        % Calculate impulse response of the filter
        [y, t] = impulse(H{i_order}, t_max);
        
        % Modify timestep of the impulse response in such a way as to have desired number of samples at the leading edge
        % of the pulse. Create a waveform using a microstep defined by 'sim_resolution' - we will sample it later. This
        % is equivalent to the noiseless analog waveform of pure impulse response of the shaper.
        t_rise = pulse_rise_time(t, y, thr_low, thr_high);
        dt = t_rise / sh_risetime(i_risetime);
        t = 0 : dt*sim_resolution : t_max;
        y = impulse(H{i_order},t);
        
        % Create AWG pulse with sim_resolution density
        awg = resample_awg(awgval,risetime(i_risetime,2),sim_resolution);

        % Convolve
        y = conv(y,awg');
        y = y ./ max(y);
        [ymax,ymax_idx] = max(y);

        % Gives risetime in samples
%        y_rt = y(1:ymax_idx);
%        i_rt = (y_rt>0.1 & y_rt<0.9);
%        sum(i_rt)*sim_resolution;
        
        
        % Loop over various signal to noise ratios
        for i_snr = 1:length(SNR_dB)
            
            % Say where we are...
            fprintf(1,['\n  Current filter order: %d, shaping rise time: %.2f samples,\n'...
                '  AWG risetime: %.2f samples, SNR: %d dB\n'],...
                min_order - 1 + i_order, sh_risetime(i_risetime),...
                awg_risetime(i_risetime),int32(SNR_dB(i_snr)));
            
            adjrsquare = zeros(1,sim_cnt); % Degrees of freedom adjusted R-square
            
            parfor j = 1:sim_cnt
            %for j = 1:sim_cnt
                
                %Create a sampled waveform from the 'analog' waveform created previously with microstep
                t_offset = 1 + round((1/sim_resolution - 1) * rand(1,1));
                idx_sample = t_offset : 1/sim_resolution : length(y);
                
                y_sample = y(idx_sample);

                % Add noise to samples. Assume, that signal power is 0 dBW (should be OK, as the noiseless signal is normalized)
                y_sample = awgn(y_sample, SNR_dB(i_snr), 0);
                
                [ysamp_max,ysamp_max_idx] = max(y_sample);
                
                % Fit parameter seeds
                fit_seeds = SeedSet(funcname,nparam,24,length(y));
                fit_seeds(i_risetime,2) = idx_sample(ysamp_max_idx);
                
                % Fix some parameters
                p = [fit_seeds(i_risetime,1) fit_seeds(i_risetime,3) fit_seeds(i_risetime,4)];
                funcEqn = funcEMGfix(p);
                
                % Sampled points to exclude in fit
                %fit_end = int32(sim_resolution*length(y) / 20);
                %exclude_index = [(ysamp_max_idx+fit_end):length(y_sample)];
                exclude_index = 0;          
                
                try
                    [sample_fit,gof] = fit(idx_sample',y_sample,funcEqn,'Start',...
                    [fit_seeds(i_risetime,2) 1 0],'Exclude',exclude_index,'TolFun',10^-8);

                    % The degrees of freedom adjusted r-square from the fit
                    adjrsquare(j) = gof.adjrsquare;
                    
                    %Find fit peak time
                    yfit = feval(sample_fit,1:length(y));
                    [fit_pk,fit_pk_idx] = findpeaks(yfit);
                    tdiff(i_snr,j) = sim_resolution*(fit_pk_idx - ymax_idx);
                    
                    if false
                        %hold off;
                        plot(sample_fit,idx_sample,y_sample');
                        %hold on;
                        % Location of last fit point
                        %plot(idx_sample(ysamp_max_idx+fit_end),y_sample(ysamp_max_idx+fit_end),'gx');
                                                
                    end;
                catch
                
                end;

            end;
            
            % Calculate time resolution and record the result
            converged = tdiff(i_snr,:) ~= 0;
            awgfit_sigma_time(i_order, i_snr, i_risetime) = std(tdiff(i_snr,converged));
            fprintf('  # of fit fails: %i\t timing resolution: %.3e \n',...
                sim_cnt - sum(converged),std(tdiff(i_snr,converged)));
            
            % Chi2 histograms
            [counts,centers] = hist(adjrsquare(converged),nbins);
            chi2hist(i_order, i_snr, i_risetime,1,:) = centers;
            chi2hist(i_order, i_snr, i_risetime,2,:) = counts;
            
            % Create filename for saving the partial results
            filename = sprintf('%s/partial_sigma_time/N%d-tr%.1f-snr%d.mat', RESULTS_FOLDER, ...
                int32(i_order) + min_order - 1, ...
                risetime(i_risetime), ...
                int32(SNR_dB(i_snr)));
            % Save data for this SNR and risetine
            t_rise = risetime(i_risetime);
            %save(filename, 'cf_time', 'cf_delay_in_samples', 't_rise');
            
        end;
        
        % Save results
        save(strcat(RESULTS_FOLDER,'/bessel_awgfit_sigma_time.mat'), 'awgfit_sigma_time', 'chi2hist', 'info_cf_sigma_time');
    end;
    
    % Clear variables occupying most of memory
    clear t y awg;
end;
