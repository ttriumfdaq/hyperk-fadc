global RESULTS_FOLDER;
set(0,'defaultlinelinewidth',1.5);
set(0,'defaultlinemarkersize',7);
fprintf(['     This will overwrite the images in results/figures/:\n'...
    '          sim_data_fits_lowrt  png&pdf\n'...
    '          sim_data_fits_lowrt_reduced  png&pdf\n'...
    '          sim_data_fits_lowrt_less  png&pdf\n'...
    '     Continue? [Ctrl+C to quit]\n'])
pause();

% Load sim results, matrix is called awgfit_sigma_time
load('results/resultsSave/bessel_awgfit_sigma_time_lowrt.mat');
sim = awgfit_sigma_time;

% Load data
data = load('results/resultsSave/data_EMGfit_lowrt_sampVSsnr.mat');
eqp_order = ['D24_10ns';'D24_16ns'];
eqp_order = cellstr(eqp_order);
data = orderfields(data,eqp_order);
eqp = fieldnames(data); % print if want to see order of equipment

% Risetimes
sim_trise_samples = [1.0:0.1:2.2 2.4:0.2:4.0 4.5 5.0];
data_trise_samples = [1.0 1.6]; % Measured risetimes would be nice
lenSim = length(sim_trise_samples);
lenData = length(data_trise_samples);

% SNR points for the sim values
SNRpts = [10:5:100];

% Legend strings
data_leg_str = [strcat('100MHz data, tr=',sprintf('%.1f',data_trise_samples(1)));
                strcat('100MHz data, tr=',sprintf('%.1f',data_trise_samples(2)))];
data_leg_str = cellstr(data_leg_str);
data_markers = ['o';'x';'s';'+';'d';'*';'p';'h';'^';'>';'v';'<'];
data_markers = cellstr(data_markers);
data_markers = data_markers';

% Divide resolutions by risetimes
data_reduced = data;
sim_reduced = sim;
for i = 1:lenData
    data_reduced.(eqp{i})(:,2) = data.(eqp{i})(:,2) ./ sqrt(data_trise_samples(i));
end
for i = 1:lenSim
    sim_reduced(1,:,i) = sim(1,:,i) ./ sqrt(sim_trise_samples(i));
end


% Figure for regular plot, sigma[samples] vs SNR[dB]
f1 = figure('Position', [200, 200, 1000, 500]);

for i = 1:lenData
    semilogy(data.(eqp{i})(:,1),data.(eqp{i})(:,2),data_markers{i},...
        'Color','r','DisplayName',data_leg_str{i});
    hold on;
end

for i = 1:lenSim
    semilogy(SNRpts,sim(1,:,i),'Color',[0 i/lenSim 1-i/lenSim],...
        'LineStyle','-','DisplayName',sprintf('sim, tr=%.1f',sim_trise_samples(i)));
    hold on;
end

title('\bf{\sigma_t vs. SNR}','FontSize',14);
axis([10 100 1E-6 10]);
grid on;
xlabel('\bf{SNR [dB]}','FontSize',13);
ylabel('\bf{\sigma_t [samples]}','FontSize',13);
set(gca,'LooseInset',get(gca,'TightInset'));
legend('Location','eastoutside');
set(f1,'Units','Inches');
pos = get(f1,'Position');
set(f1,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)]);
print(f1,'results/figures/sim_data_fits_lowrt.pdf','-dpdf','-r0')
print(f1,'results/figures/sim_data_fits_lowrt.png','-dpng','-r0')


% Figure for sigma divided by sqrt(risetime)--reduced
f2 = figure('Position', [200, 200, 1000, 500]);

for i = 1:lenData
    semilogy(data_reduced.(eqp{i})(:,1),data_reduced.(eqp{i})(:,2),data_markers{i},...
        'Color','r','DisplayName',data_leg_str{i});
    hold on;
end

for i = 1:lenSim
    semilogy(SNRpts,sim_reduced(1,:,i),'Color',[0 i/lenSim 1-i/lenSim],...
        'LineStyle','-','DisplayName',sprintf('sim, tr=%.1f',sim_trise_samples(i)));
    hold on;
end

title('\bf{\sigma_t vs. SNR}','FontSize',14);
axis([10 100 1E-6 10]);
grid on;
xlabel('\bf{SNR [dB]}','FontSize',13);
ylabel('\bf{\sigma_t [samples^{1/2} t_{rise}^{1/2}]}','FontSize',13);
set(gca,'LooseInset',get(gca,'TightInset'));
legend('Location','eastoutside');
set(f2,'Units','Inches');
pos = get(f2,'Position');
set(f2,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)]);
print(f2,'results/figures/sim_data_fits_lowrt_reduced.pdf','-dpdf','-r0')
print(f2,'results/figures/sim_data_fits_lowrt_reduced.png','-dpng','-r0')


% Regular sigma but only tr=1.0, 1.6
f3 = figure('Position', [200, 200, 550, 500]);
for i = 1:lenData
    semilogy(data.(eqp{i})(:,1),data.(eqp{i})(:,2),data_markers{i},...
        'Color','r','DisplayName',data_leg_str{i});
    hold on;
end
spec_sim_idx = [1, 7]; % indeces for tr of 1.0 and 1.6 samples
for i = 1:6:7
    semilogy(SNRpts,sim(1,:,i),'Color',[0 i/lenSim 1-i/lenSim],...
        'LineStyle','-','DisplayName',sprintf('sim, tr=%.1f',sim_trise_samples(i)));
    hold on;
end
title('\bf{\sigma_t vs. SNR}','FontSize',14);
axis([10 100 1E-6 10]);
grid on;
xlabel('\bf{SNR [dB]}','FontSize',13);
ylabel('\bf{\sigma_t [samples]}','FontSize',13);
set(gca,'LooseInset',get(gca,'TightInset'));
legend('Location','northeast');
set(f3,'Units','Inches');
pos = get(f3,'Position');
set(f3,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)]);
print(f3,'results/figures/sim_data_fits_lowrt_less.pdf','-dpdf','-r0')
print(f3,'results/figures/sim_data_fits_lowrt_less.png','-dpng','-r0')


