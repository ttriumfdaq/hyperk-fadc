function [GNV2eqn,name,nparam] = funcGNV2()
% Generalized Normal V.2 function used for fitting
name = 'GNV2';
nparam = 5;

psi = '-log(1 - c*(x - b)/a) / c';
phi = 'exp(-psi*psi/f) / sqrt(2*3.14159)';
rho = '(x-b)/f';
GNV2eqn = 'c + d*exp(-(x-b)*(x-b)/(a*a)/e) / (a*sqrt(2*3.14159))';
%If c (kappa) is not zero
%GNV2eqn = ['d + e*exp(-(log(1 - c*(x - b)/a))*(log(1 - c*(x - b)/a)) / (c*c*f))'...
%    '/ sqrt(2*3.14159) / (a - c*(x-b))'...
%    '* exp(-0.5*(x-b)/f*(x-b)/f)'];
end

