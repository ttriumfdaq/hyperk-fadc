function seeds = SeedSet_rtSet (fitfunc,nparam,nshaper,range)
% Function for setting parameter seed values for fits
if strcmp(fitfunc,'GNV2')
    
    seeds = zeros(nshaper,nparam);
    seeds(1,:) = [0.7 0 0 1.7 295.6]; %100MSPS, 15ns
    seeds(2,:) = [1.05 0 0 2.646 300.5]; %100MSPS, 30ns

elseif strcmp(fitfunc,'EMG')
    
    scale = range/200.0;

    seeds = zeros(nshaper,nparam);
        for l = 1:nshaper
            seeds(l,:) = [0.004342 0 8.996*scale 4.5545*scale 0];
        end
    
end

end

