global RESULTS_FOLDER;
set(0,'defaultlinelinewidth',1.5);
set(0,'defaultlinemarkersize',7);
fprintf(['     This will overwrite the images in results/figures/:\n'...
    '          sim_data_fits  png&pdf\n'...
    '          sim_data_fits_reduced  png&pdf\n'...
    '          sim_data_fits_100mhz  png&pdf\n'...
    '          sim_data_fits_250mhz  png&pdf\n'...
    '          sim_data_fits_500mhz  png&pdf\n'...
    '     Continue? [Ctrl+C to quit]\n'])
pause();

% Load sim results, matrix is called awgfit_sigma_time
load(strcat(RESULTS_FOLDER,'/resultsSave/bessel_awgfit_sigma_time_eqpmnt.mat'));
sim = awgfit_sigma_time;

% Load data
data = load(strcat(RESULTS_FOLDER,'/resultsSave/data_EMGfit_sampVSsnr_old.mat'));
eqp_order = ['D24_15emg';'D24_30emg';'V20_15emg';'V20_30emg';'V30_15emg';'V30_30emg'];
eqp_order = cellstr(eqp_order);
data = orderfields(data,eqp_order);
eqp = fieldnames(data); % print if want to see order of equipment
data.(eqp{1})(1,2);

% Risetimes
sim_trise_samples = [1.953 3.537 4.954 8.89 9.916 17.78];
data_trise_samples = [1.953 3.537 4.954 8.89 9.916 17.78];
lenSim = length(sim_trise_samples);
lenData = length(data_trise_samples);

% SNR points for the sim values
SNRpts = [10:5:100];

% Legend strings
data_leg_str = ['100MHz:15ns data, tr=',num2str(data_trise_samples(1),3);
                '100MHz:30ns data, tr=',num2str(data_trise_samples(2),3);
                '250MHz:15ns data, tr=',num2str(data_trise_samples(3),3);
                '250MHz:30ns data, tr=',num2str(data_trise_samples(4),3);
                '500MHz:15ns data, tr=',num2str(data_trise_samples(5),3);
                '500MHz:30ns data, tr=',num2str(data_trise_samples(6),3)];
data_leg_str = cellstr(data_leg_str);
sim_leg_str = strrep(data_leg_str,'data','sim');

data_markers = ['o';'x';'s';'+';'d';'*';'p';'h';'^';'>';'v';'<'];
data_markers = cellstr(data_markers);
data_markers = data_markers';
sim_lines = ['- ';'--';'- ';'--';'- ';'--']; % need spaces so that matrix dimension is consistent
sim_lines = cellstr(sim_lines);
sim_lines = sim_lines';

% Divide resolutions by risetimes
data_reduced = data;
sim_reduced = sim;
for i = 1:lenData
    data_reduced.(eqp{i})(:,2) = data.(eqp{i})(:,2) ./ sqrt(data_trise_samples(i));
end
for i = 1:lenSim
    sim_reduced(1,:,i) = sim(1,:,i) ./ sqrt(sim_trise_samples(i));
end

% Figure for regular plot, sigma[samples] vs SNR[dB]
f1 = figure('Position', [200, 200, 1000, 500]);

for i = 1:lenData
    semilogy(data.(eqp{i})(:,1),data.(eqp{i})(:,2),data_markers{i},...
        'Color',[0 i/lenData 1-i/lenData],'DisplayName',data_leg_str{i});
    hold on;
end

for i = 1:lenSim
    semilogy(SNRpts,sim(1,:,i),'Color',[0 i/lenSim 1-i/lenSim],...
        'LineStyle',sim_lines{i},'DisplayName',sim_leg_str{i});
end

title('\bf{\sigma_t vs. SNR}','FontSize',14);
axis([10 50 1E-3 10]);
grid on;
xlabel('\bf{SNR [dB]}','FontSize',13);
ylabel('\bf{\sigma_t [samples]}','FontSize',13);
set(gca,'LooseInset',get(gca,'TightInset'));
legend('Location','eastoutside');
set(f1,'Units','Inches');
pos = get(f1,'Position');
set(f1,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)]);
print(f1,'results/figures/sim_data_fits.pdf','-dpdf','-r0')
print(f1,'results/figures/sim_data_fits.png','-dpng','-r0')


% Figure for sigma divided by sqrt(risetime)--reduced
f2 = figure('Position', [200, 200, 1000, 500]);

for i = 1:lenData
    semilogy(data_reduced.(eqp{i})(:,1),data_reduced.(eqp{i})(:,2),data_markers{i},...
        'Color',[0 i/lenData 1-i/lenData],'DisplayName',data_leg_str{i});
    hold on;
end

for i = 1:lenSim
    semilogy(SNRpts,sim_reduced(1,:,i),'Color',[0 i/lenSim 1-i/lenSim],...
        'LineStyle',sim_lines{i},'DisplayName',sim_leg_str{i});
end

title('\bf{\sigma_t vs. SNR}','FontSize',14);
axis([10 50 1E-3 10]);
grid on;
xlabel('\bf{SNR [dB]}','FontSize',13);
ylabel('\bf{\sigma_t [samples^{1/2} t_{rise}^{1/2}]}','FontSize',13);
set(gca,'LooseInset',get(gca,'TightInset'));
legend('Location','eastoutside');
set(f2,'Units','Inches');
pos = get(f2,'Position');
set(f2,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)]);
print(f2,'results/figures/sim_data_fits_reduced.pdf','-dpdf','-r0')
print(f2,'results/figures/sim_data_fits_reduced.png','-dpng','-r0')



% Individual figures for different ADCs
%
% 100MHz
f3 = figure('Position', [200, 200, 550, 500]);
for i = 1:2
    semilogy(data.(eqp{i})(:,1),data.(eqp{i})(:,2),data_markers{i},...
        'Color','r','DisplayName',data_leg_str{i});
    hold on;
end
for i = 1:2
    semilogy(SNRpts,sim(1,:,i),'Color','r',...
        'LineStyle',sim_lines{i},'DisplayName',sim_leg_str{i});
end
title('\bf{\sigma_t vs. SNR}','FontSize',14);
axis([10 50 1E-3 10]);
grid on;
xlabel('\bf{SNR [dB]}','FontSize',13);
ylabel('\bf{\sigma_t [samples]}','FontSize',13);
set(gca,'LooseInset',get(gca,'TightInset'));
legend('Location','northeast');
set(f3,'Units','Inches');
pos = get(f3,'Position');
set(f3,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)]);
print(f3,'results/figures/sim_data_fits_100mhz.pdf','-dpdf','-r0')
print(f3,'results/figures/sim_data_fits_100mhz.png','-dpng','-r0')



% 250MHz
f4 = figure('Position', [200, 200, 550, 500]);
for i = 3:4
    semilogy(data.(eqp{i})(:,1),data.(eqp{i})(:,2),data_markers{i},...
        'Color','b','DisplayName',data_leg_str{i});
    hold on;
end
for i = 3:4
    semilogy(SNRpts,sim(1,:,i),'Color','b',...
        'LineStyle',sim_lines{i},'DisplayName',sim_leg_str{i});
end
title('\bf{\sigma_t vs. SNR}','FontSize',14);
axis([10 50 1E-3 10]);
grid on;
xlabel('\bf{SNR [dB]}','FontSize',13);
ylabel('\bf{\sigma_t [samples]}','FontSize',13);
set(gca,'LooseInset',get(gca,'TightInset'));
legend('Location','northeast');
set(f4,'Units','Inches');
pos = get(f4,'Position');
set(f4,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)]);
print(f4,'results/figures/sim_data_fits_250mhz.pdf','-dpdf','-r0')
print(f4,'results/figures/sim_data_fits_250mhz.png','-dpng','-r0')


% 500MHz
f5 = figure('Position', [200, 200, 550, 500]);
for i = 5:6
    semilogy(data.(eqp{i})(:,1),data.(eqp{i})(:,2),data_markers{i},...
        'Color','k','DisplayName',data_leg_str{i});
    hold on;
end
for i = 5:6
    semilogy(SNRpts,sim(1,:,i),'Color','k',...
        'LineStyle',sim_lines{i},'DisplayName',sim_leg_str{i});
end
title('\bf{\sigma_t vs. SNR}','FontSize',14);
axis([10 50 1E-3 10]);
grid on;
xlabel('\bf{SNR [dB]}','FontSize',13);
ylabel('\bf{\sigma_t [samples]}','FontSize',13);
set(gca,'LooseInset',get(gca,'TightInset'));
legend('Location','northeast');
set(f5,'Units','Inches');
pos = get(f5,'Position');
set(f5,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)]);
print(f5,'results/figures/sim_data_fits_500mhz.pdf','-dpdf','-r0')
print(f5,'results/figures/sim_data_fits_500mhz.png','-dpng','-r0')

