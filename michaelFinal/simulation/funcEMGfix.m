function [EMGeqn] = funcEMGfix(p)

a = num2str(p(1));
d = num2str(p(2));
g = num2str(p(3));

EMGeqn = ['h + c*a*g*exp((b+0.5*d*d/g - x)/g)',...
    '*erfc((b+d*d/g - x)/sqrt(2)/d)'];

EMGeqn = strrep(EMGeqn,'a',a);
EMGeqn = strrep(EMGeqn,'d',d);
EMGeqn = strrep(EMGeqn,'g',g);

end