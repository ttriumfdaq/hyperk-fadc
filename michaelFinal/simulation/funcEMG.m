function [EMGeqn,name,nparam] = funcEMG()
name = 'EMG';
nparam = 5;

EMGeqn = ['e + a*d*exp((b+0.5*c*c/d - x)/d)',...
    '*erfc((b+c*c/d - x)/sqrt(2)/c)'];

end

