% Skrypt do wy�wietlania wynik�w badania dyskryminatora CF pod k�tem w�asno�ci szumowych. Stara wersja, pokazuj�ca rozdzielczo�ci czasowe
% normalizowane do czasu narastania.

% Zmienne globalne
global RESULTS_FOLDER;
global FIGURES_FOLDER;
global SAVE_PLOTS_TO_PNG;
global SAVE_PLOTS_TO_EPS;
global TEX_CREATE_FILE;
global TEX_PLOTS_PER_PAGE;

% Style linii do wy�wietlania wynik�w
my_linestyles = {'-', '-.', ':', '--'};

% Struktura na potrzeby tworzenia raportu w formacie LaTeX
latex_figures = {};
latex_figures_cnt = 0;
latex_plots_cnt = 0;
latex_report_file = '';

% Za�aduj wyniki
load(strcat(RESULTS_FOLDER,'/bessel_cf_sigma_time.mat'));
load(strcat(RESULTS_FOLDER,'/bessel_cf_analyze.mat'));

fprintf(1,'Available results:\n\n');
disp(info_cf_sigma_time);
fprintf(1,'\n');

fprintf(1,'-- Choose your analysis:\n');
fprintf(1,' 1: CF delay vs risetime (order = const, SNR = const)\n');
fprintf(1,' 2: Sigma time vs risetime and sigma time vs SNR (order = const)\n');
fprintf(1,' 3: Sigma time vs order and sigma time vs risetime (SNR = const)\n');
fprintf(1,' 4: Sigma time vs order and sigma time vs SNR (risetime = const)\n');
my_analysis = input('-- Your choice: ');

% Domy�lne wy�wietlanie okienek
if (SAVE_PLOTS_TO_PNG > 0 || SAVE_PLOTS_TO_EPS > 0)
    set(0, 'DefaultFigureVisible', 'off')
end

% ----------------------------------------------------------------------------------------------------------------------------------------------
% Analiza optymalnego czasu op�nienia dyskryminatora sta�ofrakcyjnego
% ----------------------------------------------------------------------------------------------------------------------------------------------

if my_analysis == 1
	
	% Nazwa foldera dla plik�w z rysunkami
	fig_sub_folder_name = 'cf_delay';	    
    	
    % Zapytaj o minimalny rz�d filtra
    min_order = input(' -- Filter order (min): ');
    min_order = find(info_cf_sigma_time.filter_order == min_order);
    
    if isempty(min_order)
        fprintf(1,'Error: no data for this filter order!\n');
        return;
    end

    % Zapytaj o maksymalny rz�d filtra
    max_order = input(' -- Filter order (max): ');
    max_order = find(info_cf_sigma_time.filter_order == max_order);
    
    if isempty(max_order)
        fprintf(1,'Error: no data for this filter order!\n');
        return;
    end
    
    min_risetime = input(' -- Minimum risetime: ');
    min_risetime = find(info_cf_sigma_time.risetime == min_risetime);
            
	if isempty(min_risetime)
        fprintf(1,'Error: this risetime was not simulated!\n');
        return;
    end		
    
    % P�tla po wszystkich rz�dach filtr�w
    for my_order = min_order:max_order
        
        % Wyczy�� struktury na potrzeby tworzenia raportu w formacie LaTeX
        latex_figures = {};
        latex_figures_cnt = 0;
        latex_plots_cnt = 0;
        
        %
        % Poka� legend� od kolor�w - na odr�bnej 'figure', bo na wykresach jest zbyt du�o krzywych, �eby doda� opisy
        %
        fprintf(1, 'Preparing line style and color legend\n');

        h = figure();
        hold off;
        cmap = get(gca,'ColorOrder');	% Pobierz bie��c� map� kolor�w
        my_color = 1;
        my_line = 1;

        total_lines = length(min_risetime:size(cf_sigma_time, 3));

        plot_y = [total_lines total_lines];
        plot_x = [0 1];

        text_x_min = 0.03;
        text_x_max = 0.87;
        text_x_step = (text_x_max - text_x_min) / (size(cmap,1)-1);

        text_x = text_x_min;

        for j=min_risetime:size(cf_sigma_time, 3)  % P�tla po wszystkich czasach narastania

            plot(plot_x, plot_y, 'LineWidth', 1, 'Color', cmap(my_color,:), 'LineStyle', my_linestyles{my_line});
            text_to_display = sprintf('tr = %.1f', info_cf_sigma_time.risetime(j));
            text(text_x, plot_y(1), text_to_display ,'VerticalAlignment','middle', 'BackgroundColor','w', 'Color', cmap(my_color,:));

            plot_y = plot_y - 1;
            text_x = text_x + text_x_step;
            if (text_x > text_x_max)
                text_x = text_x_min;
            end;

            hold on;

            my_color = my_color+1;
            if my_color > length(cmap)
                my_color = 1;
                my_line = my_line + 1;
            end;      
        end;

        set(gca, 'XTickLabel', []);
        set(gca, 'YTickLabel', []);
        axis([0 1 0 total_lines+1]);	
        title(sprintf('Line Style and Color Legend (order = %d, param: t_{rise} [samples])',info_cf_sigma_time.filter_order(my_order)));

        if (SAVE_PLOTS_TO_PNG > 0 || SAVE_PLOTS_TO_EPS > 0)
            % Sformatuj wykres do wydruku
            FormatAxes();

            if (TEX_CREATE_FILE > 0)        
                latex_figures_cnt = latex_figures_cnt + 1;
                latex_plots_cnt = latex_plots_cnt + 1;
                latex_figures{latex_figures_cnt}.caption = sprintf(['Line style and color legend for figures with plots of the time resolution ' ...
                                                                    'as a function of delay of the inverted waveform, for various number of samples at the rising edge ' ...
                                                                    '(filter order = %d).'], info_cf_sigma_time.filter_order(my_order));

                latex_figures{latex_figures_cnt}.label = sprintf('sigma_time_vs_delay-N_%d-legend', info_cf_sigma_time.filter_order(my_order));
            end;

            % Zapisz obrazki do plik�w (format png)
            if SAVE_PLOTS_TO_PNG > 0
                filename_png = sprintf('%s/%s/sigma_time_vs_delay-N_%d-legend.png', FIGURES_FOLDER, fig_sub_folder_name, info_cf_sigma_time.filter_order(my_order));	

                fprintf(1,'Writing PNG file: %s\n', filename_png);
                print(h, filename_png, '-dpng', '-r300');

                if (TEX_CREATE_FILE > 0)            
                    latex_figures{latex_figures_cnt}.filenames{latex_plots_cnt} = filename_png;          
                end
            end;

            % Zapisz obrazki do plik�w (format eps)
            if SAVE_PLOTS_TO_EPS > 0
                filename_eps = sprintf('%s/%s/sigma_time_vs_delay-N_%d-legend.eps', FIGURES_FOLDER, fig_sub_folder_name, info_cf_sigma_time.filter_order(my_order));

                fprintf(1,'Writing EPS file: %s\n', filename_eps);
                print(h, filename_eps, '-depsc', '-tiff', '-r300');

                if (TEX_CREATE_FILE > 0)
                    latex_figures{latex_figures_cnt}.filenames{latex_plots_cnt} = filename_eps;          
                end
            end;

            close(h);
        end; 

        %
        % Poka� wykresy rozdzielczo�ci czasowej w funkcji op�nienia dyskryminatora CF
        %

        % Wyczy�� licznik wykres�w dla latex-a
        latex_plots_cnt = 0;
        caption_min_SNR = 0;

        % Jako wsp�rz�dn� x wy�wietlimy op�nienie dyskryminatora CF - nie b�dzie si� to zmienia� przy r�nych SNR-ach
        plot_x = info_cf_sigma_time.cf_delay(my_order,:);

        for i=1:length(info_cf_sigma_time.SNR_dB)

            fprintf(1, 'Showing results for order = %d, SNR = %d [dB]\n', info_cf_sigma_time.filter_order(my_order), int32(info_cf_sigma_time.SNR_dB(i)));

            h = figure();
            hold off;
            cmap = get(gca,'ColorOrder');	% Pobierz bie��c� map� kolor�w
            my_color = 1;
            my_line = 1;
			
			% Sprawd�, czy bedziemy stosowa� skal� liniow� czy logarytmiczn�
			plot_mode_linear = (max(max(cf_sigma_time(my_order,i,:,:))) / min(min(cf_sigma_time(my_order,i,:,:))) < 10);
			%keyboard;			

            for j=min_risetime:size(cf_sigma_time, 3)  % P�tla po wszystkich czasach narastania

                plot_y = cf_sigma_time(my_order,i,j,:) ./ info_cf_sigma_time.risetime(j);   % Normalizuj rozdzielczo�� czasow� do czasu narastania
                plot_y = reshape(plot_y, length(plot_y), 1);
				if (plot_mode_linear)
					plot(plot_x, plot_y, 'LineWidth', 1, 'Color', cmap(my_color,:), 'LineStyle', my_linestyles{my_line});
				else
					semilogy(plot_x, plot_y, 'LineWidth', 1, 'Color', cmap(my_color,:), 'LineStyle', my_linestyles{my_line});
				end
                hold on;

                my_color = my_color+1;
                if my_color > length(cmap)
                    my_color = 1;
                    my_line = my_line + 1;
                end;      
            end;

            % Narysuj lini� dla maksymalnego nachylenia krzywej CF
            g = get(gca);
            plot([max_slope(my_order,2) max_slope(my_order,2)], [g.YLim(1) g.YLim(2)], 'k:');
            axis([min(info_cf_sigma_time.cf_delay(my_order,:))-0.1, max(info_cf_sigma_time.cf_delay(my_order,:))+0.1, g.YLim(1) g.YLim(2)]);

            xlabel('CF delay [t_{tise}]');
            ylabel('\sigma_{time} [t_{rise}]');
            title(sprintf('\\sigma_{time} vs Pulse Delay (order = %d, SNR = %d dB)',info_cf_sigma_time.filter_order(my_order),int32(info_cf_sigma_time.SNR_dB(i))));

            if (SAVE_PLOTS_TO_PNG > 0 || SAVE_PLOTS_TO_EPS > 0)        
                % Sformatuj wykres do wydruku
                FormatAxes();

                if (TEX_CREATE_FILE > 0)
                    % Sprawd�, czy mamy utworzy� now� stron� z rysunkami
                    if (latex_plots_cnt == 0)
                        latex_figures_cnt = latex_figures_cnt + 1;
                        caption_min_SNR = int32(info_cf_sigma_time.SNR_dB(i));  % Zapami�taj pierwszy SNR do opisu rysunku zbiorczego
                    end;  

                    % Uwzgl�dnij bie��cy wykres
                    latex_plots_cnt = latex_plots_cnt+1;
                end;

                % Zapisz obrazki do plik�w (format png)
                if SAVE_PLOTS_TO_PNG > 0
                    filename_png = sprintf('%s/%s/sigma_time_vs_delay-N_%d-SNR_%ddB.png', FIGURES_FOLDER, fig_sub_folder_name, info_cf_sigma_time.filter_order(my_order), int32(info_cf_sigma_time.SNR_dB(i)));	

                    fprintf(1,'Writing PNG file: %s\n', filename_png);
                    print(h, filename_png, '-dpng', '-r300');

                    if (TEX_CREATE_FILE > 0)
                        latex_figures{latex_figures_cnt}.filenames{latex_plots_cnt} = filename_png;
                    end
                end;

                % Zapisz obrazki do plik�w (format eps)
                if SAVE_PLOTS_TO_EPS > 0
                    filename_eps = sprintf('%s/%s/sigma_time_vs_delay-N_%d-SNR_%ddB.eps', FIGURES_FOLDER, fig_sub_folder_name, info_cf_sigma_time.filter_order(my_order), int32(info_cf_sigma_time.SNR_dB(i)));

                    fprintf(1,'Writing EPS file: %s\n', filename_eps);
                    print(h, filename_eps, '-depsc', '-tiff', '-r300');

                    if (TEX_CREATE_FILE > 0)
                        latex_figures{latex_figures_cnt}.filenames{latex_plots_cnt} = filename_eps;
                    end
                end;

                if (TEX_CREATE_FILE > 0)
                    % Sprawd�, czy mamy maksymaln� liczb� wykres�w na stron� i je�li tak, to dodaj opis rysunku i resetuj licznik rysunk�w
                    if (latex_plots_cnt == TEX_PLOTS_PER_PAGE || i == length(info_cf_sigma_time.SNR_dB))

                        latex_plots_cnt = 0;
                        latex_figures{latex_figures_cnt}.caption = sprintf(['Results of simulations of the time resolution of the digital constant fraction discriminator at various delays of the '...
                                                                            'inverted waveform (expressed in multiples of the pulse rise time). Results shown for Bessel-type shaper of order %d and ' ...
                                                                            'SNR %d dB to %d dB. Black vertical line marks the delay at which maximum slope of the constant-fraction waveform is achieved ' ...
																			'at the intersection point.'], info_cf_sigma_time.filter_order(my_order), caption_min_SNR, int32(info_cf_sigma_time.SNR_dB(i)));
                        latex_figures{latex_figures_cnt}.label = sprintf('sigma_time_vs_delay-N_%d-%ddB-to-%ddB', info_cf_sigma_time.filter_order(my_order), caption_min_SNR, int32(info_cf_sigma_time.SNR_dB(i)));
                    end
                end            

                close(h);
            end;
        end; 

        if (TEX_CREATE_FILE > 0)
            latex_report_file = sprintf('%s/%s/report-%s-N%d.tex', FIGURES_FOLDER, fig_sub_folder_name, fig_sub_folder_name, info_cf_sigma_time.filter_order(my_order));
            latex_create_report(latex_report_file, latex_figures);
        end
    end;
end;

% ----------------------------------------------------------------------------------------------------------------------------------------------
% Analiza optymalnego czasu narastania przy r�nych SNR-ach
% ----------------------------------------------------------------------------------------------------------------------------------------------

if my_analysis == 2

	% Nazwa foldera dla plik�w z rysunkami
	fig_sub_folder_name = 'cf_risetime_SNR';	
	
    % Zapytaj o minimalny rz�d filtra
    min_order = input(' -- Filter order (min): ');
    min_order = find(info_cf_sigma_time.filter_order == min_order);
    
    if isempty(min_order)
        fprintf(1,'Error: no data for this filter order!\n');
        return;
    end

    % Zapytaj o maksymalny rz�d filtra
    max_order = input(' -- Filter order (max): ');
    max_order = find(info_cf_sigma_time.filter_order == max_order);
    
    if isempty(max_order)
        fprintf(1,'Error: no data for this filter order!\n');
        return;
    end
    
%     min_risetime = input(' -- Minimum risetime: ');
%     min_risetime = find(info_cf_sigma_time.risetime == min_risetime);
%             
%     if isempty(min_risetime)
%         fprintf(1,'Error: this risetime was not simulated!\n');
%         return;
%     end
% 
%     min_SNR = input(' -- Minimum SNR: ');
%     min_SNR = find(info_cf_sigma_time.SNR_dB == min_SNR);
%             
%     if isempty(min_SNR)
%         fprintf(1,'Error: this SNR was not simulated!\n');
%         return;
%     end
    min_risetime = 1;
    min_SNR = 1;

    % Powiedz, co robimy
    fprintf(1, 'Preparing data...\n');
    
    % Przygotuj dane do wykres�w
    size_SNR = size(cf_sigma_time,2);
    size_risetime = size(cf_sigma_time,3);
    
    plot_data = zeros(length(min_order:max_order), size_SNR, size_risetime);
    for N = min_order:max_order
        for i=1:size_SNR
            for j=1:size_risetime
                plot_data(N,i,j) = min(cf_sigma_time(N,i,j,:)) / info_cf_sigma_time.risetime(j);
            end;
        end;
    end;
    
	%
	% Poka� legend� od kolor�w dla wykres�w z parametryzacj� po czasie narastania - na odr�bnej 'figure', bo na wykresach jest zbyt du�o krzywych, �eby doda� opisy
	%
	fprintf(1, 'Preparing line style and color legend\n');
	
    h = figure();
	hold off;
	cmap = get(gca,'ColorOrder');	% Pobierz bie��c� map� kolor�w
	my_color = 1;
	my_line = 1;
	
	total_lines = length(min_risetime:size_risetime);

	plot_y = [total_lines total_lines];
	plot_x = [0 1];
	
	text_x_min = 0.03;
	text_x_max = 0.87;
	text_x_step = (text_x_max - text_x_min) / (size(cmap,1)-1);
	
	text_x = text_x_min;

	for j=min_risetime:size_risetime  % P�tla po wszystkich czasach narastania

		plot(plot_x, plot_y, 'LineWidth', 1, 'Color', cmap(my_color,:), 'LineStyle', my_linestyles{my_line});
		text_to_display = sprintf('tr = %.1f', info_cf_sigma_time.risetime(j));
		text(text_x, plot_y(1), text_to_display ,'VerticalAlignment','middle', 'BackgroundColor','w', 'Color', cmap(my_color,:));
		
		plot_y = plot_y - 1;
		text_x = text_x + text_x_step;
		if (text_x > text_x_max)
			text_x = text_x_min;
		end;
		
		hold on;

		my_color = my_color+1;
		if my_color > length(cmap)
			my_color = 1;
			my_line = my_line + 1;
		end;      
	end;
	
	set(gca, 'XTickLabel', []);
	set(gca, 'YTickLabel', []);
	axis([0 1 0 total_lines+1]);	
	title('Line Style and Color Legend (param: t_{rise} [samples])');

    if (SAVE_PLOTS_TO_PNG > 0 || SAVE_PLOTS_TO_EPS > 0)        
        FormatAxes();
        
        if (TEX_CREATE_FILE > 0)        
            latex_figures_cnt = latex_figures_cnt + 1;
            latex_plots_cnt = latex_plots_cnt + 1;
            latex_figures{latex_figures_cnt}.caption = ['Line style and color legend for figures with plots of the time resolution of a digital constant fraction discriminator ' ...
                                                        'as a function of signal to noise ratio, for various numbers of samples at the rising edge.'];

            latex_figures{latex_figures_cnt}.label = 'sigma_time_vs_SNR-legend';
        end;        
    
        % Zapisz obrazki do plik�w (format png)
        if SAVE_PLOTS_TO_PNG > 0
            filename_png = sprintf('%s/%s/sigma_time_vs_SNR-legend.png', FIGURES_FOLDER, fig_sub_folder_name);	

            fprintf(1,'Writing PNG file: %s\n', filename_png);
            print(h, filename_png, '-dpng', '-r300');
            
            if (TEX_CREATE_FILE > 0)            
                latex_figures{latex_figures_cnt}.filenames{latex_plots_cnt} = filename_png;          
            end    
        end;

        % Zapisz obrazki do plik�w (format eps)
        if SAVE_PLOTS_TO_EPS > 0
            filename_eps = sprintf('%s/%s/sigma_time_vs_SNR-legend.eps', FIGURES_FOLDER, fig_sub_folder_name);

            fprintf(1,'Writing EPS file: %s\n', filename_eps);
            print(h, filename_eps, '-depsc', '-tiff', '-r300');
            
            if (TEX_CREATE_FILE > 0)
                latex_figures{latex_figures_cnt}.filenames{latex_plots_cnt} = filename_eps;          
            end            
        end;	
        
        close(h);
    end;
    
    % Wyczy�� licznik wykres�w dla latex-a
    latex_plots_cnt = 0;
    caption_min_order = 0;
    
    %
    % Teraz narysuj wykresy z parametryzacj� po czasie narastania
    %    
    for N = min_order:max_order 
        
        % Powiedz, co robimy
        fprintf(1, 'Showing results for order = %d (param: t_rise)\n', info_cf_sigma_time.filter_order(N));
        
        h = figure();
        hold off;
        cmap = get(gca,'ColorOrder');	% Pobierz bie��c� map� kolor�w
        my_color = 1;
        my_line = 1;    

        plot_x = info_cf_sigma_time.SNR_dB(min_SNR : size_SNR);

        for i=min_risetime:size_risetime    % P�tla po wszystkich czasach narastania

            plot_y = plot_data(N, min_SNR:size_SNR, i);   
            %keyboard;
            semilogy(plot_x, plot_y, 'LineWidth', 1, 'Color', cmap(my_color,:), 'LineStyle', my_linestyles{my_line});
            hold on;

            my_color = my_color+1;
            if my_color > length(cmap)
                my_color = 1;
                my_line = my_line + 1;
            end;      
        end;

        xlabel('SNR [dB]');
        ylabel('\sigma_{time} [t_{rise}]');
        title(sprintf('\\sigma_{time} vs SNR (order = %d, param: t_{rise})',info_cf_sigma_time.filter_order(N)));

        if (SAVE_PLOTS_TO_PNG > 0 || SAVE_PLOTS_TO_EPS > 0)        
            FormatAxes();
        
            if (TEX_CREATE_FILE > 0)
                % Sprawd�, czy mamy utworzy� now� stron� z rysunkami
                if (latex_plots_cnt == 0)
                    latex_figures_cnt = latex_figures_cnt + 1;
                    caption_min_order = int32(info_cf_sigma_time.filter_order(N));  % Zapami�taj pierwszy rz�d filtra do opisu rysunku zbiorczego
                end;  
                
                % Uwzgl�dnij bie��cy wykres
                latex_plots_cnt = latex_plots_cnt+1;
            end;
            
            % Zapisz obrazki do plik�w (format png)
            if SAVE_PLOTS_TO_PNG > 0
                filename_png = sprintf('%s/%s/sigma_time_vs_SNR-N_%d.png', FIGURES_FOLDER, fig_sub_folder_name, info_cf_sigma_time.filter_order(N));	

                fprintf(1,'Writing PNG file: %s\n', filename_png);
                print(h, filename_png, '-dpng', '-r300');

                if (TEX_CREATE_FILE > 0)
                    latex_figures{latex_figures_cnt}.filenames{latex_plots_cnt} = filename_png;
                end                
            end;

            % Zapisz obrazki do plik�w (format eps)
            if SAVE_PLOTS_TO_EPS > 0
                filename_eps = sprintf('%s/%s/sigma_time_vs_SNR-N_%d.eps', FIGURES_FOLDER, fig_sub_folder_name, info_cf_sigma_time.filter_order(N));

                fprintf(1,'Writing EPS file: %s\n', filename_eps);
                print(h, filename_eps, '-depsc', '-tiff', '-r300');
                
                if (TEX_CREATE_FILE > 0)
                    latex_figures{latex_figures_cnt}.filenames{latex_plots_cnt} = filename_eps;
                end                
            end;
            
            if (TEX_CREATE_FILE > 0)
                % Sprawd�, czy mamy maksymaln� liczb� wykres�w na stron� i je�li tak, to dodaj opis rysunku i resetuj licznik rysunk�w
                if (latex_plots_cnt == TEX_PLOTS_PER_PAGE || N == max_order)

                    latex_plots_cnt = 0;
                    latex_figures{latex_figures_cnt}.caption = sprintf(['Results of simulations of the time resolution the digital constant fraction discriminator ' ...
                                                                        'as a function of the signal to noise ratio, for various numbers of samples at the rising edge. ' ...
                                                                        'Bessel-type shaping filters with orders %d to %d'], caption_min_order, info_cf_sigma_time.filter_order(N));
                    latex_figures{latex_figures_cnt}.label = sprintf('sigma_time_vs_SNR-N_%d-to-%d', caption_min_order, info_cf_sigma_time.filter_order(N));
                end
            end                        

            close(h);
        end;        
    end;   
    
    % Przygotuj plik raportu
    if (TEX_CREATE_FILE > 0)
        latex_report_file = sprintf('%s/%s/report-sigma_time_vs_SNR.tex', FIGURES_FOLDER, fig_sub_folder_name);
        latex_create_report(latex_report_file, latex_figures);
    end                
    
    % Wyczy�� zmienne dla raportu w latex-ie
    latex_figures = {};
    latex_figures_cnt = 0;
    latex_plots_cnt = 0;
    
	%
	% Poka� legend� od kolor�w dla wykres�w z parametryzacj� po SNR - na odr�bnej 'figure', bo na wykresach jest zbyt du�o krzywych, �eby doda� opisy
	%
    h = figure();
	hold off;
	cmap = get(gca,'ColorOrder');	% Pobierz bie��c� map� kolor�w
	my_color = 1;
	my_line = 1;
	
	total_lines = length(min_SNR:size_SNR);

	plot_y = [total_lines total_lines];
	plot_x = [0 1];
	
	text_x_min = 0.03;
	text_x_max = 0.75;
	text_x_step = (text_x_max - text_x_min) / (size(cmap,1)-1);
	
	text_x = text_x_min;

	for j=min_SNR:size_SNR  % P�tla po wszystkich SNR-ach

		plot(plot_x, plot_y, 'LineWidth', 1, 'Color', cmap(my_color,:), 'LineStyle', my_linestyles{my_line});
		text_to_display = sprintf('SNR = %d dB', info_cf_sigma_time.SNR_dB(j));
		text(text_x, plot_y(1), text_to_display ,'VerticalAlignment','middle', 'BackgroundColor','w', 'Color', cmap(my_color,:));
		
		plot_y = plot_y - 1;
		text_x = text_x + text_x_step;
		if (text_x > text_x_max)
			text_x = text_x_min;
		end;
		
		hold on;

		my_color = my_color+1;
		if my_color > length(cmap)
			my_color = 1;
			my_line = my_line + 1;
		end;      
	end;
	
	set(gca, 'XTickLabel', []);
	set(gca, 'YTickLabel', []);
	axis([0 1 0 total_lines+1]);
	title('Line Style and Color Legend (param: SNR [dB])');

    if (SAVE_PLOTS_TO_PNG > 0 || SAVE_PLOTS_TO_EPS > 0)        
        FormatAxes();
    
        if (TEX_CREATE_FILE > 0)        
            latex_figures_cnt = latex_figures_cnt + 1;
            latex_plots_cnt = latex_plots_cnt + 1;
            latex_figures{latex_figures_cnt}.caption = ['Line style and color legend for figures with plots of the time resolution ' ...
                                                        'as a function of the number of samples at the rising edge, for various signal to noise ratios.'];

            latex_figures{latex_figures_cnt}.label = 'sigma_time_vs_risetime-legend';
        end;        
        
        % Zapisz obrazki do plik�w (format png)
        if SAVE_PLOTS_TO_PNG > 0
            filename_png = sprintf('%s/%s/sigma_time_vs_risetime-legend.png', FIGURES_FOLDER, fig_sub_folder_name);	

            fprintf(1,'Writing PNG file: %s\n', filename_png);
            print(h, filename_png, '-dpng', '-r300');
            
            if (TEX_CREATE_FILE > 0)            
                latex_figures{latex_figures_cnt}.filenames{latex_plots_cnt} = filename_png;          
            end                
        end;

        % Zapisz obrazki do plik�w (format eps)
        if SAVE_PLOTS_TO_EPS > 0
            filename_eps = sprintf('%s/%s/sigma_time_vs_risetime-legend.eps', FIGURES_FOLDER, fig_sub_folder_name);

            fprintf(1,'Writing EPS file: %s\n', filename_eps);
            print(h, filename_eps, '-depsc', '-tiff', '-r300');
            
            if (TEX_CREATE_FILE > 0)
                latex_figures{latex_figures_cnt}.filenames{latex_plots_cnt} = filename_eps;          
            end                        
        end;	
	
        close(h);
    end;    
    
    % Wyczy�� licznik wykres�w dla reportu w latex-ie
    latex_plots_cnt = 0;
    caption_min_order = 0;
    
    %
    % Teraz narysuj wykresy z parametryzacj� po SNR
    %    
    for N = min_order:max_order 
        
        % Powiedz, co robimy
        fprintf(1, 'Showing results for order = %d (param: SNR)\n', info_cf_sigma_time.filter_order(N));       
        
        %
		% Wykres z parametryzacj� po SNR
		%
        h = figure();
        hold off;
        cmap = get(gca,'ColorOrder');	% Pobierz bie��c� map� kolor�w
        my_color = 1;
        my_line = 1;    

        plot_x = info_cf_sigma_time.risetime(min_risetime : size_risetime);

        for i=min_SNR:size_SNR   % P�tla po wszystkich czasach narastania

            plot_y = plot_data(N, i, min_risetime:size_risetime);   
            plot_y = reshape(plot_y, 1, length(plot_y));
            %keyboard;
            semilogy(plot_x, plot_y, 'LineWidth', 1, 'Color', cmap(my_color,:), 'LineStyle', my_linestyles{my_line});
            hold on;

            my_color = my_color+1;
            if my_color > length(cmap)
                my_color = 1;
                my_line = my_line + 1;
            end;      
        end;

        xlabel('t_{rise} [samples]');
        ylabel('\sigma_{time} [t_{rise}]');
        title(sprintf('\\sigma_{time} vs t_{rise} (order = %d, param: SNR)',info_cf_sigma_time.filter_order(N)));
        
        if (SAVE_PLOTS_TO_PNG > 0 || SAVE_PLOTS_TO_EPS > 0)        
            FormatAxes();

            if (TEX_CREATE_FILE > 0)
                % Sprawd�, czy mamy utworzy� now� stron� z rysunkami
                if (latex_plots_cnt == 0)
                    latex_figures_cnt = latex_figures_cnt + 1;
                    caption_min_order = int32(info_cf_sigma_time.filter_order(N));  % Zapami�taj pierwszy rz�d filtra do opisu rysunku zbiorczego
                end;  
                
                % Uwzgl�dnij bie��cy wykres
                latex_plots_cnt = latex_plots_cnt+1;
            end;
            
            % Zapisz obrazki do plik�w (format png)
            if SAVE_PLOTS_TO_PNG > 0
                filename_png = sprintf('%s/%s/sigma_time_vs_risetime-N_%d.png', FIGURES_FOLDER, fig_sub_folder_name, info_cf_sigma_time.filter_order(N));	

                fprintf(1,'Writing PNG file: %s\n', filename_png);
                print(h, filename_png, '-dpng', '-r300');
                
                if (TEX_CREATE_FILE > 0)
                    latex_figures{latex_figures_cnt}.filenames{latex_plots_cnt} = filename_png;
                end                                
            end;

            % Zapisz obrazki do plik�w (format eps)
            if SAVE_PLOTS_TO_EPS > 0
                filename_eps = sprintf('%s/%s/sigma_time_vs_risetime-N_%d.eps', FIGURES_FOLDER, fig_sub_folder_name, info_cf_sigma_time.filter_order(N));

                fprintf(1,'Writing EPS file: %s\n', filename_eps);
                print(h, filename_eps, '-depsc', '-tiff', '-r300');
                
                if (TEX_CREATE_FILE > 0)
                    latex_figures{latex_figures_cnt}.filenames{latex_plots_cnt} = filename_eps;
                end                                
            end;
            
            if (TEX_CREATE_FILE > 0)
                % Sprawd�, czy mamy maksymaln� liczb� wykres�w na stron� i je�li tak, to dodaj opis rysunku i resetuj licznik rysunk�w
                if (latex_plots_cnt == TEX_PLOTS_PER_PAGE || N == max_order)

                    latex_plots_cnt = 0;
                    latex_figures{latex_figures_cnt}.caption = sprintf(['Results of simulations of the time resolution of a digital constant-fraction discriminator ' ...
                                                                        'as a function of the number of samples at the rising edge, for various signal to noise ratios. ' ...
                                                                        'Bessel-type shaping filters with orders %d to %d'], caption_min_order, info_cf_sigma_time.filter_order(N));
                    latex_figures{latex_figures_cnt}.label = sprintf('sigma_time_vs_risetime-N_%d-to-%d', caption_min_order, info_cf_sigma_time.filter_order(N));
                end
            end                                    
            
            close(h);
        end;            
    end;
    
    % Przygotuj plik raportu
    if (TEX_CREATE_FILE > 0)
        latex_report_file = sprintf('%s/%s/report-sigma_time_vs_risetime.tex', FIGURES_FOLDER, fig_sub_folder_name);
        latex_create_report(latex_report_file, latex_figures);
    end                
end;

% ----------------------------------------------------------------------------------------------------------------------------------------------
% Analiza optymalnego rz�du filtru przy r�nych SNR-ach; parametryzacja po czasie narastania
% ----------------------------------------------------------------------------------------------------------------------------------------------

if my_analysis == 3
	
	% Nazwa foldera dla plik�w z rysunkami
	fig_sub_folder_name = 'cf_order_SNR';	

    % Zapytaj o rz�d filtra    
    min_risetime = input(' -- Minimum risetime: ');
    min_risetime = find(info_cf_sigma_time.risetime == min_risetime);
            
    if isempty(min_risetime)
        fprintf(1,'Error: this risetime was not simulated!\n');
        return;
    end

    max_risetime = input(' -- Maximum risetime: ');
    max_risetime = find(info_cf_sigma_time.risetime == max_risetime);
            
    if isempty(max_risetime)
        fprintf(1,'Error: this risetime was not simulated!\n');
        return;
    end

    % Powiedz, co robimy
    fprintf(1, 'Preparing data...\n');
    
    % Przygotuj dane do wykres�w
    size_SNR = size(cf_sigma_time,2);
    size_orders = size(cf_sigma_time,1);
    
    plot_data = zeros(size_SNR, length(min_risetime:max_risetime), size_orders);
    for i = 1:size_SNR
        for j = min_risetime:max_risetime
            for N = 1:size_orders
                plot_data(i,j,N) = min(cf_sigma_time(N,i,j,:)) / info_cf_sigma_time.risetime(j);
                %keyboard;
            end;
        end;
    end;
    
	%
	% Poka� legend� od kolor�w dla wykres�w z parametryzacj� po czasie narastania - na odr�bnej 'figure', bo na wykresach jest zbyt du�o krzywych, �eby doda� opisy
	%
	fprintf(1, 'Preparing line style and color legend\n');
	
    h = figure();
	hold off;
	cmap = get(gca,'ColorOrder');	% Pobierz bie��c� map� kolor�w
	my_color = 1;
	my_line = 1;
	
	total_lines = length(min_risetime:max_risetime);

	plot_y = [total_lines total_lines];
	plot_x = [0 1];
	
	text_x_min = 0.03;
	text_x_max = 0.87;
	text_x_step = (text_x_max - text_x_min) / (size(cmap,1)-1);
	
	text_x = text_x_min;

	for j=min_risetime:max_risetime  % P�tla po wszystkich czasach narastania

		plot(plot_x, plot_y, 'LineWidth', 1, 'Color', cmap(my_color,:), 'LineStyle', my_linestyles{my_line});
		text_to_display = sprintf('tr = %.1f', info_cf_sigma_time.risetime(j));
		text(text_x, plot_y(1), text_to_display ,'VerticalAlignment','middle', 'BackgroundColor','w', 'Color', cmap(my_color,:));
		
		plot_y = plot_y - 1;
		text_x = text_x + text_x_step;
		if (text_x > text_x_max)
			text_x = text_x_min;
		end;
		
		hold on;

		my_color = my_color+1;
		if my_color > length(cmap)
			my_color = 1;
			my_line = my_line + 1;
		end;      
	end;
	
	set(gca, 'XTickLabel', []);
	set(gca, 'YTickLabel', []);
	axis([0 1 0 total_lines+1]);	
	title('Line Style and Color Legend (param: t_{rise} [samples])');

    if (SAVE_PLOTS_TO_PNG > 0 || SAVE_PLOTS_TO_EPS > 0)        
        FormatAxes();
    
        if (TEX_CREATE_FILE > 0)        
            latex_figures_cnt = latex_figures_cnt + 1;
            latex_plots_cnt = latex_plots_cnt + 1;
            latex_figures{latex_figures_cnt}.caption = ['Line style and color legend for figures with plots of the time resolution ' ...
                                                        'as a function of the filter order, for different signal to noise ratios. ' ...
                                                        'Each plot is parameterized by the number of samples at the rising edge.'];
            latex_figures{latex_figures_cnt}.label = 'sigma_time_vs_order-SNR-legend';
        end;        
        
        % Zapisz obrazki do plik�w (format png)
        if SAVE_PLOTS_TO_PNG > 0
            filename_png = sprintf('%s/%s/sigma_time_vs_order-legend.png', FIGURES_FOLDER, fig_sub_folder_name);	

            fprintf(1,'Writing PNG file: %s\n', filename_png);
            print(h, filename_png, '-dpng', '-r300');
            
            if (TEX_CREATE_FILE > 0)
                latex_figures{latex_figures_cnt}.filenames{latex_plots_cnt} = filename_png;
            end                                            
        end;

        % Zapisz obrazki do plik�w (format eps)
        if SAVE_PLOTS_TO_EPS > 0
            filename_eps = sprintf('%s/%s/sigma_time_vs_order-legend.eps', FIGURES_FOLDER, fig_sub_folder_name);

            fprintf(1,'Writing EPS file: %s\n', filename_eps);
            print(h, filename_eps, '-depsc', '-tiff', '-r300');
            
            if (TEX_CREATE_FILE > 0)
                latex_figures{latex_figures_cnt}.filenames{latex_plots_cnt} = filename_eps;
            end                                            
        end;	
	
        close(h);
    end;

    % Wyczy�� licznik wykres�w dla latex-a
    latex_plots_cnt = 0;
    caption_min_SNR = 0;
    
    %
    % Wykresy z parametryzacj� po czasie narastania
    %
    for i = 1:size_SNR
        
        % Powiedz, co robimy
        fprintf(1, 'Showing results for SNR = %d\n', info_cf_sigma_time.SNR_dB(i));
        
        h = figure();
        hold off;
        cmap = get(gca,'ColorOrder');	% Pobierz bie��c� map� kolor�w
        my_color = 1;
        my_line = 1;    

        plot_x = info_cf_sigma_time.filter_order;
		
		% Sprawd�, czy b�dziemy stosowa� skal� liniow�, czy logarytmiczn�
		plot_mode_linear = (max(max(plot_data(i,:,:))) /  min(min(plot_data(i,:,:))) < 10);

        for j = min_risetime:max_risetime    % P�tla po wszystkich czasach narastania

            plot_y = plot_data(i, j, :);   
            plot_y = reshape(plot_y, 1, length(plot_y));
            %keyboard;
			if (plot_mode_linear > 0)
				plot(plot_x, plot_y, 'LineWidth', 1, 'Color', cmap(my_color,:), 'LineStyle', my_linestyles{my_line});
			else
				semilogy(plot_x, plot_y, 'LineWidth', 1, 'Color', cmap(my_color,:), 'LineStyle', my_linestyles{my_line});
			end
            hold on;

            my_color = my_color+1;
            if my_color > length(cmap)
                my_color = 1;
                my_line = my_line + 1;
            end;      
        end;

        xlabel('Filter Order');
        ylabel('\sigma_{time} [t_{rise}]');
        title(sprintf('\\sigma_{time} vs order (SNR = %d dB, param: t_{rise})',info_cf_sigma_time.SNR_dB(i)));

        if (SAVE_PLOTS_TO_PNG > 0 || SAVE_PLOTS_TO_EPS > 0)        
            FormatAxes();
            
            if (TEX_CREATE_FILE > 0)
                % Sprawd�, czy mamy utworzy� now� stron� z rysunkami
                if (latex_plots_cnt == 0)
                    latex_figures_cnt = latex_figures_cnt + 1;
                    caption_min_SNR = info_cf_sigma_time.SNR_dB(i);  % Zapami�taj pierwszy stosunek sygna�/szum do opisu rysunku zbiorczego
                end;  
                
                % Uwzgl�dnij bie��cy wykres
                latex_plots_cnt = latex_plots_cnt+1;
            end;            
        
            % Zapisz obrazki do plik�w (format png)
            if SAVE_PLOTS_TO_PNG > 0
                filename_png = sprintf('%s/%s/sigma_time_vs_order-SNR_%ddB.png', FIGURES_FOLDER, fig_sub_folder_name, info_cf_sigma_time.SNR_dB(i));	

                fprintf(1,'Writing PNG file: %s\n', filename_png);
                print(h, filename_png, '-dpng', '-r300');
                
                if (TEX_CREATE_FILE > 0)
                    latex_figures{latex_figures_cnt}.filenames{latex_plots_cnt} = filename_png;
                end                                            
                
            end;

            % Zapisz obrazki do plik�w (format eps)
            if SAVE_PLOTS_TO_EPS > 0
                filename_eps = sprintf('%s/%s/sigma_time_vs_order-SNR_%ddB.eps', FIGURES_FOLDER, fig_sub_folder_name, info_cf_sigma_time.SNR_dB(i));

                fprintf(1,'Writing EPS file: %s\n', filename_eps);
                print(h, filename_eps, '-depsc', '-tiff', '-r300');	

                if (TEX_CREATE_FILE > 0)
                    latex_figures{latex_figures_cnt}.filenames{latex_plots_cnt} = filename_eps;
                end                                            
            end;
            
            if (TEX_CREATE_FILE > 0)
                % Sprawd�, czy mamy maksymaln� liczb� wykres�w na stron� i je�li tak, to dodaj opis rysunku i resetuj licznik rysunk�w
                if (latex_plots_cnt == TEX_PLOTS_PER_PAGE || i == size_SNR)

                    latex_plots_cnt = 0;
                    latex_figures{latex_figures_cnt}.caption = sprintf(['Results of simulations of the time resolution of a digital constant fraction discriminator ' ...
                                                                        'as a function of the filter order, for different numbers of samples at the rising edge. ' ...
                                                                        'Results shown for Bessel-type shaper and SNR %d dB to %d dB.'], ...
                                                                        caption_min_SNR, int32(info_cf_sigma_time.SNR_dB(i)));
                    latex_figures{latex_figures_cnt}.label = sprintf('sigma_time_vs_order-SNR_%ddB_to_%ddB', caption_min_SNR, int32(info_cf_sigma_time.SNR_dB(i)));
                end
            end            
            
            close(h);
        end;
    end;
    
    if (TEX_CREATE_FILE > 0)
        latex_report_file = sprintf('%s/%s/report-%s.tex', FIGURES_FOLDER, fig_sub_folder_name, fig_sub_folder_name);
        latex_create_report(latex_report_file, latex_figures);
    end    
end;

% ----------------------------------------------------------------------------------------------------------------------------------------------
% Analiza optymalnego rz�du filtru przy r�nych czasach narastania; parametryzacja po SNR
% ----------------------------------------------------------------------------------------------------------------------------------------------

if my_analysis == 4
	
	% Nazwa foldera dla plik�w z rysunkami
	fig_sub_folder_name = 'cf_order_risetime';

    % Zapytaj o rz�d filtra    
    min_SNR = input(' -- Minimum SNR: ');
    min_SNR = find(info_cf_sigma_time.SNR_dB == min_SNR);
            
    if isempty(min_SNR)
        fprintf(1,'Error: this SNR was not simulated!\n');
        return;
    end

    max_SNR = input(' -- Maximum SNR: ');
    max_SNR = find(info_cf_sigma_time.SNR_dB == max_SNR);
            
    if isempty(max_SNR)
        fprintf(1,'Error: this SNR was not simulated!\n');
        return;
    end

    % Zapytaj o rz�d filtra    
    min_risetime = input(' -- Minimum risetime: ');
    min_risetime = find(info_cf_sigma_time.risetime == min_risetime);
            
    if isempty(min_risetime)
        fprintf(1,'Error: this risetime was not simulated!\n');
        return;
    end

    max_risetime = input(' -- Maximum risetime: ');
    max_risetime = find(info_cf_sigma_time.risetime == max_risetime);
            
    if isempty(max_risetime)
        fprintf(1,'Error: this risetime was not simulated!\n');
        return;
    end
    
    % Powiedz, co robimy
    fprintf(1, 'Preparing data...\n');
    
    % Przygotuj dane do wykres�w
    size_SNR = length(min_SNR:max_SNR);
    size_risetime = length(min_risetime:max_risetime);
    size_orders = size(cf_sigma_time,1);
    
    plot_data = zeros(size_SNR, size_risetime, size_orders);
    for i = min_SNR:max_SNR
        for j = min_risetime:max_risetime
            for N = 1:size_orders
                plot_data(i,j,N) = min(cf_sigma_time(N,i,j,:)) / info_cf_sigma_time.risetime(j);
                %keyboard;
            end;
        end;
    end;
    
	%
	% Poka� legend� od kolor�w dla wykres�w z parametryzacj� po SNR - na odr�bnej 'figure', bo na wykresach jest zbyt du�o krzywych, �eby doda� opisy
	%
	fprintf(1, 'Preparing line style and color legend\n');
	
    h = figure();
	hold off;
	cmap = get(gca,'ColorOrder');	% Pobierz bie��c� map� kolor�w
	my_color = 1;
	my_line = 1;
	
	total_lines = length(min_SNR:max_SNR);

	plot_y = [total_lines total_lines];
	plot_x = [0 1];
	
	text_x_min = 0.03;
	text_x_max = 0.75;
	text_x_step = (text_x_max - text_x_min) / (size(cmap,1)-1);
	
	text_x = text_x_min;

	for j=min_SNR:max_SNR  % P�tla po wybranych SNR-ach

		plot(plot_x, plot_y, 'LineWidth', 1, 'Color', cmap(my_color,:), 'LineStyle', my_linestyles{my_line});
		text_to_display = sprintf('SNR = %d dB', info_cf_sigma_time.SNR_dB(j));
		text(text_x, plot_y(1), text_to_display ,'VerticalAlignment','middle', 'BackgroundColor','w', 'Color', cmap(my_color,:));
		
		plot_y = plot_y - 1;
		text_x = text_x + text_x_step;
		if (text_x > text_x_max)
			text_x = text_x_min;
		end;
		
		hold on;

		my_color = my_color+1;
		if my_color > length(cmap)
			my_color = 1;
			my_line = my_line + 1;
		end;      
	end;
	
	set(gca, 'XTickLabel', []);
	set(gca, 'YTickLabel', []);
	axis([0 1 0 total_lines+1]);
	title('Line Style and Color Legend (param: SNR [dB])');

    if (SAVE_PLOTS_TO_PNG > 0 || SAVE_PLOTS_TO_EPS > 0)        
        FormatAxes();

        if (TEX_CREATE_FILE > 0)        
            latex_figures_cnt = latex_figures_cnt + 1;
            latex_plots_cnt = latex_plots_cnt + 1;
            latex_figures{latex_figures_cnt}.caption = ['Line style and color legend for figures with plots of the time resolution ' ...
                                                        'as a function of the filter order, for different numbers of samples at the rising edge. ' ...
                                                        'Each plot is parameterized by signal-to-noise ratio.'];
            latex_figures{latex_figures_cnt}.label = 'sigma_time_vs_order-risetime-legend';
        end;        
        
        % Zapisz obrazki do plik�w (format png)
        if SAVE_PLOTS_TO_PNG > 0
            filename_png = sprintf('%s/%s/sigma_time_vs_order-legend.png', FIGURES_FOLDER, fig_sub_folder_name);	

            fprintf(1,'Writing PNG file: %s\n', filename_png);
            print(h, filename_png, '-dpng', '-r300');
            
            if (TEX_CREATE_FILE > 0)
                latex_figures{latex_figures_cnt}.filenames{latex_plots_cnt} = filename_png;
            end                                                        
        end;

        % Zapisz obrazki do plik�w (format eps)
        if SAVE_PLOTS_TO_EPS > 0
            filename_eps = sprintf('%s/%s/sigma_time_vs_order-legend.eps', FIGURES_FOLDER, fig_sub_folder_name);

            fprintf(1,'Writing EPS file: %s\n', filename_eps);
            print(h, filename_eps, '-depsc', '-tiff', '-r300');
            
            if (TEX_CREATE_FILE > 0)
                latex_figures{latex_figures_cnt}.filenames{latex_plots_cnt} = filename_eps;
            end                                                        
        end;	
        
        close(h);
    end;
    
    % Wyczy�� licznik wykres�w dla latex-a
    latex_plots_cnt = 0;
    caption_min_risetime = 0;   
    
    %
    % Wykresy z parametryzacj� po czasie narastania
    %
    for i = min_risetime:max_risetime
        
        % Powiedz, co robimy
        fprintf(1, 'Showing results for risetime = %.1f\n', info_cf_sigma_time.risetime(i));
        
        h = figure();
        hold off;
        cmap = get(gca,'ColorOrder');	% Pobierz bie��c� map� kolor�w
        my_color = 1;
        my_line = 1;    

        plot_x = info_cf_sigma_time.filter_order;
		
		% Sprawd�, czy b�dziemy stosowa� skal� liniow�, czy logarytmiczn�
		plot_mode_linear = (max(max(plot_data(:,i,:))) /  min(min(plot_data(:,i,:))) < 10);		

        for j = min_SNR:max_SNR    % P�tla po wszystkich czasach narastania

            plot_y = plot_data(j, i, :);   
            plot_y = reshape(plot_y, 1, length(plot_y));
            %keyboard;
			if (plot_mode_linear)
				plot(plot_x, plot_y, 'LineWidth', 1, 'Color', cmap(my_color,:), 'LineStyle', my_linestyles{my_line});
			else
				semilogy(plot_x, plot_y, 'LineWidth', 1, 'Color', cmap(my_color,:), 'LineStyle', my_linestyles{my_line});
			end
            hold on;

            my_color = my_color+1;
            if my_color > length(cmap)
                my_color = 1;
                my_line = my_line + 1;
            end;      
        end;

        xlabel('Filter Order');
        ylabel('\sigma_{time} [t_{rise}]');
        title(sprintf('\\sigma_{time} vs order (t_{rise} = %.1f samples, param: SNR)',info_cf_sigma_time.risetime(i)));
		
        if (SAVE_PLOTS_TO_PNG > 0 || SAVE_PLOTS_TO_EPS > 0)        
            FormatAxes();
            
            if (TEX_CREATE_FILE > 0)
                % Sprawd�, czy mamy utworzy� now� stron� z rysunkami
                if (latex_plots_cnt == 0)
                    latex_figures_cnt = latex_figures_cnt + 1;
                    caption_min_risetime = info_cf_sigma_time.risetime(i);  % Zapami�taj pierwszy czas narastania do opisu rysunku zbiorczego
                end;  
                
                % Uwzgl�dnij bie��cy wykres
                latex_plots_cnt = latex_plots_cnt+1;
            end;                        
        
            % Zapisz obrazki do plik�w (format png)
            if SAVE_PLOTS_TO_PNG > 0
                filename_png = sprintf('%s/%s/sigma_time_vs_order-risetime_%.1f.png', FIGURES_FOLDER, fig_sub_folder_name, info_cf_sigma_time.risetime(i));	

                fprintf(1,'Writing PNG file: %s\n', filename_png);
                print(h, filename_png, '-dpng', '-r300');
                
                if (TEX_CREATE_FILE > 0)
                    latex_figures{latex_figures_cnt}.filenames{latex_plots_cnt} = filename_png;
                end                                                            
            end;

            % Zapisz obrazki do plik�w (format eps)
            if SAVE_PLOTS_TO_EPS > 0
                filename_eps = sprintf('%s/%s/sigma_time_vs_order-risetime_%.1f.eps', FIGURES_FOLDER, fig_sub_folder_name, info_cf_sigma_time.risetime(i));

                fprintf(1,'Writing EPS file: %s\n', filename_eps);
                print(h, filename_eps, '-depsc', '-tiff', '-r300');
                
                if (TEX_CREATE_FILE > 0)
                    latex_figures{latex_figures_cnt}.filenames{latex_plots_cnt} = filename_eps;
                end                                            
            end;		
            
            if (TEX_CREATE_FILE > 0)
                % Sprawd�, czy mamy maksymaln� liczb� wykres�w na stron� i je�li tak, to dodaj opis rysunku i resetuj licznik rysunk�w
                if (latex_plots_cnt == TEX_PLOTS_PER_PAGE || i == max_risetime)

                    latex_plots_cnt = 0;
                    latex_figures{latex_figures_cnt}.caption = sprintf(['Results of simulations of the time resolution of a digital constant fraction discriminator ' ...
                                                                        'as a function of the filter order, for different signal to noise ratios. ' ...
                                                                        'Results shown for Bessel-type shaper and rise times %.1f samples to %.1f samples.'], ...
                                                                        caption_min_risetime, info_cf_sigma_time.risetime(i));
                    latex_figures{latex_figures_cnt}.label = sprintf('sigma_time_vs_order-risetime_%.1f_to_%.1f', caption_min_risetime, info_cf_sigma_time.risetime(i));
                end
            end                        
            
            close(h);
        end;
    end;
    
    if (TEX_CREATE_FILE > 0)
        latex_report_file = sprintf('%s/%s/report-%s.tex', FIGURES_FOLDER, fig_sub_folder_name, fig_sub_folder_name);
        latex_create_report(latex_report_file, latex_figures);
    end
end;

% Przywr�� domy�lne wy�wietlanie okienek
set(0, 'DefaultFigureVisible', 'on')
