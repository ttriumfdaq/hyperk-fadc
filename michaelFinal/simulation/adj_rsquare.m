global RESULTS_FOLDER;

dir = strcat(RESULTS_FOLDER,'/figures/adj_rsquare');
load(strcat(RESULTS_FOLDER,'/resultsSave/bessel_awgfit_sigma_time_eqpmnt.mat'));

% For reference: chi2hist = zeros(length(H), length(SNR_dB), length(risetime),2,nbins);
% For 1 order, 9 SNR, 6 risetimes --> 54 histograms
% Organize as 9 SNR pages with the 6 different configurations on each
% Note, since length(H) is 1, the permute will made chi2hist a 4-D matrix
chi2hist = permute(chi2hist,[5 4 3 2 1]); % Other structure causes issues
setup = ['100MSPS - 1.5sh';...
    '100MSPS - 3.0sh';...
    '250MSPS - 1.5sh';...
    '250MSPS - 3.0sh';...
    '500MSPS - 1.5sh';...
    '500MSPS - 3.0sh'];

nSNR = 10:5:50;

width = 0.35;
height = 0.2;
left = 0.1;
right = 0.56;
plot_pos = [
    left 0.7; right 0.7;
    left 0.4; right 0.4;
    left 0.1; right 0.1];

scrsz = get(groot,'ScreenSize');

fprintf(['\nCareful!! This will overwrite the previously existing gof images in',...
    '\n\n    %s',...
    '\n\nContinue? [Ctrl+C to quit]'],dir);

pause();

fprintf('\n\nSaving images to %s/...\n',dir);

for i_snr = 1:length(chi2hist(1,1,1,:))
%for i_snr = 1
    figure('Position',[1 scrsz(4)/3 scrsz(3)/2 2*scrsz(4)/3])
    suptitle(sprintf('Goodness of Fits\nSNR = %i dB',nSNR(i_snr)));
    
    for i_rt = 1:6
        centers = chi2hist(:,1,i_rt,i_snr);
        counts = chi2hist(:,2,i_rt,i_snr);
        subplot(3,2,i_rt,'position',[plot_pos(i_rt,:) width height]);
        
        bar(centers,counts);
        title(sprintf('%s',setup(i_rt,:)));
    end
    
    fout = sprintf('/gof_%idB.png',nSNR(i_snr));
    print(figure(1),strcat(dir,fout));
    close;
    
end

