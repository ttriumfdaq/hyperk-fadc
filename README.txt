This directory contains an example of a rootana analyzer based on the
libAnalyzer/libAnalyzerDisplay framework.  It contains analysis code appropriate for
processing 'standard' TRIUMF MIDAS data taken with CAEN V792, V1190, V1720, V1730, DT5724, CAMAC TL2249 and Agilent data.

To use the analysis code you first need ROOT & ROOTANA:

1) Install rootana in usual place ($HOME/packages) and make (cd $HOME/packages/rootana; make)

2) Set environment variable ROOTANASYS to rootana location (usually $HOME/packages/rootana)

3) Copy this directory (rootana/examples) to somewhere to play with code and build

mkdir ~/analyzer/.
cp ~/packages/rootana/examples/* ~/analyzer/.
cd ~/analyzer
make

4) You will have now built a default analyzer display (anaDisplay.exe) and batch-mode analyzer (ana.exe) 
that you can use to display CAEN V792 and V1190 data.

You can then modify the files to create new plots.

The files anaDisplay.cxx and ana.cxx mostly just use histograms setup inside TAnaManager.hxx/cxx.  
In particular you can edit the file TAnaManager.hxx to quickly choose which equipment to use with ifdef.  
For instance, the selection

#define USE_V792
#define USE_V1190
//#define USE_L2249
//#define USE_AGILENT
//#define USE_V1720
//#define USE_V1730DPP
//#define USE_V1730RAW
//#define USE_DT724

will create histograms of V792 and V1190 data.  By changing the commented out precompiler flags 
you can modify the expected hardware.

Once you've retrieved this repo issue 'make' to build the executables hkAnalyzer.exe and hkDisplay.exe

hkAnalyzer processes an entire run and outputs a tree.
hkDisplay is to view the waveforms, debug, etc.

An example command:
./hkDisplay.exe /path/to/run00208.mid.gz --useDT5724 --use15ns -s1000

As you can see you need to specify which ADC and shaper combination the run uses.
If you look in TAnaManager.cxx there's a commented 'return 0' which can be uncommented if you 
want to skip the analysis and just view the waveform(s) (useful for debugging sometimes).
The -s1000 tells the program not to display the first 1000 events, though it still analyzes them. 
This is necessary to see how things like the pulse time difference histo evolves without clicking thousands of times.

Use --help for more details. 
Another useful tip is for analyzing multiple files use something along the lines of:
./hkAnalyzer.exe /path/run0020{1..8}.mid.gz --useDT5724 --use15ns


**TAnaManager** is the control center for the display/analysis, among other things. Histogram design
can be tweaked here, and it also builds the tree.
**TPulseFitter** and **TFits** concern the fitting techniques and parameters, TFits has the function 
formulae and is where the fitting parameters (and the functions that set them) are stored,
TPulseFitter configures the fitting and such.
**TPulseFinder** scans the raw waveform to locate max/min bins and such used for the fitting and tree.
**TimeResMacro** is a ROOT macro used separately to build plots from the tree.

Lastly, the name of the game is minimizing the timing resolution. The pulse time differences are
recorded in 'Fitted Time Differences', and we want as tight a gaussian as possible.
Sometimes you'll find results that aren't very gaussian, and correcting this is not a straight forward
problem. Sometimes parameter values need to be refined (TFits), or the fitting range needs to be 
shortened/lengthened (TPulseFitter).



Thomas Lindner: lindner@triumf.ca
Michael Walters: walterms@mcmaster.ca