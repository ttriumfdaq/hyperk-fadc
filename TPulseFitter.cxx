#include "TPulseFitter.hxx"

#include <cmath>
#include <vector>
#include "TROOT.h"
#include "TMath.h"
#include "TFitResult.h"
#include "TGraph.h"


TF1 *fStep, *Template, *Prod;


PulseFitterResult TPulseFitter::FitPulse(TH1D *waveform, int ch, bool negPulse, bool useMF, double sigNoise, 
					 double pulseHeight, double peakTime, double baseline, 
					 int pulseType, int shaper, TGraph *graph){
  
  PulseFitterResult result;

  double range_low = 0;
  double range_high = 0;

  // Set ranges
  // 0 is signal, 1 is reference
  if(pulseType == 0){
    
    if(shaper == 0){
      // 15 ns
      range_low = 50; 
      range_high = 30;
    }else if(shaper == 1){
      // 30 ns
      range_low = 120;
      range_high = 50;
    }else{
      //no shaper
      range_low = 22;
      range_high = 30;
    }

  }

  if(pulseType == 1){
    range_low = 30;
    //range_low = 50; 
    //range_high = 30;
    range_high = 10;

  }
  
  double start = peakTime - range_low;
  double end = peakTime + range_high;

  // Delete old fits
  char refname[100],signame[100],tempname[100];
  sprintf(refname,"pulse_timing_0");
  sprintf(signame,"pulse_timing_1");
  sprintf(tempname,"template_name");

  TF1 *oldref = (TF1*)gROOT->Get(refname);
  TF1 *oldsig = (TF1*)gROOT->Get(signame);
  TF1 *oldtemp = (TF1*)gROOT->Get(tempname);

  if(oldref){
    delete oldref;
  }
  if(oldsig){
    delete oldsig;
  }
  if(oldtemp){
    delete oldtemp;
  }


  // Reference pulse  --  EMG

  if(pulseType == 1) {
  
    // Currently only has range set up for EMG, but EMG works well
    TF1 *fitted_ref = new TF1(refname,funcEMG,start,end,5);

    // Fitting function that sets parameters
    fitEMG(fitted_ref,peakTime,pulseHeight,baseline,pulseType,shaper);
    fitted_ref->SetLineColor(2);
    
    TFitResultPtr reffit =  waveform->Fit(refname,"QRS","",start, end);

    // Filter non-convergent fits
    // Using Minuit convention : =0 not calculated, =1 approximated (failed), =2 made pos def, =3 accurate 
    if(reffit->CovMatrixStatus() == 3) {

      if(negPulse == false)
    	result.fittedTime = fitted_ref->GetMaximumX();
      else
    	result.fittedTime = fitted_ref->GetMinimumX();

    } else {

      result.fittedTime = 0;
      std::cout << "Bad reference fit\n";

    }


    // // Loop parameter values to determine an average
    // static double avg  = 0;
    // static double avgn = 0;
        
    // avg += fitted_ref->GetParameter(3);
    // avgn += 1.0;
       
    // std::cout << "single value " << fitted_ref->GetParameter(3)
    // 		<< " average value " <<  avg/avgn 
    // 		<< std::endl;




  }


  // Signal Pulse

  // For a fitting function "FF"
  // - funcFF() is the mathematical function
  // - fitFF() sets (or fixes) the parameters
  

  if(pulseType == 0) {

    if(useMF) {

      // Matced Filtering

      // double window = (end-start)*3*2;
      // double win_low = start - window/3;
      // double win_high = end + window/3;

      double window = 600;
      double win_low = 800;
      double win_high = 1400;


      double binwidth = waveform->GetBinWidth(1);


      // Make template
      Template = new TF1(tempname,funcGNV2,win_low-window/3,win_high+window/3,8);
      //   fitGNV2(Template,peakTime,pulseHeight,baseline,pulseType,2,0,start,end); 

      // double alpha = 0.5479;
      // double kappa = -0.00391206;
      // //    double mag = pulseHeight * 1.363;
      // //double mag = 173.798 * 1.363;
      // double mag = 25.0;
      // double sigma = 418.061;


      // DT5724 template 30ns
      double alpha = 2.08;
      double kappa = -0.00664;
      double mag = -400.0;
      double sigma = 400.0;

  

  
      Template->FixParameter(0,alpha);
      Template->FixParameter(1,win_low + window/2);
      Template->FixParameter(2,kappa);
      Template->FixParameter(3,baseline);
      Template->FixParameter(4,mag);
      Template->FixParameter(5,sigma);
      Template->FixParameter(6,start);
      Template->FixParameter(7,end);

      Template->SetLineColor(49);
    
      TFitResultPtr Tempfit =  waveform->Fit(tempname,"QRS","",win_low-window/3,win_high+window/3);


      if(negPulse == false)
        result.fittedTime = Template->GetMaximumX();
      else
        result.fittedTime = Template->GetMinimumX();


      int nfactor = 1; // number of points per bin

      if(shaper == 0) {
        nfactor = 2; 
      }


      int n = nfactor * window / binwidth; // number of graph points in convolt region
      std::vector<double> x(n),ytemp(n),ydat(n),prod(n),yconv(n);
      int index;
      double step = window/n;


      // Clear graph
      if(graph){
      
        graph->Clear();

      }


      // Scaling factor -- smaller factor means large height
      double factor = nfactor * fabs(pulseHeight - baseline) / 2.0;

      if(fabs(pulseHeight - baseline) < 15.0)
        factor = factor / 150.0;


      // Gather template and waveform data points, then convolude
      for(int j=0;j<n;j++) {

        for(int i=0;i<n;i++) {

      	index = (win_low + i*step)/binwidth + 1;
    	
      	x[i] = win_low + (i)*step;
      	ydat[i] = waveform->GetBinContent(index) - baseline;

      	// For Convolution
      	ytemp[i] = Template->Eval(result.fittedTime + range_high - i*step + step*j ) - baseline;

      	// For Correlation
      	//ytemp[i] = Template->Eval(result.fittedTime - range_low + i*step - step*j ) - baseline;

      	prod[i] = ydat[i] * ytemp[i] / factor;

      	yconv[j] += prod[i] + baseline/n;

        }

        graph->SetPoint(j,x[j],yconv[j]);

      }



 
    } else {
      // Direct, non-MF


      // Don't forget to adjust the fitting function and number of parameters accordingly in TF1
      int npar = 8;
      TF1 *fitted_signal = new TF1(signame,funcGNV2,start,end,npar);
      fitGNV2(fitted_signal,peakTime,pulseHeight,baseline,pulseType,shaper,0,start,end); //8par
      //fit4Exp(fitted_signal,peakTime,pulseHeight,baseline,pulseType,shaper); //5par
      //fitEMG(fitted_signal,peakTime,pulseHeight,baseline,pulseType,shaper); //5par
      fitted_signal->SetLineColor(9);
    
      TFitResultPtr sigfit =  waveform->Fit(signame,"QRS","",start,end);



      if(sigfit->CovMatrixStatus() == 3) {

	if(negPulse == false)
	  result.fittedTime = fitted_signal->GetMaximumX();
	else {
	  result.fittedTime = fitted_signal->GetMinimumX(); 
	}

      } else {

	std::cout << "Bad signal fit" << std::endl;
	result.fittedTime = 0;

      }

    
      // Loop parameter values to determine an average
      if(0) {
	static double avg  = 0;
	static double avgn = 0;
	int par = 5;
        
	avg += fitted_signal->GetParameter(par);
	avgn += 1.0;
       
	std::cout << "single value " << fitted_signal->GetParameter(par)
		  << " average value " <<  avg/avgn 
		  << std::endl;
      }

    }

 
  }

  
  return result;


}
