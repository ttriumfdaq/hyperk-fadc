% Skrypt do badania dyskryminatora CF w zale�no�ci od rz�du zastosowanego filtra Bessela.
% Sprawdza rozdzielczo�� czasow� w zale�no�ci od czasu narastania impulsu, stosunku sygna�/szum oraz op�nienia
% dyskryminatora CF.

global SHOW_PLOTS;
global RESULTS_FOLDER;

my_marker_size = 18;
my_line_width = 2;
my_font_size = 16;

% Ustawienia do oblicze�
flt_order = 5;      % Rz�d filtru, kt�ry b�dziemy bada�
risetime = 2;		% Liczba pr�bek na narastaj�cym zboczu impulsu
SNR_dB = 10;        % Stosunek sygna� szum

t_max = 18;         % Czas, do kt�rego b�dziemy wy�wietla� odpowiedzi impulsowe filtr�w
thr_low = 0.1;      % Dolny pr�g do obliczania czas�w narastania/opadania (wzgl�dem amplitudy impulsu)
thr_high = 0.9;     % G�rny pr�g do obliczania czas�w narastania/opadania (wzgl�dem amplitudy impulsu)
%keyboard;

sim_resolution = 0.01;				% Krok symulacji czasowej (w pr�bkach). 
fft_N = round(1/sim_resolution);	% Stopie� interpolacji FFT	

% Utw�rz prototypy filtr�w (znormalizowane)
fprintf(1,'Creating transfer functions of Bessel-type low pass filter with order %d\n', flt_order);

[num,den] = besself(flt_order,1);
H = tf(num,den);    

% Wyznacz odpowied� impulsow� filtru
[y, t] = impulse(H, t_max);
        
% Zmodyfikuj krok czasowy odpowiedzi impulsowej tak, by mie� zadan� liczb� pr�bek na zboczu narastaj�cym.
% W�wczas zmieniaj�c op�nienie o jeden b�dziemy de-facto mieli op�nienie wyra�one w procentach czasu
% narastania.
t_rise = pulse_rise_time(t, y, thr_low, thr_high);
dt = t_rise / risetime;
t = 0 : dt*sim_resolution : t_max;
y = [impulse(H,t)];
		
% Normalizuj odpowied� filtra
y = y ./ max(y);
		
% Dodaj troch� zer na pocz�tku - to jest przydatne przy ma�ej liczbie pr�bek na zboczu, gdy� inaczej alborytm
% CF morze p�j�� w krzaki
y = [zeros(2*risetime / sim_resolution,1); y];
t = [1:length(y)];

% Wywietl przebieg �r�d�owy
figure;
plot(t,y, 'LineWidth', my_line_width);
set(gca,'fontsize',my_font_size)

% Dodaj szum (zak�adamy, �e moc sygna�u to 0 dBW (amplitudowo b�dzie OK, bo mamy unormowan� odpowied� uk�adu kszta�tuj�cego)
y_noisy = awgn(y, SNR_dB, 0);

% Wywietl przebieg zaszumiony
figure;
plot(t,y_noisy);
set(gca,'fontsize',my_font_size)

% Utw�rz spr�bkowany przebieg na podstawie przebiegu zag�szczonego
t_offset = 1 + round((1/sim_resolution - 1) * rand(1,1));
idx_sample = t_offset : 1/sim_resolution : length(y);
y_sampled = y_noisy(idx_sample);
flt_sampled = y(idx_sample);

% Utw�rz przebieg interpolowany
y_interp = interpft(y_sampled, fft_N*length(y_sampled));
t_interp = [1:length(y_interp)];

% Wy�wietl przebieg spr�bkowany interpolowany fft
figure;
plot(t_interp,y_interp,'LineWidth', my_line_width);
hold on;
plot(1:fft_N:length(t_interp),y_sampled,'r.', 'MarkerSize', my_marker_size);
set(gca,'fontsize',my_font_size)

% Wywietl spr�bkowan� odpowied� impulsow�
figure;
plot(t,y, 'LineWidth', my_line_width);
hold on;
plot(idx_sample,flt_sampled,'r.', 'MarkerSize', my_marker_size);
set(gca,'fontsize',my_font_size)

% Filtruj przebieg
y_filtered = conv(y_sampled, flt_sampled);

flt_interp = interpft(y_filtered, fft_N*length(y_filtered));
t_interp = [1:length(flt_interp)];
%keyboard;

% Wy�wietl przebieg interpolowany
figure;
plot(t_interp,flt_interp, 'LineWidth', my_line_width);
hold on;
plot(1:fft_N:length(t_interp), y_filtered, 'r.', 'MarkerSize', my_marker_size);
set(gca,'fontsize',my_font_size)
					    