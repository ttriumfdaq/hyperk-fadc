% Skrypt do badania dyskryminatora CF w zale�no�ci od rz�du zastosowanego filtra Bessela.
% Sprawdza rozdzielczo�� czasow� w zale�no�ci od czasu narastania impulsu, stosunku sygna�/szum oraz op�nienia
% dyskryminatora CF.

global SHOW_PLOTS;
global RESULTS_FOLDER;

% Ustawienia do oblicze�
min_order = 3;      % Minimalny rz�d filtru, kt�ry b�dziemy bada�
max_order = 12;     % Maksymalny rz�d filtru, kt�ry b�dziemy bada�
t_max = 22;         % Czas, do kt�rego b�dziemy wy�wietla� odpowiedzi impulsowe filtr�w
thr_low = 0.1;      % Dolny pr�g do obliczania czas�w narastania/opadania (wzgl�dem amplitudy impulsu)
thr_high = 0.9;     % G�rny pr�g do obliczania czas�w narastania/opadania (wzgl�dem amplitudy impulsu)
min_risetime_for_CF = 64;	% Minimalny czas narastania dla dyskryminatora CF; Je�li mniej pr�bek, to interpolujemy wykorzystuj�c FFT;

risetime = [ 1:0.1:2.2 2.4:0.2:4 4.5:0.5:5 ];		% Liczba pr�bek na narastaj�cym zboczu impulsu
SNR_dB = [ 10:5:50 60:10:100 ];                     % Stosunek sygna� szum

% Oblicz stopie� interpolacji FFT tak, by mie� wymagan� pr�bek na zboczu narastaj�cym
fft_N = round((min_risetime_for_CF * ones(size(risetime))) ./ risetime);
fft_N = max(fft_N,1);
%keyboard;

delay_min = -1;         % Maksymalne odsuni�cie czasowe na minus od optymalnego op�nienia dyskryminatora CF (wielokrotno�� czasu narastania)
delay_max = 1;			% Maksymalne odsuni�cie czasowe na plus od optymalnego op�nienia dyskryminatora CF (wielokrotno�� czasu narastania)
delay_step = 0.1;		% Krok symulacji op�nienia, od minimalnej do maksymalnej odchy�ki (wielokrotno�� czasu narastania)

sim_cnt = 10000;        % Liczba iteracji dla danego zestawu parametr�w
sim_resolution = 0.001;	% Krok symulacji czasowej (w pr�bkach). 

% Utw�rz prototypy filtr�w (znormalizowane)
fprintf(1,'Creating transfer functions of Bessel-type low pass filters...\n');

H = {};
for N = min_order:max_order
    [num,den] = besself(N,1);
    H{N-min_order+1} = tf(num,den);    
end;

% Wyznacz charakterystki nachylenia krzywych dyskryminatora CF
fprintf(1,'Loading results of previous digital CF analysis...\n');
load(strcat(RESULTS_FOLDER,'/bessel_cf_analyze.mat'), 'max_slope');

% Sprawd�, na ile mo�emy odsun�� si� od op�nienia generuj�cego maksymalne zbocze, �eby wci�� utrzyma� przyzwoite osi�gi dyskryminatora CF
% Dalej zak�adamy, �e mamy du�y nadmiar pr�bek na zboczu narastajacym, wi�c cz�stotliwo�� pr�bkowania nie powinna by� problemem.
fprintf(1,'Testing time resolution of digital CF algorithm...\n');

% Utw�rz macierze i wektory na potrzeby zapami�tania oblicze�
cf_delay = delay_min : delay_step : delay_max;

% Sprawd�, czy mamy zezwolenie na obliczenia r�wnoleg�e
if matlabpool('size') == 0
    matlabpool open;
end;

% Utw�rz struktur� z opisem wynik�w symulacji
info_cf_sigma_time.description = 'Results of digital CF-discriminator timing';
info_cf_sigma_time.parameters = 'filter order; SNR; rise time; CF delay';
info_cf_sigma_time.filter_order = min_order:max_order;
info_cf_sigma_time.SNR_dB = SNR_dB;
info_cf_sigma_time.risetime = risetime;
info_cf_sigma_time.fft_N = fft_N;
info_cf_sigma_time.cf_delay = zeros(size(max_slope,1), length(cf_delay));

for i = 1:size(max_slope,1)
	info_cf_sigma_time.cf_delay(i,:) = cf_delay + max_slope(i,2);
end;

% Utw�rz struktury danych
cf_sigma_time = zeros(length(H), length(SNR_dB), length(risetime), length(cf_delay));

% P�tla po rz�dach filtru Bessel-a
for i_order = 1:length(H)

    % P�tla po czasach narastania (ekwiwalent pr�dko�ci pr�bkowania)
    for i_risetime = 1:length(risetime)
        
        % Wyznacz odpowied� impulsow� filtru
        [y, t] = impulse(H{i_order}, t_max);
        
        % Zmodyfikuj krok czasowy odpowiedzi impulsowej tak, by mie� zadan� liczb� pr�bek na zboczu narastaj�cym.
        % W�wczas zmieniaj�c op�nienie o jeden b�dziemy de-facto mieli op�nienie wyra�one w procentach czasu
        % narastania.
        t_rise = pulse_rise_time(t, y, thr_low, thr_high);
        dt = t_rise / risetime(i_risetime);
        t = 0 : dt*sim_resolution : t_max;
        y = [impulse(H{i_order},t)];
		
        % Normalizuj odpowied� filtra
        y = y ./ max(y);
		
		% Dodaj troch� zer na pocz�tku - to jest przydatne przy ma�ej liczbie pr�bek na zboczu, gdy� inaczej alborytm
		% CF morze p�j�� w krzaki
		y = [zeros(2*risetime(i_risetime) / sim_resolution,1); y];

        % Oblicz minimalne i maksymalne op�nienia, w "pe�nych" pr�bkach
        min_delay_in_samples = (max_slope(i_order,2) + min(cf_delay)) * risetime(i_risetime);
        max_delay_in_samples = (max_slope(i_order,2) + max(cf_delay)) * risetime(i_risetime);
        step_delay_in_samples = delay_step * risetime(i_risetime);

        % Utw�rz wektor op�nie�, kt�re b�dziemy symulowa�
        cf_delay_in_samples = min_delay_in_samples : step_delay_in_samples : max_delay_in_samples;
        %keyboard;
                
        % Symuluj r�ne poziomy szumu
        for i_snr = 1:length(SNR_dB)

            % Powiedz, jaki mamy obecnie stosunek sygna�u do szumu
            fprintf(1,'\n  Current filter order: %d, rise time: %.1f samples, SNR: %d dB\n', min_order - 1 + i_order, risetime(i_risetime), int32(SNR_dB(i_snr)));

            % Alokuj pami�� na wyniki (mamy zbiorcze dla wszystkich przesuni��, �eby zmniejszy� liczb� plik�w
            cf_time = zeros(length(cf_delay_in_samples),sim_cnt);
            
            % Symuluj op�nienia
            for d = 1:length(cf_delay_in_samples)
                
                % Poka�, kt�re op�nienie symulujemy
                fprintf(1,'    Delay: %.2f samples\n', cf_delay_in_samples(d));
    
                % Symuluj offsety czasowe
                tmp_delay = cf_delay_in_samples(d);
    
                parfor j = 1:sim_cnt
                %for j = 1:sim_cnt
                    % Utw�rz spr�bkowany przebieg na podstawie przebiegu zag�szczonego
                    t_offset = 1 + round((1/sim_resolution - 1) * rand(1,1));
                    idx_sample = t_offset : 1/sim_resolution : length(y);
                    y_sampled = y(idx_sample);
					
					% Dodaj szum (zak�adamy, �e moc sygna�u to 0 dBW (amplitudowo b�dzie OK, bo mamy unormowan� odpowied� uk�adu kszta�tuj�cego)
					y_sampled = awgn(y_sampled, SNR_dB(i_snr), 0);
    
                    % Wyznacz czas za pomoc� dyskryminatora CF i uwzgl�dnij za�o�ony wcze�niej offset
                    % (dodaj�c offset pr�bkowania de facto przyspieszczamy impuls, w zwi�zku z czym musimy to
                    % uwzgl�dni� w wyniku)
                    cf_time(d,j) = pulse_cf_time_integral_fft(y_sampled, tmp_delay, [0 0], fft_N(i_risetime), 2, SHOW_PLOTS);
                    cf_time(d,j) = cf_time(d,j) + (t_offset-1) * sim_resolution;
                end;
				
				% Oblicz rozdzielczo�� czasow� i zapami�taj wynik
				cf_sigma_time(i_order, i_snr, i_risetime, d) = std(cf_time(d,:));
            end;
            
            % Utw�rz nazw� pliku do zapami�tania szcz�tkowych wynik�w symulacji (na wszelki wypadek)
            filename = sprintf('%s/partial_sigma_time/N%d-tr%.1f-snr%d.mat', RESULTS_FOLDER, ...
                                                                             int32(i_order) + min_order - 1, ...
                                                                             risetime(i_risetime), ...
                                                                             int32(SNR_dB(i_snr)));
            % Zapisz dane dotycz�ce tego op�nienia i SNR
            t_rise = risetime(i_risetime);
            save(filename, 'cf_time', 'cf_delay_in_samples', 't_rise');                                                                             
            clear cf_time;
        end;
                
        % Zapisz wyniki (co ka�dy czas narastania - w razie czego b�dzie mo�na ograniczy� powtarzanie symulacji)
        save(strcat(RESULTS_FOLDER,'/bessel_cf_sigma_time.mat'), 'max_slope', 'cf_sigma_time', 'info_cf_sigma_time');        
    end;
     
    % Wyczy�� zag�szczone przebiegi oraz zmienn� do liczenia rozdzielczo�ci czasowej (bo zajmuj� du�o pami�ci) 
    clear t y;
end;
