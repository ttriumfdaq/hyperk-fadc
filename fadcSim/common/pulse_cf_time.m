function [ cf_time cf_slope ] = pulse_cf_time( samples, cf_delay, show_plots )
%PULSE_CF_TIME Extract time of pulse arrival using a digital constant fraction algorithm
%
%   [ cf_time slope ] = PULSE_CF_TIME( samples, delay, show_plots )
%   Extract time of pulse arrival using a digital constant fraction algorithm. Returned value
%   is a sample index corresponding to zero-crossing of the constant fraction waveform and is
%   interpolated using linear interpolation, so it doesn't have to be an integer value. The algorithm works
%   as follows:
%
%   1. The original waveform is delayed by 'delay' samples, inverted and multiplied by 2;
%   2. The original and delayed/inverted waveform are added together, thus forming the 'cf' waweform.
%   3. The intersection of the 'cf' waveform and the abisca is found, at the section between the maximum and 
%      the minimum value of the 'cf' waveform. The exact value is interpolated using a linear interpolation.
%
%   Arguments:
%       samples - waveform to analyze; pedestal subtraction must be done elsewhere.
%       cf_delay - delay of inverted waveform (in samples)
%       show_plots - turns on/off display of CF waveforms
%
%   Returnes values:
%       cf_time - calculated time of pulse arrival (in samples)
%       cf_slope - slope at the intersection point (in arbitrary unints per sample).

[cf_time, ~, cf_slope] = pulse_cf_time_integral_fft( samples, cf_delay, [0 0], 1, 2, show_plots );

end

