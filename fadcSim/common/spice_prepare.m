function spice_prepare(filename)
%Przygotowuje plik .net dla symulacji (z pliku .asc)
%Parametr podajemy bez rozszerzenia

global SPICE_EXEC;

filename = [filename '.asc'];
command = [SPICE_EXEC ' -netlist ' filename];
system(command);

