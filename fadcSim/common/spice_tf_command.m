function command = spice_tf_command(output, input)
%Generuje komend� .tf (DC Transfer)
%Parametry:
%   output:             wyj�cie V(nXXX) lub V(nXXX,nYYY) lub I(Vx)
%   input:              �r�d�o wej�ciowe (np. V1)

command = sprintf('.tf %s %s', output, input);
