function [data dataop]=spice_execute(script_file,param_file,params,spice_commands,prepare,readop)
%G��WNA FUNKCJA FREJM�ORKU
%Przeprowadza ca�y proces spajsowania
%Wch�ania nazwy dw�ch plik�w (bez rozszerze�) i list� parametr�w
%Parametry w postaci takiej jak dla spice_params
%spice_commands to tablica celli zawieraj�cych dodatkowe linie do
%wstawienia do pliku .net - zazwyczaj komendy dotycz�ce symulacji
%prepare=1 oznacza, �e b�dzie generowany plik .net
%Je�li plik ju� jest, dla zaoszcz�dzenia czasu mo�na da� prepare=0
%Je�li go nie ma, to zawsze zostanie wygenerowany
%Zmiana parametr�w nie powoduje konieczno�ci ponownej konwersji.
%Readop m�wi, czy przeczyta� plik .op(z punktem pracy). Taki plik mo�e by�
%wygenerowany, ale nie musi.

disp('SPICE: Preparing parameter file');
spice_parameters(param_file,params);
if prepare || ~exist([script_file '.net'],'file')
    disp('SPICE: Preparing netlist');
    spice_prepare(script_file);
end;
disp('SPICE: Adding commands');
spice_addlines(script_file,[script_file '_mod'],spice_commands);
disp('SPICE: Running script');
spice_run([script_file '_mod']);
disp('SPICE: Acquiring data');
data = spice_read([script_file '_mod']);
if readop
    dataop = spice_read([script_file '_mod' '.op']);
else
    dataop = [];
end;
disp('SPICE: Ready!');
