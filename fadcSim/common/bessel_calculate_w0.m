function w0 = bessel_calculate_w0( N, f_sample, t_rise )
%BESSEL_CALCULATE_W0
%
%   [w0 H] = bessel_calculate_w0( N, f_sample, t_rise )
%   Calculate w0 (omega zero) of a Bessel-type shaping filter of a given order N, when
%	sampling frequency is f_sample and the desired number of samples at the rising edge is t_rise.
%	The time of the rising edge is defined as 10% to 90% of the amplitude.

% Wyznacz okres pr�bkowania
Ts = 1/f_sample;

% Wylicz prototyp dolnoprzepustowego filtra Bessela zadanego rz�du
[num,den] = besself(N,1);
H = tf(num,den);

% Wyznacz odpowied� impulsow�
[y,t] = impulse(H);

% Oblicz czas narastania
t_rise_normalized = pulse_rise_time(t, y, 0.1, 0.9);

% Oblicz cz�stotliwo�� graniczn�
scale_factor = Ts*t_rise / t_rise_normalized;
w0 = 1/scale_factor;

end

