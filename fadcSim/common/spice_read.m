function data = spice_read(filename)
%Odczytuje dane z pliku wynikowego .raw
%Na razie obs�uguje wersj� tekstow� (tzn. na razie to �adnej)
%filename to nazwa pliku wynikowego (czyli taka sama jak .asc i .net), bez
%rozszerzenia
%Zwraca tablic� struktur R
%   R.label - nazwa zmiennej
%   R.data - tablica danych

filename = [filename '.raw'];

if ~exist(filename,'file')
    data = [];
    return;
end;

file = fopen(filename,'rt');

line = '';

%***Sprawdzamy typ danych
while ~length(strfind(line,'Flags:'))
    line = fgetl(file);
end;
if feof(file)
    return;
end;
complexdata = 0;
if length(strfind(line,'complex'))
    complexdata = 1;
end;

%***Sprawdzamy ilo�� zmiennych
line = fgetl(file);
while ~length(strfind(line,'No. Variables:'))
    line = fgetl(file);
end;
if feof(file)
    return;
end;
no_variables = sscanf(line(15:length(line)),'%d',1);  %15 - bo tyle zajmuje napis 'No. variables'
 
%complexdata
%no_variables

%***Sprawdzamy ilo�� punkt�w
line = fgetl(file);
while ~length(strfind(line,'No. Points:'))
    line = fgetl(file);
end;
if feof(file)
    return;
end;
no_points = sscanf(line(12:length(line)),'%d',1);  %12 - bo tyle zajmuje napis 'No. Points'
 
%complexdata
%no_variables
%no_points

%***Wy�awiamy nazwy zmiennych
line = fgetl(file);
while ~length(strfind(line,'Variables:'))
    line = fgetl(file);
end;
if feof(file)
    return;
end;
for i=1:no_variables
    line = fgetl(file);
    [no,count,errmsg,nextindex] = sscanf(line,'%d');
    data(i).label = sscanf(line(nextindex:length(line)),'%s',1);
end;

%***Jedziemy do warto�ci
line = fgetl(file);
while ~length(strfind(line,'Values:'))
    line = fgetl(file);
end;
if feof(file)
    return;
end;

if complexdata == 1
    %Dane zespolone
    for i=1:no_points
        fscanf(file,'%d',1);
        values(:,i) = fscanf(file,'%f,%f',2*no_variables);
    end;
    for i=1:no_variables
        data(i).data = values(2*i-1,:) + j*values(2*i,:);
    end;    
else
    %Dane rzeczywiste
    values = fscanf(file,'%f',[no_variables+1,no_points]);
    for i=1:no_variables
        data(i).data = values(i+1,:);
    end;    
end;

fclose(file);
