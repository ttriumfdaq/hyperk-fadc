function command = spice_dc_command(source_name, sweep_type, start_list, stop, increment)
%Generuje komend� .dc (DC sweep)
%Parametry:
%   source_name:        nazwa �r�d�a, po kt�rym sweepujemy
%                       UWAGA - wszystkie parametry mog� mie� posta�
%                       cellow� (nie tablicow�) - mo�emy sweepowa� 
%                       po kilku �r�d�ach (LTSpice potrafi po dw�ch albo 
%                       po trzech albo i wi�cej)
%   sweep_type:         rodzaj przemiatania
%       0 lub 'oct':    oktawami
%       1 lub 'dec':    dekadami
%       2 lub 'lin':    liniowo
%       3 lub 'list':   lista punkt�w
%   start_list:         dolna granica przemiatania (lista w trybie list)
%   stop:               g�rna granica przemiatania (nieistotne w trybie list)
%   increment:          krok lub liczba punkt�w na oktaw� / dekad� (nieistotne w trybie list)

if ~iscell(source_name)
    source_name = {source_name};
end;
if ~iscell(sweep_type)
    sweep_type = {sweep_type};
end;
if ~iscell(start_list)
    sweep_type = {start_list};
end;
if ~iscell(stop)
    sweep_type = {stop};
end;
if ~iscell(increment)
    sweep_type = {increment};
end;

command = '.dc';

sweeps_count = length(source_name);

for i=1:sweeps_count

    switch sweep_type{i}
        case 0
            sweep_type{i} = 'oct';
        case 1
            sweep_type{i} = 'dec';
        case 2
            sweep_type{i} = 'lin';
        case 3
            sweep_type{i} ='list';
    end;

    if strcmp(sweep_type{i},'list')
        command = [command ' ' source_name{i} ' list' sprintf(' %e', start_list{i})];
    elseif strcmp(sweep_type{i},'lin')
        command = [command ' ' source_name{i} sprintf(' %e %e %e', start_list{i}, stop{i}, increment{i})];
    else
        command = [command ' ' source_name{i} ' ' sweep_type{i} sprintf(' %e %e %e', start_list{i}, stop{i}, increment{i})];
    end;  
end;    
