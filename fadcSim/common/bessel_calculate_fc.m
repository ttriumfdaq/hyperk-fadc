function fc = bessel_calculate_fc( N, f_sample, t_rise )
%BESSEL_CALCULATE_FC Calculate cut-off frequency of a bessel filter for given amount of samples at the rising edge
%
%   [fc fstop] = bessel_calculate_fc( N, f_sample, t_rise )
%   Calculate cut-off frequency (fc) of a Bessel-type shaping filter of a given order N, when
%	sampling frequency is f_sample and the desired number of samples at the rising edge is t_rise.
%	The time of the rising edge is defined as 10% to 90% of the amplitude.
%
%	The function returns the following results:
%		fc - cut-off frequency fc (in Hz).
%		fstop - stopband corner frequency

% Oblicz wymagane omega zero (tj. cz�sto�� graniczn� do kt�rej jest sta�e op�nienie grupowe)
w0 = bessel_calculate_w0(N, f_sample, t_rise);

% A teraz utw�rz model filtra i okre�l jego cz�stotliwo�� graniczn�
[num,den] = besself(N,w0);
H = tf(num,den);
fc = bandwidth(H) / (2*pi);

end

