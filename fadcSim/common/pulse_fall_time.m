function fall_time = pulse_fall_time( t, y, thr_low, thr_high )
%PULSE_FALL_TIME Calculate fall time of the pulse
%
%   Calculates fall time of the provided pulse. The method finds the
%   maximum, then proceeds forward in order to find low and high
%   thresholds. The exact times for low and high thresholds are
%   interpolated using linear interpolation. The method DOES NOT subtract
%   pedestal.
%
%   Arguments:
%       t - time vector
%       y - pulse vector (must be same length as time)
%       thr_low - low threshols, specified as fraction of max amplitude
%       thr_high - high thresholds, specified as fraction of max amplitude

% Wyznacz progi (niski i wysoki)
[a_max,i_max] = max(y);
thr_10 = thr_low * a_max;
thr_90 = thr_high * a_max;

%
% Wyznacz czas opadania
%

% Wyznacz punkt 90%
j = i_max-1 + find(y(i_max:length(y)) > thr_90, 1, 'last');
y_vector = [y(j) y(j+1)];
t_vector = [t(j) t(j+1)];
y_interp = [y(j) thr_90 y(j+1)];
t_interp = interp1(y_vector, t_vector, y_interp, 'linear');
t1 = t_interp(2);

% Wyznacz punkt 10%
j = i_max-1 + find(y(i_max:length(y)) > thr_10, 1, 'last');
y_vector = [y(j) y(j+1)];
t_vector = [t(j) t(j+1)];
y_interp = [y(j) thr_10 y(j+1)];
t_interp = interp1(y_vector, t_vector, y_interp, 'linear');
t2 = t_interp(2);

% Oblicz czas opadania
fall_time = t2-t1; 
    
end

