function command = spice_ac_command(sweep_type, no_points_list, start_freq, stop_freq)
%Generuje komend� .ac (AC Analysis)
%Parametry:
%   sweep_type:         rodzaj przemiatania osi cz�stotliwo�ci
%       0 lub 'oct':    oktawami
%       1 lub 'dec':    dekadami
%       2 lub 'lin':    liniowo
%       3 lub 'list':   lista punkt�w
%   no_pointslist:      liczba punkt�w na oktaw� / dekad� / w og�le 
%                       lub lista punkt�w w przypadku trybu list
%   start_freq:         dolna cz�stotliwo�� przemiatania (pomin�� w trybie list)
%   stop_freq:          g�rna cz�stotliwo�� przemiatania (pomin�� w trybie list)
     
switch sweep_type
    case 0
        sweep_type = 'oct';
    case 1
        sweep_type = 'dec';
    case 2
        sweep_type = 'lin';
    case 3
        sweep_type ='list';
end;

if strcmp(sweep_type,'list')
    command = ['.ac list' sprintf(' %e', no_points_list)];
else
    command = sprintf('.ac %s %e %e %e', sweep_type, no_points_list, start_freq, stop_freq);
end;
