function awg_waveform = resample_awg( awgval, risetime, sim_resolution )
%PULSE_RISE_TIME Calculate rise time of the pulse

global SHOW_PLOTS;
DEBUG_FIGURES = 240:244;

% These four lines are DEBUG ONLY %
%risetime = 6;
%sim_resolution = 0.001;
%awgval = [0 0 1 16 167 1103 4761 13521 25570 32767 29238 18958 9514 4010 1542 572 210 77 28 10 3 1 0 0];
%awgval = awgval ./ max(awgval);

% Add additional zeros to the front and back of the AWG samples from data tile to keep FFT routines happy
% (they assume periodic waveforms and we do not want discontinuities when we concatenate pulses).
awgval = [0 awgval 0];

% Compute the length
N = length(awgval);

% Interpolate the AWG waveform in order to get a reasonable estimate of the risetime
L = 100;    % Increase samples density by a factor of 100
M = N*L;
x = 0:L:L*N-1;
xi = 0:M-1;
yi = interpft(awgval,M);

% Show debug figure
if SHOW_PLOTS
    figure(DEBUG_FIGURES(1));
    plot(x,awgval,'o',xi,yi,'-');
    legend('AWG samples (norm.)','Interpolated data');
end

% Calculate risetime of the interpolated waveform
interp_risetime = pulse_rise_time(1:length(yi), yi, 0.1, 0.9);

% Calculate resampling factor
resample_factor = risetime/sim_resolution / interp_risetime;

% Resample the response
M_final = round(M * resample_factor);
awg_waveform = resample(yi, M_final, M);

if SHOW_PLOTS
    figure(DEBUG_FIGURES(2));
    x = 1:1/sim_resolution:length(awg_waveform);
    plot(x,awg_waveform(x),'o',1:length(awg_waveform),awg_waveform,'-');
    legend('Resampled AWG','Microstep data');
end
