% Skrypt badaj�cy charakterystyki dolnoprzepustowych filtr�w Bessela o
% r�nych rz�dach pod k�tem czas�w narastania i opadania zboczy odpowiedzi
% impulsowej. 
%
% Okre�la tak�e wymagane wzmocnienie kolejnych sekcji implementuj�cych bieguny powy�ej 
% drugiego b�d� trzeciego - w zale�no�ci, czy pierwsza sekcja b�dzie dwu
% czy tr�jbiegunowa. Poziomem odniesienia jest amplituda odpowiedzi
% wyj�ciowej pierwszej sekcji.

% Numerki okienek do wy�wietlania wynik�w
my_figures = [20:30];

% Style linii do wy�wietlania wynik�w
my_linestyles = {'-', ':', '--'};

% Ustawienia do oblicze�
max_order = 12;     % Maksymalny rz�d filtru, kt�ry b�dziemy bada�
t_max = 6;         % Czas, do kt�rego b�dziemy wy�wietla� odpowiedzi impulsowe filtr�w
thr_low = 0.1;      % Dolny pr�g do obliczania czas�w narastania/opadania (wzgl�dem amplitudy impulsu)
thr_high = 0.9;     % G�rny pr�g do obliczania czas�w narastania/opadania (wzgl�dem amplitudy impulsu)

% Utw�rz prototypy filtr�w (znormalizowane)
fprintf(1,'Calculating transfer functions and impulse responses of Bessel-type low pass filters...\n');

% Projektuj tak, by mie� identyczn� cz�stotliwo�� odci�cia (gdy� funkcja besself zmienia cz�stotliwo��
% odci�cia tak, by mie� sta�e op�nienie grupowe, a my chcemy mie� identyczne pasmo, bo ono okre�la stromo��
% przedniego zbocza).

% Wyczy�� tablice na wyniki 
H = {};
y = {};
t = {};

% Wyznacz transmitancj� filtra 2-go rz�du - b�dziemy tak projektowa� reszt�, by dosta� ten sam czas narastania
[num,den] = besself(2,1);
H{1} = tf(num,den);
[y{1}, t{1}] = impulse(H{1}, t_max);
reference_t_rise = pulse_rise_time(t{1}, y{1}, 0.1, 0.9);

fprintf(1,'\tt_rise (N=%d) = %.1f\n', 2, reference_t_rise);

for N = 3:max_order
	
	% Indeks do tablic, �eby nie trzeba co i rusz odejmowa�
	i = N-1;
	
	% Wyznacz standardowy prototyp
    [num,den] = besself(N,1);
    H{i} = tf(num,den);
	
	% Wyznacz czas narastania odpowiedzi impulsowej
	[y{i}, t{i}] = impulse(H{i});
	current_t_rise = pulse_rise_time(t{i}, y{i}, 0.1, 0.9);
	%keyboard;
	
	% Skoryguj cz�stotliwo�� projektow� filtra tak, aby dosta� identyczny czas narastania
	adjusted_w0 = current_t_rise / reference_t_rise;
	
	% Wyznacz w�a�ciwy prototyp
    [num,den] = besself(N,adjusted_w0);
    H{N-1} = tf(num,den);
	
	% Wyznacz odpowied� impulsow� i sprawd�, czy mamy w�a�ciwy czas narastania
	[y{i}, t{i}] = impulse(H{i}, t_max);
	current_t_rise = pulse_rise_time(t{i}, y{i}, 0.1, 0.9);
	fprintf(1,'    t_rise (N=%d) = %.1f\n', N, current_t_rise);	
end;

% Wyznacz po�o�enia przeci�cia prog�w 0.1 oraz 0.9 amplitudy
t_thr_low = zeros(1,length(H));
t_thr_high = zeros(1,length(H));
y_thr_low = zeros(1,length(H));
y_thr_high = zeros(1,length(H));

for i=1:length(H)
	[amplitude idx] = max(y{i}); 	
	thr_level = 0.1*amplitude;
	j = find(y{i}(1:idx) < thr_level, 1, 'last');
	y_vector = [y{i}(j) y{i}(j+1)];
	t_vector = [t{i}(j) t{i}(j+1)];
	y_interp = [y{i}(j) thr_level y{i}(j+1)];
	t_interp = interp1(y_vector, t_vector, y_interp, 'linear');
	
	t_thr_low(i) = t_interp(2);
	y_thr_low(i) = thr_level;	
	%keyboard;
	
	thr_level = 0.9*amplitude;
	j = find(y{i}(1:idx) < thr_level, 1, 'last');
	y_vector = [y{i}(j) y{i}(j+1)];
	t_vector = [t{i}(j) t{i}(j+1)];
	y_interp = [y{i}(j) thr_level y{i}(j+1)];
	t_interp = interp1(y_vector, t_vector, y_interp, 'linear');
	
	t_thr_high(i) = t_interp(2);
	y_thr_high(i) = thr_level;
	
end

% Wyr�wnaj wykresy do punktu przeci�cia progu
max_t_thr = max(t_thr_low);
for i=1:length(H)
	t_shift = max_t_thr - t_thr_low(i);
	if (t_shift > 0)
		t{i} = t{i} + t_shift;
		t_thr_low(i) = t_thr_low(i) + t_shift;	 % Uwzgl�dnij przesuwanie wykresu tak�e przy punktach przej�cia progu
		t_thr_high(i) = t_thr_high(i) + t_shift;
		
		t{i} = [0;t{i}];
		y{i} = [0;y{i}];
	end
	
	if (max(t{i}) > t_max)
		t_max = max(t{i});
	end
end

% % Wyznacz po�o�enia maksim�w
% t_peak = zeros(1,length(H));
% 
% for i=1:length(H)
% 	[~, idx] = max(y{i});
% 	t_peak(i) = t{i}(idx);
% end
% 
% % Wyr�wnaj maksima na osi czasu
% max_t_peak = max(t_peak);
% for i=1:length(H)
% 	t_shift = max_t_peak - t_peak(i);
% 	if (t_shift > 0)
% 		t{i} = t{i} + t_shift
% 		t{i} = [0;t{i}];
% 		y{i} = [0;y{i}];
% 	end
% end

%
% Wy�wietl odpowiedzi impulsowe poszczeg�lnych filtr�w
%
figure(my_figures(1));
hold off;
cmap = get(gca,'ColorOrder');	% Pobierz bie��c� map� kolor�w
my_color = 1;
my_line = 1;

% Narysuj lini� odniesienia 
plot_x = [0 t_max];
plot_y = [0 0];
plot(plot_x, plot_y, ':k');
hold on;

for i = 1:length(H)
    % Narysuj charakterystyki cz�stotliwo�ciowe; zmieniaj kolory tak, �eby
    % da�o si� to �adnie rozr�ni� (plot jako� nie chce robi� tego sam gdy mamy 'hold on').
    plot(t{i}, y{i}, 'LineWidth', 1, 'Color', cmap(my_color,:), 'LineStyle', my_linestyles{my_line});
	
	% Narysuj punkty przej�cia prog�w 0.1 i 0.9 amplitudy
	%plot(t_thr_low(i), y_thr_low(i), 'Marker', '.', 'Color', cmap(my_color,:));
	%plot(t_thr_high(i), y_thr_high(i), 'Marker', '.', 'Color', cmap(my_color,:));
    
	my_color = my_color+1;
	if my_color > length(cmap)
		my_color = 1;
        my_line = my_line + 1;
	end;      
end;

% Narysuj linie oznaczaj�ce czas 0.1 i 0.9 amplitudy impulsu
g = get(gca);
plot([max(t_thr_low) max(t_thr_low)], [g.YLim(1) g.YLim(2)], 'k:');
plot([max(t_thr_high) max(t_thr_high)], [g.YLim(1) g.YLim(2)], 'k:');

hold off;
xlabel('Time [s]');
ylabel('Amplitude [A.U.]');
title('Impulse Response');
axis([0 t_max -0.1 1.1]);

%
% Wy�wietl znormalizowane odpowiedzi impulsowe poszczeg�lnych filtr�w
%
figure(my_figures(2));
hold off;
cmap = get(gca,'ColorOrder');	% Pobierz bie��c� map� kolor�w
my_color = 1;
my_line = 1;

% Narysuj lini� odniesienia 
plot_x = [0 t_max];
plot_y = [0 0];
plot(plot_x, plot_y, ':k');
hold on;

for i = 1:length(H)
    % Narysuj charakterystyki cz�stotliwo�ciowe; zmieniaj kolory tak, �eby
    % da�o si� to �adnie rozr�ni� (plot jako� nie chce robi� tego sam gdy mamy 'hold on').
    plot(t{i}, y{i} ./ max(y{i}), 'LineWidth', 1, 'Color', cmap(my_color,:), 'LineStyle', my_linestyles{my_line});
	
	% Narysuj punkty przej�cia prog�w 0.1 i 0.9 amplitudy
	%plot(t_thr_low(i), y_thr_low(i), 'Marker', '.', 'Color', cmap(my_color,:));
	%plot(t_thr_high(i), y_thr_high(i), 'Marker', '.', 'Color', cmap(my_color,:));
    
	my_color = my_color+1;
	if my_color > length(cmap)
		my_color = 1;
        my_line = my_line + 1;
	end;      
end;

% Narysuj linie oznaczaj�ce czas 0.1 i 0.9 amplitudy impulsu
g = get(gca);
plot([max(t_thr_low) max(t_thr_low)], [g.YLim(1) g.YLim(2)], 'k:');
plot([max(t_thr_high) max(t_thr_high)], [g.YLim(1) g.YLim(2)], 'k:');
plot([g.XLim(1) g.XLim(2)], [0.1 0.1], 'k:');
plot([g.XLim(1) g.XLim(2)], [0.9 0.9], 'k:');

hold off;
xlabel('Time [s]');
ylabel('Amplitude [A.U.]');
title('Impulse Response (normalized amplitude)');
axis([0 t_max -0.1 1.1]);

%
% Sprawd� asymetri� impuls�w
%

% Poinformuj co robimy
fprintf(1,'Checking pulse asymmetry...\n');

% Wylicz czasy narastania i opadania (10% do 90%) poszczeg�lnych odpowiedzi impulsowych,
% a nast�pnie przygotuj wykres stosunku czasu opadania do czasu narastania
rise_time = zeros(1,length(H));
fall_time = zeros(1,length(H));

% Wyznacz czasy narastania oraz opadania
for i = 1:length(H)      
    rise_time(i) = pulse_rise_time(t{i}, y{i}, thr_low, thr_high);
    fall_time(i) = pulse_fall_time(t{i}, y{i}, thr_low, thr_high);   
	fprintf(1,'\tt_fall/t_rise (N=%d) = %.2f\n', i+1, fall_time(i)/rise_time(i));
end

% Wy�wietl zale�no�� stosunku czas�w opadania do czasu narastania w
% zale�no�ci od rz�du filtra Bessela
figure(my_figures(3));
plot_x = [2:0.01:max_order];
plot_y = interp1([2:max_order], fall_time ./ rise_time, plot_x, 'spline');
plot(plot_x, plot_y, 'b');
hold on;
plot([2:max_order], fall_time ./ rise_time, 'k.');
hold off;
xlabel('Filter Order');
ylabel('t_{fall}/t_{rise}');
title('Pulse Assymetry');
axis([1.5 max_order+0.5 0.9 4.8]);

%
% Sprawd� FWHM w zale�no�ci od rz�du filtra, przy sta�ym czasie narastania
%

% Poinformuj co robimy
fprintf(1,'Checking pulse FWHM...\n');

% Wylicz czasy narastania i opadania (10% do 90%) poszczeg�lnych odpowiedzi impulsowych,
% a nast�pnie przygotuj wykres stosunku czasu opadania do czasu narastania
fwhm = zeros(1,length(H));

% Wyznacz FWHM odpowiedzi impulsowych
for i = 1:length(H)      
    fwhm(i) = pulse_fwhm(t{i}, y{i});
	fprintf(1,'\tFWHM (N=%d) = %.2f\n', i+1, fwhm(i));	
end

% Wy�wietl zale�no�� stosunku czas�w opadania do czasu narastania w
% zale�no�ci od rz�du filtra Bessela
figure(my_figures(4));
plot_x = [2:0.01:max_order];
plot_y = interp1([2:max_order], fall_time ./ rise_time, plot_x, 'spline');
plot(plot_x, plot_y, 'b');
hold on;
plot([2:max_order], fall_time ./ rise_time, 'k.');
hold off;
xlabel('Filter Order');
ylabel('FWHM (sec)');
title('Pulse FWHM');
axis([1.5 max_order+0.5 0.8 4.8]);

%
% Narysuj wykresy z charakterystyk� cz�stotliwo�ciow�
%

% Poinformuj co robimy
fprintf(1,'Plotting frequency characteristics...\n');

% Wy�wietl charakterystki cz�stotliwo�ciowe filtr�w o poszczeg�lnych rz�dach
figure(my_figures(5));
hold off;
cmap = get(gca,'ColorOrder');	% Pobierz bie��c� map� kolor�w
my_color = 1;
my_line = 1;

for i = 1:length(H)
    % Wylicz wykresy Bodego i przekszta�� charakterystyk� cz�stotliwo�ciow�
    % do postaci jednowymiarowej tablicy
    [mag phase w] = bode(H{i});
	my_mag = reshape(mag,1,[]);
    
    % Narysuj charakterystyki cz�stotliwo�ciowe; zmieniaj kolory tak, �eby
    % da�o si� to �adnie rozr�ni� (semilogx jako� nie chce robi� tego sam).
	semilogx(w, 20*log(my_mag),'LineWidth',1,'Color',cmap(my_color,:), 'LineStyle', my_linestyles{my_line});
    hold on;
	
	my_color = my_color+1;
	if my_color > length(cmap)
		my_color = 1;
		my_line = my_line + 1;
	end;    
end;

% Narysuj progi dla poszczeg�lnych rozdzielczo�ci przetwornika ADC
adc_bits = [10 12 14 16];
plot_x = [0.1 10];
for i=1:length(adc_bits)
	% Policz teoretyczny SNR dla danel liczby bit�w przetwornika; Pr�g dla t�umienia filtru to b�dzie -SNR.
	adc_snr = 6.02*adc_bits(i)+1.76;	% [dB]
	plot_y = [-adc_snr -adc_snr];
	plot(plot_x, plot_y, ':k');
end
hold off;
axis([0.1 10 -100 10]);
xlabel('Normalized frequency (rad/s)');
ylabel('Amplitude [dB]');
title('Frequency Response');

%
% Narysuj wykres z zale�no�ci� cz�stotliwo�ci granicznej filtra od jego rz�du, przy sta�ym czasie narastania
%

% Poinformuj co robimy
fprintf(1,'Plotting cut-off frequency vs filter order...\n');

% Przygotuj struktury danych
wc = zeros(1, length(H));

for i = 1:length(H)
    % Wylicz cz�stotliwo�� graniczn� filtra
    wc(i) = bandwidth(H{i});
	fprintf(1,'\tCut-off frequency (N=%d) = %.2f rad/s\n', i+1, wc(i));
end;

figure(my_figures(6));

plot_x = [2:0.01:max_order];
plot_y = interp1([2:max_order], wc, plot_x, 'spline');
plot(plot_x, plot_y, 'b');
hold on;
plot([2:max_order], wc, 'k.');
hold off;
xlabel('Filter Order');
ylabel('Cut-off frequency (rad/s)');
title('Cut-off Frequency (t_{rise} = const)');
axis([1.5 max_order+0.5 0.7 2.5]);
% fprintf(1,'Calculating gain necessary to maintain output pulse amplitude...\n');
% 
% % Teraz policz wzmocnienia potrzebne dla kolejnych rz�d�w powy�ej drugiego i potem powy�ej trzeciego.
% % Wyniki poka� na jednym wykresie. Zak�adamy, �e na wyj�ciu filtru chcemy mie� amplitud� wykorzystuj�c� ca�y zakres dynamiki wzmacniacza.
% % Przyjmujemy tak�e, �e tak dobieramy wzmocnienie fotopowielacza, by pierwszy stopie� m�g� mie� wzmocnienie jeden przy wykorzystanym
% % maksymalnym zakresie dynamiki wzmacniacza.
% amplitudes = zeros(1, length(H));
% gain_2 = zeros(1, length(H)-1);
% gain_3 = zeros(1, length(H)-2);
% 
% % Oblicz amplitudy odpowiedzi impulsowych
% for i=1:length(H)
%     amplitudes(i) = max(y{i});
% end;
% 
% % Policz wzmocnienia dla przypadku z pierwszym stopniem 2-go rz�du
% for i=2:length(H)
%     gain_2(i-1) = amplitudes(1) / amplitudes(i);
% end;
% 
% % Policz wzmocnienia dla przypadku z pierwszym stopniem 2-go rz�du
% for i=3:length(H)
%     gain_3(i-2) = amplitudes(2) / amplitudes(i);
% end;

% % Wykre�l zale�no�ni na wzmocnienie
% figure(my_figures(4));
% hold off;
% plot_x = [1:length(gain_2)];
% plot(plot_x, gain_2, plot_x(1:length(plot_x)-1), gain_3, 'LineWidth',1);
% hold on;
% plot(plot_x, gain_2, 'k.', plot_x(1:length(plot_x)-1), gain_3, 'k.');
% hold off;
% 
% xlabel('Additional Stages');
% ylabel('Gain');
% title('Required Shaper Gain');
% axis([0 length(gain_2)+1 1.05 2.05]);

