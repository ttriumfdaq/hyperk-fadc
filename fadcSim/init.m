global SPICE_DIR;
global SPICE_EXEC;
global RESULTS_FOLDER;
global FIGURES_FOLDER;
global SHOW_PLOTS;
global SAVE_PLOTS_TO_PNG;
global SAVE_PLOTS_TO_EPS;
global TEX_CREATE_FILE;
global TEX_PLOTS_PER_PAGE;
global DEFAULT_PRINT_FMT;

SPICE_DIR = '"C:\Program Files (x86)\LTC\LTspiceIV\"';
SPICE_EXEC = '"C:\Program Files (x86)\LTC\LTspiceIV\scad3.exe"';

FIGURES_FOLDER = 'figures';
RESULTS_FOLDER = 'results';

addpath('common');

% Wy�wietlanie okienek pomocniczych
SHOW_PLOTS = 0;

% Zapisywanie obrazk�w do plik�w
SAVE_PLOTS_TO_PNG = 0;
SAVE_PLOTS_TO_EPS = 0;

% Tworzenie kodu TeX do do��czenia plik�w z wykresami do raportu
TEX_CREATE_FILE = 0;
TEX_PLOTS_PER_PAGE = 6;

% Ustal domy�lne formatowanie rysunk�w do wydruku
DEFAULT_PRINT_FMT = DefaultAxesFormat();

% Uruchom mo�liwo�� oblicze� r�wnoleg�ych -- old matlab, do not use
% if matlabpool('size') == 0
%     matlabpool open
% end

% Start parallel computing server (new matlab)
% if isempty(gcp('nocreate'))
%     parpool('local',2);
% end
    