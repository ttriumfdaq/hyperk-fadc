% Skrypt do badania dyskryminatora CF w zale�no�ci od rz�du zastosowanego filtra Bessela.
% Nie uwzgl�dnia analizy szumowej.

global SHOW_PLOTS;
global RESULTS_FOLDER;

% Numerki okienek do wy�wietlania wynik�w
my_figures = [30 31 32 33 34];

% Ustawienia do oblicze�
min_order = 3;      % Minimalny rz�d filtru, kt�ry b�dziemy bada�
max_order = 12;     % Maksymalny rz�d filtru, kt�ry b�dziemy bada�
t_max = 18;         % Czas, do kt�rego b�dziemy wy�wietla� odpowiedzi impulsowe filtr�w
thr_low = 0.1;      % Dolny pr�g do obliczania czas�w narastania/opadania (wzgl�dem amplitudy impulsu)
thr_high = 0.9;     % G�rny pr�g do obliczania czas�w narastania/opadania (wzgl�dem amplitudy impulsu)

min_cf_delay = 0.1;             % Minimalne op�nienie impulsu w dyskryminatorze CF (wielokrotno�� czasu narastania)
max_cf_delay = 5;               % Maksymalne op�nienie impulsu w dyskryminatorze CF (wielokrotno�� czasu narastania)
samples_at_rising_edge = 100;   % Liczba pr�bek na narastaj�cym zboczu impulsu

% Dane dla symulacji odsuni�cia od optymalnego op�nienia dyskryminatora CF
timing_shift_min = -1;                  % Maksymalne odsuni�cie czasowe na minus od optymalnego op�nienia dyskryminatora CF (wielokrotno�� czasu narastania)
timing_shift_max = 1;					% Maksymalne odsuni�cie czasowe na plus od optymalnego op�nienia dyskryminatora CF (wielokrotno�� czasu narastania)
timing_shift_step = 0.1;				% Krok op�nienia, od minimalnej do maksymalnej odchy�ki (wielokrotno�� czasu narastania)
timing_shift_cnt = 5000;				% Liczba iteracji dla danego op�nienia
timing_sub_sample_resolution = 0.001;	% Krok symulacji czasowej przesuni�� narastaj�cego zbocza impulsu wzgl�dem okresu pr�bkowania (tj. z jak� dok�adno�ci� interpolujemy pomi�rzy pr�bkami). 

% Oblicz op�nienia dyskryminatora CF wyra�one w pr�bkach
min_cf_delay_in_samples = min_cf_delay * samples_at_rising_edge;
max_cf_delay_in_samples = max_cf_delay * samples_at_rising_edge;

% Utw�rz prototypy filtr�w (znormalizowane)
fprintf(1,'Creating transfer functions of Bessel-type low pass filters...\n');

H = {};
for N = min_order:max_order
    [num,den] = besself(N,1);
    H{N-min_order+1} = tf(num,den);    
end;

% Wyznacz charakterystki nachylenia krzywych dyskryminatora CF
fprintf(1,'Calculating slope of digital constant fraction waveforms...\n');

% Utw�rz macierze do przechowywania wynik�w
max_slope = zeros(max_order-min_order+1, 2);
cf_slope = zeros(max_order-min_order+1, max_cf_delay_in_samples - min_cf_delay_in_samples + 1);

%
% Teraz wyznacz charakterystyki nachylenia dyskryminatora CF
%

% Tutaj robimy tablic�, bo p�niej wykorzystamy te wyniki przy symulacjach czasowych b��d�w algorytmu CF
dt = zeros(1,length(H));

for i = 1:length(H)
    
    % Wyznacz odpowied� impulsow� filtru
    [y, t] = impulse(H{i}, t_max);
    
    % Zmodyfikuj krok czasowy odpowiedzi impulsowej tak, by mie� 100 pr�bek na zboczu narastaj�cym.
    % W�wczas zmieniaj�c op�nienie o jeden b�dziemy de-facto mieli op�nienie wyra�one w procentach czasu
    % narastania.
    t_rise = pulse_rise_time(t, y, thr_low, thr_high);
    dt(i) = t_rise / samples_at_rising_edge;
    t = 0:dt(i):t_max;
    y = impulse(H{i},t);
    
    % Normalizuj odpowied� filtra
    y = y ./ max(y);
    
    % Sprawd�, czy rzeczywi�cie mamy 100 pr�bek na zboczu narastaj�cym
    t_rise = pulse_rise_time(1:length(y), y, thr_low, thr_high);
    fprintf(1,'   Filter order: %d, samples at rising edge: %f\n', int32(i+min_order-1), t_rise);
    
    % Unormuj odpowied� filtru
    y = y ./ max(y);
    
	%
    % Oblicz charakterystyk� nachylenia krzywej dyskryminatora CF, a nast�pnie wyznacz maksymalne nachylenie w punkcie przeci�cia
	%
    for j = min_cf_delay_in_samples:max_cf_delay_in_samples
        [tmp cf_slope(i,j-min_cf_delay_in_samples+1)] = pulse_cf_time(y, j, SHOW_PLOTS);
    end;
    
    %for j = 1:100
    %    cf_slope(i,:) = smooth(cf_slope(i,:),30);
    %end;

    % Znajd� indeksy dla fitu (tj. zaw� zakres fitowania do punkt�w w pobli�u maksymalnego nachylenia)
    idx = find(cf_slope(i,:) < 0.9 * min(cf_slope(i,:)));
    fit_x = idx' + min_cf_delay_in_samples - 1;
    fit_y = cf_slope(i,idx)';
    f0 = fit(fit_x, fit_y, 'poly3');
    %keyboard;
    
    % Znajd� minimum oraz uwzgl�dnij, �e nasz indeks '1' odpowiada niezerowemu op�nieniu w dyskryminatorze CF
    [max_slope(i,1) max_slope(i,2)] = min(f0(min_cf_delay_in_samples:max_cf_delay_in_samples));
     max_slope(i,2) = max_slope(i,2) + min_cf_delay_in_samples - 1;
end;

% Wyra� op�nienia w wielokrotno�ciach czasu narastania
max_slope(:,2) = max_slope(:,2) / samples_at_rising_edge;

% Sprawd�, na ile mo�emy odsun�� si� od op�nienia generuj�cego maksymalne zbocze, �eby wci�� utrzyma� przyzwoite osi�gi dyskryminatora CF
% Dalej zak�adamy, �e mamy du�y nadmiar pr�bek na zboczu narastajacym, wi�c cz�stotliwo�� pr�bkowania nie powinna by� problemem.
fprintf(1,'Testing time shift from optimum delay...\n');

% Utw�rz macierze i wektory na potrzeby zapami�tania oblicze�
cf_delay = timing_shift_min : timing_shift_step : timing_shift_max;
time_sigma_vs_delay = zeros(length(H), length(cf_delay));
time_avg_vs_delay = zeros(length(H), length(cf_delay));

% Sprawd�, czy mamy zezwolenie na obliczenia r�wnoleg�e
if matlabpool('size') == 0
    matlabpool open;
end;

for i = 1:length(H)
    % Powiedz, kt�ry rz�d filtru symulujemy
    fprintf(1,'  Current filter order: %d\n', min_order-1+i);
    
    % Wylicz odpowied� impulsow� filtru na zag�szczonym przebiegu, uwzgl�dniaj�c nasz�d rozdzielczo�� mi�dzy pr�bkami. Przesuni�cia pod-pr�bkowe
	% b�dziemy realizowa� w�a�nie na tym przebiegu, gdy� niestety nie mo�na wykorzysta� opcji z podawaniem wektora czasu do MATLAB-owej funkcji obliczaj�cej 
	% odpowied� impulsow�, gdy� robi przybli�enia i zaburzy symulacj� b��d�w algorytmu dyskryminatora CF.
    t = 0 : dt(i)*timing_sub_sample_resolution : t_max;
    y = impulse(H{i},t);

    % Normalizuj odpowied� filtra
    y = y ./ max(y);    

    % Oblicz minimalne i maksymalne op�nienia, w "pe�nych" pr�bkach
    min_delay = round( (max_slope(i,2) + min(cf_delay)) * samples_at_rising_edge );
    max_delay = round( (max_slope(i,2) + max(cf_delay)) * samples_at_rising_edge );
    step_delay = round(timing_shift_step * samples_at_rising_edge);
    
    % Wyznacz czas przekroczenia przez impuls po�owy amplitudy. P�niej b�dziemy por�wnywa�, jak zmienia si�
    % czas dyskryminatora CF z czasem osi�gni�cia po�owy amplitudy w zale�no�ci od op�nienia impulsu odwr�conego.
    t_half_amplitude = find(y > max(y)/2, 1) * timing_sub_sample_resolution;

	% Utw�rz wektor op�nie�, kt�re b�dziemy symulowa�
	cf_delay_in_samples = min_delay:step_delay:max_delay;
	%keyboard;
	
    % Symuluj op�nienia
    for d = 1:length(cf_delay_in_samples)
        % Poka�, kt�re op�nienie symulujemy
        fprintf(1,'    Delay shift: %d%% of rise time \n', int32(100*cf_delay(d)));
    
        % Symuluj offsety czasowe
        cf_time = zeros(1,timing_shift_cnt);
        tmp_delay = cf_delay_in_samples(d);
        
        parfor j = 1:timing_shift_cnt
        %for j = 1:20
            % Utw�rz spr�bkowany przebieg na podstawie przebiegu zag�szczonego
            t_offset = 1 + round((1/timing_sub_sample_resolution - 1) * rand(1,1));
            idx_sample = t_offset : 1/timing_sub_sample_resolution : length(t);
            y_sampled = y(idx_sample);
            
            % Wyznacz czas za pomoc� dyskryminatora CF i uwzgl�dnij za�o�ony wcze�niej offset
            % (dodaj�c offset pr�bkowania de facto przyspieszczamy impuls, w zwi�zku z czym musimy to
            % uwzgl�dni� w wyniku)
            cf_time(j) = pulse_cf_time(y_sampled, tmp_delay, SHOW_PLOTS);
            cf_time(j) = cf_time(j) + (t_offset-1) * timing_sub_sample_resolution;
        end;
        
        % Oblicz rozdzielczo�� czasow� (tj. RMS)
        time_sigma_vs_delay(i,d) = std(cf_time);
        time_avg_vs_delay(i,d) = mean(cf_time) - t_half_amplitude;
    end;
    
    % Wyczy�� zag�szczone przebiegi (bo zajmuj� du�o pami�ci)
    clear t y;
end;

save(strcat(RESULTS_FOLDER,'/bessel_cf_analyze.mat'), 'max_slope');
