% Skrypt do wy�wietlania wynik�w badania dyskryminatora CF w zale�no�ci od rz�du zastosowanego filtra Bessela.

% Numerki okienek do wy�wietlania wynik�w
my_figures = [30 31 32 33 34];
my_linestyles = {'-', ':', '--'};

% Wy�wietl wykresy nachylenia impulsu dyskryminatora CF w funkcji op�nienia impulsu
figure(my_figures(1));
hold off;
cmap = get(gca,'ColorOrder');	% Pobierz bie��c� map� kolor�w
my_color = 2;
my_line = 1;

for i=1:length(H)
    plot_x = min_cf_delay : 1/samples_at_rising_edge : max_cf_delay;
    plot_y = cf_slope(i,:);
    plot(plot_x, plot_y, 'LineWidth', 1, 'Color', cmap(my_color,:), 'LineStyle', my_linestyles{my_line});
    hold on;
    
   	my_color = my_color+1;
	if my_color > length(cmap)
		my_color = 1;
        my_line = my_line + 1;
	end;      
end;

xlabel('CF delay [t_{tise}]');
ylabel('Slope @ Zero-Crossing');
title(sprintf('CF Pulse Slope vs Pulse Delay (t_{rise} = %d samples)',samples_at_rising_edge));

% Wy�wietl wykres op�nienia dla maksymalnego nachylenia w funkcji rz�du filtru
figure(my_figures(2));
hold off;

plot_x = min_order:0.01:max_order;
plot_y = interp1([min_order:max_order], max_slope(:,2), plot_x, 'spline');
plot(plot_x, plot_y, 'b');
hold on;
plot([min_order:max_order], max_slope(:,2), 'k.');

xlabel('Filter Order');
ylabel('Delay [t_{rise}]');
title(sprintf('Delay @ Maximum Slope (t_{rise} = %d samples)',samples_at_rising_edge));
%axis([2.5 12.5 1.3 2.3]);

% Wy�wietl wykres maksymalnego nachylenia w punkcie wyznaczania czasu, w funkcji rz�du filtru
figure(my_figures(3));
hold off;

plot_x = min_order:0.01:max_order;
plot_y = interp1([min_order:max_order], max_slope(:,1), plot_x, 'spline');
plot(plot_x, plot_y, 'b');
hold on;
plot([min_order:max_order], max_slope(:,1), 'k.');

xlabel('Filter Order');
ylabel('Maximum Slope');
title(sprintf('Maximum Slope @ Zero-Crossing (t_{rise} = %d samples)',samples_at_rising_edge));
%axis([2.5 12.5 -2.7e-4 -2.35e-4]);

% Narysuj wykresy rozdzielczo�ci czasowych w funkcji op�nienia
figure(my_figures(4));
hold off;
cmap = get(gca,'ColorOrder');	% Pobierz bie��c� map� kolor�w
my_color = 2;
my_line = 1;

for i=1:length(H)
    plot(cf_delay, time_sigma_vs_delay(i,:), 'LineWidth', 1, 'Color', cmap(my_color,:), 'LineStyle', my_linestyles{my_line});
    hold on;
    
   	my_color = my_color+1;
	if my_color > length(cmap)
		my_color = 1;
        my_line = my_line + 1;
	end;      
end;

xlabel('CF_{delay} - CF_{max slope delay} [t_{rise}]');
ylabel('\sigma_{time} [samples]');
title(sprintf('CF Algorithm Errors (t_{rise} = %d samples)',samples_at_rising_edge));

% Narysuj wykresy �redniego wyznaczonego czasu w funkcji op�nienia
figure(my_figures(5));
hold off;
cmap = get(gca,'ColorOrder');	% Pobierz bie��c� map� kolor�w
my_color = 2;
my_line = 1;

for i=1:length(H)
    plot(cf_delay, time_avg_vs_delay(i,:), 'LineWidth', 1, 'Color', cmap(my_color,:), 'LineStyle', my_linestyles{my_line});
    hold on;
    
   	my_color = my_color+1;
	if my_color > length(cmap)
		my_color = 1;
        my_line = my_line + 1;
	end;      
end;

xlabel('CF_{delay} - CF_{max slope delay} [t_{rise}]');
ylabel('CF time - t_{1/2} [samples]');
title(sprintf('CF Time vs t_{1/2} (t_{rise} = %d samples)',samples_at_rising_edge));