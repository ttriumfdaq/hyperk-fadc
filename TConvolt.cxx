#include "TConvolt.hxx"

double ConvoltPeakTime(TGraph *sigConvolt,
		   double baseline,
		   int shaper) {

  // Fit function to convolution graph

  // Get points
  int n = sigConvolt->GetN();
  std::vector<double> xconv(n);
  std::vector<double> yconv(n);

  double xconvPeak = 0;
  double yconvPeak = 0;

  // Fill points and find peak time
  for(int i = 0; i < n; i++) {

    sigConvolt->GetPoint(i,xconv[i],yconv[i]);

    if(yconv[i] > yconvPeak) {

      yconvPeak = yconv[i];
      xconvPeak = xconv[i];

    }

  }

  // Set Fit boundaries
  double range_low = 0;
  double range_high = 0;

  if(shaper == 0){
    // 15 ns
    range_low = 30; 
    range_high = 30;
  }else if(shaper == 1){
    // 30 ns
    range_low = 80 + 60;
    range_high = 80;
  }else {
    // No shaper (Template)
    range_low = 29;
    range_high = 22;
  }
  
  double start = xconvPeak - range_low;
  double end = xconvPeak + range_high;

  // Fit function
  TF1 *ConvoltFit = new TF1("ConvoltFit",funcGNV2,start,end,8);
  fitGNV2(ConvoltFit,xconvPeak,yconvPeak,baseline,0,shaper,1,start,end);

  TFitResultPtr ConvoltFitPtr = sigConvolt->Fit("ConvoltFit","QMRS","",start,end);

  double convPeakTime = ConvoltFit->GetMaximumX();


  // static double avg  = 0;
  // static double avgn = 0;
        
  // avg += ConvoltFit->GetParameter(2);
  // avgn += 1.0;
       
  // std::cout << "single value " << ConvoltFit->GetParameter(2)
  // 	    << " average value " <<  avg/avgn 
  // 	    << std::endl;



   
  if(ConvoltFitPtr->CovMatrixStatus() == 3) {

    return convPeakTime;
      
  } else {

    std::cout << "Bad Conv fit" << std::endl;
    return 0;

  }



}
