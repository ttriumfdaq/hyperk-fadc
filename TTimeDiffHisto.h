#ifndef TTimeDiffHisto_h
#define TTimeDiffHisto_h

#include <string>
#include "THistogramArrayBase.h"


/// Class for making histograms of 
/// fitted time differences.
class TTimeDiffHisto : public THistogramArrayBase {
public:
  TTimeDiffHisto();
  virtual ~TTimeDiffHisto(){};

  void UpdateHistograms(TDataContainer& dataContainer){};
  
  void UpdateHistograms(double pe, double timeDiff);

  // Reset the histograms; needed before we re-fill each histo.
  void Reset();
  

  TH1F *tmp; // used in CreateHistograms()
  void CreateHistograms();
  
  /// Take actions at begin run
  void BeginRun(int transition,int run,int time){		
    CreateHistograms();		
  }


private:
};

#endif


