#ifndef TPulseFitter_h
#define TPulseFitter_h

#include "TTree.h"
#include "TH1D.h"
#include "TF1.h"
#include "TGraph.h"

#include "TFits.hxx"


class PulseFitterResult{

public:

  double fittedTime;

};



class TPulseFitter  {

public:

  TPulseFitter(){
    
  }


  PulseFitterResult FitPulse(TH1D *waveform, 
			     int ch,
			     bool negPulse,
			     bool useMF,
			     double noise,
			     double pulseHeight, 
			     double peakTime, 
			     double baseline, 
			     int pulseType, 
			     int shaper,
			     TGraph *graph = 0);


  void EndRun(){

  }
  
private:

};

#endif

